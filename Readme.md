# Cosmo: A Concurrent Separation Logic for Multicore OCaml

These are the machine-checked proofs that accompany the following papers:

  * *Cosmo: A Concurrent Separation Logic for Multicore OCaml*,
    by Glen Mével, Jacques-Henri Jourdan and François Pottier (ICFP 2020).
  * *Formal Verification of a Concurrent Bounded Queue in a Weak Memory Model*,
    by Glen Mével and Jacques-Henri Jourdan (ICFP 2021).

## Compiling

### Step 1: Creating an opam switch

_If opam is not already installed:_ See instructions [there][install-opam] to
install it; then:

    opam init --comp=4.11.1
    eval $(opam config env)

(This will create a `~/.opam` directory.)

_If opam is already installed:_ Create a new switch for the project:

    opam switch cosmo --alias-of 4.11.1                    # for opam 1.x
    opam switch create cosmo ocaml-base-compiler.4.11.1    # for opam 2.x
    eval $(opam config env)

### Step 2: Installing the dependencies

In an opam switch as created above, the commands

    opam repo add iris-dev https://gitlab.mpi-sws.org/iris/opam.git
    opam update
    make build-dep

will pin and install the dependencies at the correct version.

If you want to browse the Coq development using CoqIDE (a graphical, interactive
toplevel for Coq), install it as well:

    opam install coqide

### Step 3: Compiling the proof scripts

When all required libraries can be found (e.g. in an opam switch as configured
above), compile the proof scripts with:

    make -j4

Other recipes are available, such as `all`, `clean` and `userinstall`.

[install-opam]: https://opam.ocaml.org/doc/Install.html

## Index of modules

In the proof scripts, comments starting with “ICFP20” or “ICFP21” highlight
connections with respective papers. Below is an index of relevant modules and
what they contain. **Modules of significance for the ICFP21 paper are put in bold.**

Base definitions:

  * `base/vprop.v`
    defines the type vProp of subjective (ie. lifted) assertions
    (note: what the papers denote with _↑V_ has no specific denotation in the
    Coq development, it is simply written `monPred_in V`)
  * `lang/model.v`
    defines the memory semantics
  * `lang/lang.v`
    defines the toy programming language, both its syntax and small-step
    semantics (additional notations in `lang/notations.v`)

Building the program logic:

  * `program_logic/store.v`
    defines the fragment of the state interpretation describing the store,
    and the various pointsto predicates
  * `program_logic/weakestpre.v`
    defines what the ICFP20 paper calls the _valid_ predicate on views (unfortunately
    named `seen` in the Coq development), the associated fragment of the state
    interpretation, and the WP predicate of the lifted logic
  * `program_logic/lifting.v`
    defines the state interpretation by wrapping together the two previous
    modules, and proves the Hoare triples of all constructs of the programming
    language in the base logic then in the lifted logic
  * `program_logic/adequacy.v`
    proves the adequacy (soundness) of the lifted logic
  * `program_logic/proofmode.v`
    defines tactics for carrying out interactive proofs of programs in the lifted
    logic, within Iris’s “Proof Mode”
  * **`program_logic/atomic.v`
    defines logically atomic triples, including notations**

Case studies:

  * `examples/spinlock.v`
  * `examples/ticketlock.v`
  * `examples/dekker.v`
  * `examples/peterson.v`
  * **`examples/bounded_queue.v`:
    specification and proof of correctness of a concurrent bounded queue
    implemented with a circular buffer**
  * **`examples/pipeline.v`:
    example client code using the bounded queue**

## License

This work is distributed under the terms of the [3-clause BSD license](LICENSE).
