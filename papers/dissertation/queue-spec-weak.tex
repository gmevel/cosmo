\subsection{Specification under weak memory: synchronization}
\label{sec:queue:spec:weak}

\input{fig-queue-spec-weak}

Up to now, we have ignored the weakly consistent behavior of the semantics of \mocaml
(\citenop{dolan-18}; described in~\sref{sec:mocaml:model}).
Starting in this section, we take this aspect into account
and propose a refined specification, stated in our program logic \hlog.


%\hlog has a notion of view (\sref{sec:hlog:assertions:langindpt})
%that captures the essence of weak memory---%
%the fact that each thread has different knowledge of the state of the shared memory.
%A view represents this subjective knowledge.
%All assertions of the logic depend on an \emph{ambient view},
%which corresponds to the current view of the subject thread.
%Thus, \hlog{} assertions are in general \emph{subjective},
%that is, implicitly dependent on an ambient view.

Because \hlog{} is based on Iris, logically atomic triples can also be defined in \hlog.
In fact, the specification shown in~\fref{fig:queue:spec:sc} still applies.
%
Yet, as such, it is of little value in a weakly consistent context.
Indeed, as explained in~\sref{sec:queue:spec:sc}, it is designed so that
$\isQueueSC\elemList$ can be shared among threads by means of an invariant.
But, in \hlog{}, invariants are restricted to containing objective assertions.
%
Hence, our first addition to the specification is to stipulate that
the representation predicate is objective.
%
This reflects the fact that there exists a total order on the updates to the logical state,
on which all threads objectively agree.

% FP voilà les grandes lignes de ce que je dirais sur la linéarisabilité:
%
%    linearizability is a (complex) technical criterion
%    that is defined only in an SC setting
%      (because it assumes, as its starting point, that
%       we have a *totally ordered* trace of call and
%       return events),
%    so one must be careful not to use this word in a weak-memory setting,
%    unless one has defined its meaning (possible citation of Smith, Winter, and Colvin, 2018).
%
%    In an SC setting, the purpose of linearizability is to imply a refinement relation:
%      "the fine-grained concurrent data structure
%      behaves like coarse-grained data structure,
%      that is, a sequential data structure guarded by a lock".
%    Here, even though our concurrent queue has "linearizable
%    internal state" in a certain sense (there exists only one
%    instance of the predicate $\isQueueSC\elemList$, which
%    at every time keeps track of "the current logical state"
%    of the queue), it does \emph{not} quite behave like a
%    coarse-grained data structure,
%    because it imposes fewer \emph{happens-before}
%    relationships between accesses. Indeed, all accesses to a lock are totally ordered
%    by \emph{happens-before}, whereas our data structure guarantees
%    the existence of \emph{happens-before} relationships between
%    some, but not all, pairs of operations.

Even with this addition, the specification given in~\sref{sec:queue:spec:sc} is not precise enough
to verify interesting clients such as the one described in~\sref{sec:pipeline}.
Indeed, in a weakly consistent setting, one typically expects a concurrent data structure to establish
synchronization between some of its concurrent accesses.
For example, imagine that thread~$A$ enqueues a pointer to a complex data structure (say, a hash table).
Then, when thread~$B$ dequeues this pointer, $B$ should obtain the unique ownership
of the hash table
and be able to access it accordingly.
In a weakly consistent memory model, $B$ expects to see all of the changes that $A$ has made to the data~structure.
This is guaranteed only if there is a \emph{happens-before} relationship from
the enqueuing event to the dequeuing event.

One possibility would be to guarantee that our concurrent queue implementation behaves like its coarse-grained alternative, that is, a sequential implementation guarded by a lock.
This would correspond to an intuitive definition of linearizability, even though this notion is difficult to define precisely outside of the world of sequential consistency~\citep{smith2019linearizability}.
However, the concurrent queue implementation (\sref{sec:queue:impl}) that we aim to verify is weaker than that: it does guarantee \emph{some} happens-before relationships, but not between all pairs of accesses.
Namely, it guarantees a happens-before relationship:
%
\begin{enumerate}%
  \item from an enqueuer to the dequeuer that obtains the corresponding item;
  \item from an enqueuer to the following enqueuers;
  \item from a dequeuer to the following dequeuers.
\end{enumerate}%
The first one permits resource transfer through the queue as described in the example above.

In \hlog{}, as seen in previous chapters,
happens-before relationships can be expressed as transfers of views
(denoted in this chapter by calligraphic capital letters, such as $\mathcal{T,H,V,S}$):
specifying a happens-before relationship between two program points
can be done by giving the client the ability to transfer any assertion of the form $\seen\view$
between these two points:
this corresponds to saying that the destination program point
has all the knowledge the source program point had about the shared memory.
%
Thanks to rule~\splitso (\sref{sec:hlog:assertions:langindpt}),
this is sufficient for transferring any subjective resource from a sender to a receiver,
as will be confirmed later when verifying a client application of the queue (\sref{sec:pipeline:proof}).
This technique has already been illustrated by the case studies in \chref{sec:examples}.

In the specification of the queue,
to express the happens-before relationships mentioned earlier,
the representation predicate now takes more parameters:
%
\[
  \isQueue \tview \hview \elemViewList
\]

\begin{enumerate}%

  \item For each item~$\elem_\idx$ in the queue, we now have a corresponding
    view~$\eview_\idx$. This view materializes the flow of memory knowledge
    from the thread which enqueued the item, to the one which will dequeue it.

  \item The \textit{head} view~$\hview$ materializes memory knowledge
    accumulated by successive enqueuers.

  \item The \textit{tail} view~$\tview$ materializes memory knowledge
    accumulated by successive dequeuers.

\end{enumerate}%

The queue that we study, however, does not guarantee any happens-before relationship from
a dequeuer to an enqueuer.%
%
\footnote{%
  This is not entirely true: the implementation shown in \sref{sec:queue:impl} does create
  a happens-before relationship from the dequeuer of rank~$\idx$ to the enqueuer of
  rank~$\idx+\capacity$ (hence also to all enqueuers of subsequent ranks).
  We choose to not reveal this in the specification, since it reflects uninteresting implementation details.
}
%
Hence, it provides fewer guarantees than a sequential queue guarded by a lock.

Interestingly, \hlog{} is able to express this subtle difference between the behavior of our library and that of a lock-based implementation:
the full specification under weak memory is shown in~\fref{fig:queue:spec:weak}.
%To simplify notation, we left the premise $\queueInv$ implicit for all logically atomic specifications.
This specification extends the previous one~(\fref{fig:queue:spec:sc}) with views.
The mentioned happens-before relationships are captured as follows.

\begin{enumerate}%

  \item When a thread with a local view~$\view$ (in other words, with
    $\seen\view$ as a precondition) $\enqueue$s an item~$\elem$, it pairs it
    with the view~$\view$.
    Afterwards, when another thread $\dequeue$s that same item~$\elem_0$, it
    merges the view~$\eview_0$ that was paired with it into its own local view
    (in other words, it obtains $\seen\eview_0$ as a postcondition).

  \item When a thread $\enqueue$s an item, it also obtains the head~view~$\hview$
    left by the previous enqueuer (in other words, it obtains $\seen\hview$ as
    a postcondition), and it adds its own view~$\view$ to the head~view (which
    becomes $\hview \viewjoin \view$).

  \item When a thread $\dequeue$s an item, it also obtains the tail~view~$\tview$
    left by the previous dequeuer (in other words, it obtains $\seen\tview$ as
    a postcondition), and it adds its own view~$\view$ to the tail~view (which
    becomes $\tview \viewjoin \view$).

\end{enumerate}%
