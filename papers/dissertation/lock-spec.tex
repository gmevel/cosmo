% ------------------------------------------------------------------------------

% General presentation of locks; specification of locks in CSL.

\section{Specification of locks}
\label{sec:examples:lock:spec}

The lock is perhaps the most basic and best known concurrent data structure.
It supports three operations, namely \lockmake, \acquire, and \release.
% \lockmake allocates a fresh lock; \acquire locks a lock; \release unlocks it.
%
One way of describing its purpose is to say that it allows achieving
\emph{mutual exclusion} between several threads: that is, the \acquire and
\release operations delimit \emph{critical sections} in the code, and the
purpose of the lock is to guarantee that no two threads can be in a critical
section ``at the same time''. However, this is not a very good description,
especially in a weak memory setting, where the intuitive notions of ``time''
and ``simultaneity'' do not make much sense. What matters, really, is that a
lock mediates access to a shared resource, and does so in a correct manner. A
thread that successfully acquires the lock expects the resource to be in a
consistent state, and expects to be allowed to affect this state in arbitrary
ways, as long as it brings the resource back to a consistent state by the time
it releases the lock.

\input{fig-lock-spec}

\CSL can express that idea in a simple and elegant manner~\citep{ohearn-07}.
The classic specification of dynamically-allocated locks in \CSL
\citep{gotsman-aplas-07,hobor-oracle-08,buisse-11,sieczkowski-15,iris-lecture-notes},
%
a~version of which appears in \fref{fig:lock:spec},
%
allows the user to choose an \emph{invariant}~$\prop$ when a lock is
allocated. This invariant is a \SL assertion. It appears in the postcondition
of $\acquire$ and in the precondition of $\release$, which means that a thread
that acquires the lock gets access to the invariant, and can subsequently
break this invariant if desired, while a thread that releases the lock must
restore and relinquish the invariant.

In \fref{fig:lock:spec}, the entire specification is contained in a triple for
\lockmake, whose precondition requires the invariant to hold initially, and whose
postcondition contains triples for \acquire and \release. Because a triple is
persistent \citep{iris}, therefore duplicable, several threads can share the
use of the newly-created lock.

In addition to the invariant~$\prop$, an abstract assertion ``$\locked$''
appears in the postcondition of $\acquire$ and in the precondition of
$\release$. Because it is abstract, it must be regarded as nonduplicable by the user.
Therefore, it can be thought of as a
``token'', a witness that the lock is currently held. This witness is required
and consumed by \release.
%
There are two reasons why it is desirable to let such a token appear in the
specification. First, from a user's point of view, this is a useful feature,
as it forbids releasing a lock that one does not hold, an error that could
arise in the (somewhat unlikely) situation where several copies of the
invariant~$\prop$ can coexist. Second, from an implementor's point of view,
this makes the specification easier to satisfy. In a ticket lock
implementation (\sref{sec:examples:ticketlock}), for instance, only the thread
that holds the lock can safely release~it. Thus, a ticket lock does not
satisfy a stronger specification of locks where the ``\locked'' token is omitted.
%
% It is clear that the spec without \locked is stronger:
% indeed, if it is satisfied, then the spec in \fref{fig:lock:spec}
% is satisfied as well: just take \locked to be $\TRUE$.
%

If one can prove that an implementation of locks satisfies this specification,
then (because \SL is compositional) a user can safely rely on this
specification to \emph{reason} about her use of locks in an application
program. This is a necessary and sufficient condition for an implementation of
locks to be considered correct. Proving something along the lines of ``no two
threads can be in a critical section at the same time'' is not necessary, nor
would it be sufficient.
It would not provide a means of reasoning about user programs.
And, in a weak memory setting, a lock might actually guarantee
mutual exclusion and nevertheless be broken, failing to achieve the
necessary degree of synchronization.

We emphasize that, when this specification is understood in the setting of
\hlog, the user invariant~$\prop$ is an arbitrary \hlog assertion, thus
possibly a subjective assertion. Indeed, the
synchronization performed by the lock at runtime ensures that every thread has
a consistent view of the shared resource. This is in contrast with Iris
invariants, which involve no runtime synchronization, and therefore must be
restricted to objective assertions (\sref{sec:hlog:assertions:langindpt}).

We must also emphasize that, because we work in a logic of partial
correctness, this specification does not guarantee deadlock freedom, nor does
it guarantee any form of fairness. As an extreme example, an implementation
where \acquire diverges satisfies the specification in \fref{fig:lock:spec}.
%
% Also, even the lock is perfectly correct, nothing prevents the user
% from attempting to acquire a lock twice.

Proving the correctness of a lock implementation
against the abstract lock interface shown in \fref{fig:lock:spec}
consists of proving a triple for \lockmake.
In the body of \lockmake, once all the physical locations are allocated
and are in scope, one would typically
create the ghost state that the proof needs (if any),
establish an invariant that governs the data structure,
instantiate ``\locked'',
then prove the mandated triples for \acquire and \release.
