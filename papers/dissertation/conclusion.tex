\chapter{Conclusion and future work}
\label{sec:conclusion}

% Summary of what we have done.

In this dissertation, we have laid the groundwork for verifying \mocaml programs
in a suitable variant of \CSL. We have instantiated Iris \citep{iris} for
\mocaml, yielding a low-level logic, \llog, which exposes all of the details of
the \mocaml memory model \citep{dolan-18}, including timestamps, histories, and
views. \llog can in principle verify arbitrary \mocaml programs, including
programs that involve data races on non-atomic memory locations.

However, \llog is too low-level to be pleasant and convenient.
%
Thus, we have built a higher-level logic, \hlog, aiming to provide simpler reasoning
principles. In order to achieve this result, we have removed the ability of
reasoning about programs that race on non-atomic memory locations---%
we believe it is reasonable to assume that most (correct) practical programs are
exempt of such races. This has allowed us to offer
the illusion that a non-atomic memory location stores a single value,
to remove all mention of timestamps and histories,
and to present views to the user as an abstract type,
equipped with the structure of a bounded semilattice.
%
We believe that the manner in which \hlog allows reasoning about weak memory
is original and relatively simple and natural. A key mechanism is the ability
to split an arbitrary assertion~$\prop$ into an objective fragment
$\prop\opat\view$ and a subjective fragment $\seen\view$ and to later
reassemble these fragments. The two fragments are transmitted from one thread
to another by different means: whereas sharing an objective assertion requires
no runtime synchronization and is typically achieved via an Iris invariant,
transmitting a view is typically achieved by relying on the
``release/acquire'' behavior of atomic writes and reads.

\hlog contains \CSL as a fragment, that allows reasoning about coarse-grained
concurrent programs in the same way as in \CSL.

We have illustrated the applicability of the high-level logic \hlog to the study
of several concurrent data structures, including a relaxed multiple-reader
multiple-write queue. In these case studies, we found the view mechanism to be
a pleasant and concise formalism to specify and reason about thread
synchronization.
%
In order to write satisfactory specifications for such lock-free data structures,
we have used a notion of logical atomicity. Although it is straightforward to
port the existing definition of that notion \citep{jung-slides-2019} to a weak
memory setting, logical atomicity in such a setting does not imply
``linearizability'' in the traditional sense:
logical atomicity does imply the existence of a linear history, but
synchronization between successive events in that history is not guaranteed.

% Future work.

As said earlier, our high-level logic \hlog does not support reasoning about
data races on non-atomic memory locations. Yet, there are some legitimate
programs which exploit such races correctly. For instance, idempotent
work-stealing \citep{michael2009idempotent} aims at reducing synchronizations
between cooperating threads in a load-balancing algorithm, under the assumption
that the tasks can be repeated without compromising program correctness.
%
Writing concurrent programs which are willingly racy is a preserve of memory
model experts, and the correctness of such programs is especially subtle.
Therefore, there is a call for a program logic that allows checking them.
Although it is possible to reason about these racy programs in the low-level
logic \llog, an opportunity for future research is to discover what rules can be
proposed to reason about these programs without abandoning the simplicity of
\hlog.
%
Drawing inspiration from iGPS \citep{kaiser-17},
one direction might be to have racy non-atomic locations be governed by
per-location protocols, involving some notion of state monotonicity.

Another future work may consist in proposing suitable specifications for \mocaml's
\href{https://github.com/ocaml-multicore/ocaml-multicore/blob/master/stdlib/domain.mli}
     {\texttt{Domain}} API,
which offers a set of primitive synchronization operations.
%
On this basis, one may verify concurrent data structures that have been
developed for \mocaml, for instance
a \href{https://github.com/ocaml-multicore/kcas} {multiword compare-and-swap},%
  \footnote{This uses RDCSS, which has been verified in Iris by \citet{jung-prophecies-20}.}
a suite of lock-free
\href{https://github.com/ocaml-multicore/lockfree}{work-stealing queues},
bags, and hash tables,
and an \href{https://github.com/ocaml-multicore/reagents}{implementation of Turon's reagents}
(\citeyear{turon-12}).

On an even more practical ground, much engineering work may be done to make
\llog applicable to \emph{actual} \mocaml code. At the moment, the code that
\llog reasons about belongs to a toy language whose syntax is deeply embedded
within Coq. This minimalist language focuses on memory-concerned operations and
lacks most of the features of the actual \mocaml language, including types,
modules, objects, algebraic data~types, exceptions and algebraic effects.
%
A realistic verification tool should
\begin{enumerate}%
  \item \label{it:subset}
    support a satisfying subset of \mocaml, including the aforementioned features,
  \item \label{it:translator}
    be connected to actual \mocaml code
  \item \label{it:auto}
    and offer an interface to proving specifications that minimizes human effort.
\end{enumerate}%
%
Regarding item~\ref{it:subset}, many programming features do not interact with
the memory model and should be straightforward to support, if cumbersome.
However, exceptions, algebraic effects and their handlers%
  \footnote{Algebraic effects and handlers are another feature added to OCaml
    by \mocaml. Although this feature is conceptually independent from
    concurrency, it constitutes a convenient tool for implementing cooperative
    scheduling, hence it has been deemed useful to ship both features together.
  }
have a non-local impact on the control flow and thus cannot be dealt with seamlessly.
Specialized program logics have been designed to reason about languages with
exceptions and algebraic effects \citep{de-vilhena-pottier-21}.
Work remains to be done to build a unified framework that would support them in
addition to weak-memory concurrency.
%
Item~\ref{it:translator} could involve implementing an automatic translation
tool from deep-embedded code to \mocaml source files, or the converse.
For now, \mocaml itself has no notion of formal specification---neither does it
have, in fact, a formal semantics.
However, the Gospel project \citep{gospel} aims at defining a specification
language for OCaml; that specification language is translated to \SL.
Provided that Gospel be extended to \CSL, it would then be natural that our
verification framework be capable of reading these specifications.

Regarding item~\ref{it:auto}: in the vein of the Iris Proof Mode \citep{krebbers-17},
the proof mode we have built for \hlog is easy to use, but verbose: it follows
the syntax of the implementation closely and requires writing one tactic
invocation per reduction step of the subject program.
Such a proof script is legible, but lengthy and sensitive to small code changes.
A user might hope for a more automated interface which would tackle the
mundane parts and let her focus on the interesting parts, those where human
intelligence is the most required: designing ghost state and invariants, and
choosing when to open and close them.
Several proof efforts have been conducted to automate proofs of programs in Iris
\citep{refinedc,mulder2022diaframe,bedrocksystems}.
Designing a good automation infrastructure for \hlog is thus a possible future work.

Lastly, on the theoretical ground, one may build a semantic model of \mocaml's
type system in \hlog---that is, interpreting types of the source language as
predicates of the logic.
In addition to proving type soundness, this would help specifying typed
functions which could be called by untyped (and unsafe) code, as long as the
latter would conform to the binary interface for the input and output types.

% We do not exploit the Local DRF property, nor do we know exactly what it
% means. Could we say something about it?
