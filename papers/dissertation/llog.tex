\chapter{A low-level logic: \llog}
\label{sec:llog}

% ------------------------------------------------------------------------------

% A general presentation of the Iris ``functor''.

In this chapter, we set up a program logic for \mocaml, based on the
operational semantics presented in the previous chapter. To do so, we rely on
Iris~\citep{iris}. Iris is not tied to a particular
programming language or calculus.
Its lower layer, the Iris base logic~\citep[\S3--5]{iris}, is a purely logical
construction, of which we have given a short introduction in~\sref{sec:iris}.
Its upper layer, the Iris program logic~\citep[\S6--7]{iris}, is parameterized
with a programming language. In order to instantiate it, a client must provide
the following information \citep[\S7.3]{iris}.
%
\begin{itemize}
\item A set of ``expressions''.
\item A subset of ``values''.
\item A set of machine ``states''.
      For instance, a state might be a store, that is,
      a map whose domain is the set of all currently allocated memory locations.
      % éviter de dire ``a map of memory locations to values'',
      % car ce ne seraient pas les mêmes valeurs que mentionné ci-dessus.
\item An operational semantics,
      in the form of a ``per-thread step relation''.
      This relation relates an expression and a state
      to an expression and a state
      and a list of expressions, which represent newly-spawned threads.
      % Rel (expr*state) (expr*state*list expr).
      % This relation must have the property that a value cannot make a step.
      %
      % The thread-pool reduction relation is given by Iris, I suppose.
\item A ``state interpretation'' predicate $\stateinterp : \typename{State}\ra\typeIProp$.%
      \footnote{Recall that $\typeIProp$ is the type of Iris assertions.}
      This predicate represents a global invariant about the machine state.
      % It appears in the definition of wp (not yet mentioned).
      It typically relates the state
      with a piece of ghost state
      whose structure is chosen by the client so
      as to justify splitting the ownership of the machine state
      in certain ways.
      % This ghost state typically appears
      % in the definition of client assertions
      % such as the ``points-to'' assertion,
      % which represents the ownership of a part of the state.
      % (not yet mentioned)
\end{itemize}
Once the client has provided this information, the
framework yields a program logic, that is,
%
\begin{itemize}
\item A weakest-precondition predicate $\WP\expr\pred$.
\item A Hoare triple predicate $\hoare\prop\expr\pred$,
      which is just sugar for $\always{(\prop\wand\WP\expr\pred)}$.
      % \citep[\S6]{iris}.
\item An adequacy theorem \citep[\S6.4, \S7.4]{iris},
      which roughly state that if a closed program~$\expr$ satisfies
      $\WP\expr\pred$
      then it is safe to run, % in an empty initial store
      that is, its execution will not lead to a stuck configuration;
      moreover, its final configurations will satisfy the postcondition~$\pred$.
\item A set of programming-language-independent deduction rules for triples
      (\sref{sec:csl} and \sref{sec:iris}).
      These include the consequence rule, the frame rule,
      rules for allocating and updating ghost state,
      rules for setting up and exploiting invariants,
      and so on.
\end{itemize}
%
It is then up to the client to perform
extra programming-language-specific work,
namely:
%
\begin{itemize}
\item Define programming-language-specific assertions,
      such as ``points-to'' assertions.
\item Prove
      % programming-language-specific
      entailment laws
      % that involve these assertions,
      describing, e.g., how points-to assertions
      can be split and combined.
\item Establish programming-language-specific deduction rules
      for triples,
      e.g., rules that give triples for reading and writing memory cells.
      % The preconditions and postconditions of these triples
      % typically involve points-to assertions.
\end{itemize}
%
We now apply this recipe to \mocaml.
This yields \llog, a logic for \mocaml.

% ------------------------------------------------------------------------------

% How we apply the Iris functor.

\section{Instantiating Iris for \mocaml}
\label{sec:llog:instantiation}

% cf. theories/lang/lang.v
% The language is named view_lang.

We begin instantiating Iris as follows:
%
\begin{itemize}
\item An ``expression'' is a pair $\mkexpr\expr\view$
      of a \mocaml expression and a view.
\item Accordingly, a ``value'' is a pair $\mkval\val\view$
      of a \mocaml value and a view.
\item A ``state'' is a store $\store$.
\item The ``per-thread step relation''
      is as defined in \fref{fig:lang:langsem:expr:impure}.
\end{itemize}

To complete this instantiation, there remains to define a suitable ``state
interpretation''~$\stateinterp$. In choosing this definition, we
have a great deal of freedom. Our choice is guided by several considerations,
including the manner in which we wish to allow splitting the ownership of the
state, and the invariants about the state that we wish to keep track of. In
the present case, we have the following three independent concerns in mind.
%
\begin{itemize}
\item[---] We wish to allow splitting the ownership of memory cells
  (be it atomic or non-atomic) under a standard
  ``fractional permissions'' regime \citep{boyland-fractions-03}.
\item[---] We wish to have a persistent assertion reflecting the knowledge
  that a given memory block has a certain length.
\item[---] We need to keep track of the global view invariant
  (\sref{sec:mocaml:lang:semantics})
  enjoyed by the operational semantics of \mocaml,
  because this invariant is required to justify
  that a non-atomic read instruction at an allocated non-atomic cell cannot be stuck.
\end{itemize}
%
To achieve these goals, we define our state interpretation~$\stateinterp$
with well-chosen camera structures.
We use standard camera constructions presented in~\sref{sec:iris:cmra}.
We delay an explanation of the definition to \sref{sec:llog:assertions}.

\begin{itemize}
\item Let $\gnamestore$ be a ghost variable storing an element of
      \(\authm (\typeStoreCMRA)\), where we define the camera of stores as:
      \[
        \typeStoreCMRA
        \;\eqdef\;
        \begin{array}{@{}l@{}}
              \typeCell \maparrow
          \\  \quad
              \big( \agm(\typeNACellContents) + \agm(\typeATCellContents) \big)
              \times \fracm
        \end{array}
      \]%.
      We encode a physical store~$\store$ as an element of this camera as follows:
      \[
        \storetocmra\store
        \;\eqdef\;
        \begin{array}{@{}l@{\;}l@{}}
            & \mapcompr
                {\locoff\loc\offset}
                {\pair{\cinl(\aginj(\hist))}1 \qquad\;}
                {\;\loc \in \Dom\store
                  \land 0 \leq \offset < \length{\storeapp\store\loc}
                  \land \storeapp\store{\locoff\loc\offset} = \cinl(\hist)}
          \\  \uplus
            & \mapcompr
                {\locoff\loc\offset}
                {\pair{\cinr(\aginj(\mkval\val\view))}1\;}
                {\;\loc \in \Dom\store
                  \land 0 \leq \offset < \length{\storeapp\store\loc}
                  \land \storeapp\store{\locoff\loc\offset} = \cinr(\mkval\val\view)}
        \end{array}
      \]
      We use this piece of ghost state to implement points-to assertions
      (\sref{sec:llog:assertions:napointsto},
      \sref{sec:llog:assertions:atpointsto}).
\item Let $\gnamelen$ be a ghost variable storing an element of
      \(
        \authm (\typeLoc \maparrow \agm(\Z))
      \).
      This camera allows us to encode the lengths of the blocks
      of the physical store~$\store$ as follows:
      \[
        \storetolencmra\store
        \;\;\eqdef\;\;
        \mapcompr
          {\loc}
          {\aginj(\length{\storeapp\store\loc})}
          {\loc \in \Dom\store}
      \]
      We use this piece of ghost state to implement length assertions
      (\sref{sec:llog:assertions:haslength}).
\item Let $\gnameglobalview$ be a ghost variable storing an element of
      $\authm(\typeView)$, where we equip the type of views with a camera
      structure by taking the join operation $\viewjoin$ as the composition.
      This composition law is idempotent.
      This piece of ghost state allows us to keep track of
      the global view~$\globalview$,
      which we use to implement view validity
      (\sref{sec:llog:assertions:validview}).
\item We define the state interpretation as the following $\typeIProp$~assertion:
  \[
    \stateinterp(\store)
    \;\;\eqdef\;\;
    \ownGhost{\gnamestore}{\authfull{\storetocmra\store}}
    \isep
    \ownGhost{\gnamelen}{\authfull{\storetolencmra\store}}
    \isep
    \Exists \globalview.%
    \isepV{%
      \ownGhost{\gnameglobalview}{\authfull\globalview}
      \cr
      \liftpure{%
        \All \atcell, \val, \viewatloc.%
        \storeapp\store\atcell = \mkval\val\viewatloc \IMPLIES
        \viewatloc \viewleq \globalview
      }
      \cr
      \liftpure{%
        \All \nacell, \hist.%
        \storeapp\store\nacell = \hist \IMPLIES
        \viewapp\globalview\nacell \in \Dom\hist
      }
      \cr
      \liftpure{%
        \All \cell \notin \Dom\store.%
        \viewapp\globalview\cell = \timeMin
      }
    }
  \]
  %Note that the last three conjuncts are pure assertions.
\end{itemize}

% ------------------------------------------------------------------------------

% The soundness theorem for the base logic.

\section{Soundness of \llog}
\label{sec:llog:soundness}

Having instantiated Iris for the operational semantics of \mocaml
(\sref{sec:mocaml:lang:semantics})
and for a state interpretation of our choosing (previous section),
we obtain a weakest-precondition predicate $\WP{\mkexpr\expr\view}\pred$;
a~triple $\hoare\prop{\mkexpr\expr\view}\pred$,
satisfying a set of programming-language-independent deduction rules
(\fref{fig:hoare:struct});
and an adequacy theorem, stated now,
which guarantees that this triple is sound.

Triples conform to the informal interpretation given in~\sref{sec:progverif}.
In a triple $\hoare\prop{\mkexpr\expr\thisview}\pred$,
the precondition~$\prop$ describes the resources required in order to safely
execute the expression $\expr$ on a thread whose view is $\thisview$. The
postcondition~$\pred$, which takes a pair $\mkval{\val'}{\thisview'}$ of a
value and a new view as an argument, describes the updated resources that
exist at the end of this execution, if it terminates. This is thus a logic of
partial correctness, as defined in~\sref{sec:progverif}.
%
This claim is made formal by the following theorem, which we establish
by relying on Iris's generic adequacy theorem~\citep[\S7.4]{iris}.

A configuration $\store; \pool$ is said to be \emph{safe}
if there is no reduction sequence
$\store; \pool \threadPoolReduces^* \store'; \pool'$
such that $\store'; \pool'$ is stuck (as defined on~p.~\pageref{def:stuck}).
%
A configuration $\store; \mkexpr\expr\view$ is said to be \emph{adequate}
with respect to some pure predicate
$\pprop : {\typeVal \times \typeView \rightarrow \typeCoqProp}$
if it is safe and, for any reduction sequence
$\store; \mkexpr\expr\view \threadPoolReduces^*
\store'; \mkexpr{\val'}{\view'}, \pool'$
such that $\val'$ is a value, the proposition $\pprop(\val', \view')$ holds.
%
Note that this assertion is \emph{pure}, that is,
it lives in the host logic $\typeCoqProp$ (\sref{sec:iris}).

Recall that $\emptyview$ is the empty view and $\mapempty$ is the empty store.

\begin{theorem}[Adequacy of \llog]
  For any expression~$\expr \in \typeExpr$ and any
  pure predicate~$\pprop : \typeVal \times \typeView \rightarrow \typeCoqProp$,
  if the entailment $\vdash \WP {\mkexpr\expr\emptyview} {\liftpure\pprop}$ holds,
  then the configuration $\mapempty; \mkexpr\expr\emptyview$
  is adequate with respect to~$\pprop$.
  In particular, this configuration is safe.
\end{theorem}

% ------------------------------------------------------------------------------

% Specific assertions for \mocaml.

\section{\mocaml-specific assertions}
\label{sec:llog:assertions}

We now define four custom assertions, namely the non-atomic and atomic
``points-to'' assertions, the block length assertion a ``valid-view'' assertion.
%
Next (\sref{sec:llog:rules}), we give a set of
reasoning rules where these assertions appear.

\subsection{Non-atomic points-to}
\label{sec:llog:assertions:napointsto}

We wish to be able to split up the ownership of the non-atomic store under a
fractional permission regime. As
usual~\citep{boyland-fractions-03,bornat-permission-accounting-05}, the
fraction~1 should represent exclusive read-write access, while a fraction
$q<1$ should represent shared read-only access. Furthermore, we wish to ensure
that whoever owns a share of a non-atomic cell has full knowledge of its history.

For this purpose, we have placed the ghost-state-ownership assertion
$\ownGhost\gnamestore{\authfull \storetocmra\store}$ in the state interpretation
(\sref{sec:llog:instantiation}), and we now define the non-atomic
points-to assertion for a cell~$\nacell$, a~fraction~$q$ and a history~$\hist$,
as follows:
%
\[
  \histNA[q]{\nacell}\hist
  \;\eqdef\;
  \ownGhost\gnamestore
    {\authfrag {\mapsingleton\nacell{\pair{\cinl(\aginj(\hist))} q}}}
\]
%
We omit the fraction~$q$ when it is $1$:
we write $\histNA\nacell\hist$ for $\histNA[1]\nacell\hist$.

What is going on here?
%
On the one hand,
the points-to assertion claims the ownership of a fragmentary element of the camera
$\authm (\typeStoreCMRA)$.
Indeed, $\mapliteral{\mapbinding{\nacell}{\pair{\cinl(\aginj(\hist))} q}}$
denotes a~singleton map,
which maps the cell~$\nacell$ to the pair $\pair{\cinl(\aginj(\hist))} q$
where $\hist$ is a history (recall that $\typeNACellContents = \typeHist$)
and $q$ is a fraction.
%
On the other hand, the state interpretation owns the authoritative
element $\authfull \storetocmra\store$.
%
This ties the non-atomic fragment of the store
$\store$, which is part of the physical machine state, with the state of
the ghost variable~$\gnamestore$.

When such a points-to assertion $\histNA[q]\nacell\hist$ is at hand,
one has a fragmentary element
$\authfrag{\mapliteral{\mapbinding{\nacell}{\pair{\cinl(\aginj(\hist))} q}}}$.
%
By comparing it against the authoritative element $\authfull \storetocmra\store$,
one deduces
$\mapliteral{\mapbinding{\nacell}{\pair{\cinl(\aginj(\hist))} q}} \mincl \storetocmra\store$
where $\mincl$ is the ordering induced
by the pointwise composition law of the finite map camera,
which yields
$\pair{\cinl(\aginj(\hist))} q \mincl \storetocmra\store(\nacell)$, hence
$\cinl(\aginj(\hist)) \mincl \cinl(\aginj(\storeapp\store\nacell))$ and $q \mincl 1$.
Finally, by the laws of the sum and agreement cameras,
we find that $\storeapp\store\nacell = \hist$, that is,
in the physical store, $\nacell$ is indeed a non-atomic cell
whose history is exactly~$\hist$.
%
In short, the following holds, where the conclusion is pure:
%
\begin{mathpar}
  \infer{%
      \histNA[q]\nacell\hist  \IISEP
      \stateinterp(\store)
    }{%
      \storeapp\store\nacell = \hist  \IISEP
      0 < q \leq 1
    }
\end{mathpar}

The composition law of the underlying product camera achieves
agreement of the history components
and addition of the fraction components.
In other words, the following holds:
%
% composition + agreement, inline:
\[
  \histNA[q_1]\nacell{\hist_1}  \ISEP
  \histNA[q_2]\nacell{\hist_2}
  \IEQUIV
  \histNA[(q_1+q_2)]\nacell{\hist_1}  \ISEP
  \liftpure{\hist_1 = \hist_2}
\]
% composition, inline:
%\[
%  \histNA[q_1]\nacell{\hist}  \ISEP
%  \histNA[q_2]\nacell{\hist}
%  \IEQUIV
%  \histNA[(q_1+q_2)]\nacell{\hist}
%\]
%\begin{mathpar}
%  % composition + agreement, infer (FIXME: notation for equiv?):
%  \infer{%
%      \histNA[q_1]\nacell{\hist_1}  \IISEP
%      \histNA[q_2]\nacell{\hist_2}
%    }{%
%      \histNA[(q_1+q_2)]\nacell{\hist_1}  \IISEP
%      \liftpure{\hist_1 = \hist_2}
%    }
%  % agreement, infer (FIXME: notation for equiv?):
%  %\infer{%
%  %    \histNA[q_1]\nacell{\hist_1}  \IISEP
%  %    \histNA[q_2]\nacell{\hist_2}
%  %  }{%
%  %    \liftpure{\hist_1 = \hist_2}
%  %  }
%
%  % composition, inline:
%  %\infer{}
%  %  {
%  %    \histNA[q_1]\nacell{\hist}  \ISEP
%  %    \histNA[q_2]\nacell{\hist}
%  %    \IEQUIV
%  %    \histNA[(q_1+q_2)]\nacell{\hist}
%  %  }
%  %
%  % composition, infer 1/2:
%  %\infer{%
%  %    \histNA[(q_1+q_2)]\nacell{\hist}
%  %  }{%
%  %    \histNA[q_1]\nacell{\hist}  \IISEP
%  %    \histNA[q_2]\nacell{\hist}
%  %  }
%\end{mathpar}
%
Thus, one can read the assertion $\histNA[q]\nacell\hist$ as representing
the knowledge that the cell~$\nacell$ is non-atomic
and that its history is $\hist$,
along with the ownership of a $q$-share of that cell.
As explained in~\sref{sec:iris:cmra}, a 1-share is exclusive:
in the composition rule above, if $q_1 = 1$, then
from the combined points-to assertion and the state interpretation
we deduce $1 + q_2 \leq 1$, which contradicts $0 < q_2$.

This technique of encoding a (fractional) points-to assertion
using these ghost state constructions is not original:
we follow the pattern presented by~\citet[\S6.3.3, \S7.3]{iris}.
A departure from the well-established points-to assertion is that,
in our memory model, the global store maps each non-atomic cell
to an entire history, rather than a single value,
and the points-to assertion of \llog{} reflects that fact.
Indeed, unless some information is known about the view of the current thread,
every write in the history of a non-atomic cell might be relevant.

\subsection{Atomic points-to}
\label{sec:llog:assertions:atpointsto}

Regarding the atomic points-to assertion, we proceed essentially in the same
manner:
%
\[
  \pointstoAT[q] {\atcell} {\mkval\val\view}
  \;\eqdef\;
  \Exists \altview.\;
  \liftpure{\view \viewleq \altview}
  \isep
  \ownGhost{\gnamestore}
    {\authfrag {\mapsingleton\atcell{\pair{\cinr(\aginj(\mkval\val\altview))}q}}}
\]
We omit the fraction~$q$ when it is $1$:
we write $\pointstoAT\atcell{\mkval\val\view}$
for $\pointstoAT[1]\atcell{\mkval\val\view}$.

As a result of these definitions,
the assertion $\pointstoAT[q] {\atcell} {\mkval\val\view}$
claims the ownership of a $q$-share of~$\atcell$
and guarantees that it is an atomic memory cell,
that the value it stores is~$\val$, and
that the view  it stores is at least~$\view$.
%
Again, a 1-share is exclusive.
%
\begin{mathpar}
  \infer{%
      \pointstoAT[q]\atcell{\mkval\val\view}  \IISEP
      \stateinterp(\store)
    }{%
      \Exists \altview.
      \storeapp\store\atcell = \mkval\val{\altview}  \IISEP
      \view \viewleq \altview  \ISEP
      0 < q \leq 1
    }

  \infer{}
    {
      \pointstoAT[q_1]\atcell{\mkval{\val_1}{\view_1}}  \ISEP
      \pointstoAT[q_2]\atcell{\mkval{\val_2}{\view_2}}
      \IEQUIV
      \pointstoAT[(q_1+q_2)]\atcell{\mkval{\val_1}{\view_1 \viewjoin \view_2}}  \ISEP
      \val_1 = \val_2
    }
\end{mathpar}

By requiring the view $\altview$ stored at~$\atcell$ to
satisfy $\view\viewleq\altview$, as opposed to the equality $\view=\altview$,
we make the points-to assertion anti-monotonic in its view
parameter: that is, if $\view_1\viewleq\view_2$ holds,
then $\pointstoAT[q] {\atcell} {\mkval\val{\view_2}}$
entails $\pointstoAT[q] {\atcell} {\mkval\val{\view_1}}$.
%
This seems convenient in practice, as it gives the user a concise way
of retaining partial knowledge of the view that is stored at cell~$\atcell$.
%
On the other hand,
we do lose the ability of expressing negative information about the view,
that is, an upper bound on the view. This should not be a problem in practice,
as one always reasons about events that must be part of a view,
and never about events that must not.
In fact, in the high-level logic presented in the next chapter,
assertions are \emph{monotonic} functions of a view,
so this style of reasoning becomes built-in.

\subsection{Block length}
\label{sec:llog:assertions:haslength}

We have placed the assertion $\ownGhost\gnamelen{\authfull\storetolencmra\store}$
in the state interpretation.
This lets us define an assertion representing the knowledge of the length of a given block:
%
\[
  \haslength\loc\nbelems
  \;\eqdef\;
  \ownGhost \gnamelen {\authfrag {\mapsingleton\loc{\nbelems}}}
\]
%
This assertion is persistent (\sref{sec:iris:pers})
and achieves agreement on the length of a block:
%
\begin{mathpar}%
  \infer
    {\haslength\loc\nbelems  \IISEP  \haslength\loc{\nbelems'}}
    {\liftpure{\nbelems = \nbelems'}}
\end{mathpar}%
%
We can use it to define a points-to assertion for entire blocks as sugar,
where the $b_i$ are either histories or pairs of a value and a view:
%
\[
  \arraypointstoANY[q] \loc {\listliteral{b_0, \dots, b_{\nbelems-1}}}
  \;\eqdef\;
  \haslength\loc\nbelems
  \isep
  \Sep_{0 \leq \offset < \nbelems} \pointstoANY[q] {\locoff\loc\offset} {b_\offset}
\]
%
Again, when omitted the fraction~$q$ is taken to be~$1$, i.e.\ full ownership.
%
Thanks to the length information, this block-points-to assertion achieves
agreement on the value of a block:
%
\[
  \arraypointstoANY[q] \loc b   \IISEP
  \arraypointstoANY[q'] \loc b'
  \IEQUIV
  \arraypointstoANY[(q+q')] \loc b  \IISEP
  \liftpure{b = b'}
\]
%
Without the length information, we would only be able to deduce that
the shortest list among $b$ and $b'$ is a prefix of the longest list.

As will be seen later (rule~\baselength in \fref{fig:llog:rules:mem}),
the length assertion is what we need for giving a specification to
the operation of the language that computes the length of a block.
By using the frame rule (\hoareslframe), we will be able to derive another specification
for this operation, that takes as precondition a block-points-to assertion
rather than a length assertion:
%
\begin{mathpar}
\hoare
  {\arraypointstoANY[q] \loc b}
  {\mkexpr{\Length\loc}{\thisview}}
  {\Lam \mkval{\val'}{\thisview'}. \isepV{%
     \liftpure{\val' = \length b} \cr
     \liftpure{\thisview' = \thisview} \cr
     \arraypointstoANY[q] \loc b
  }}
\end{mathpar}

\subsection{Validity of a view}
\label{sec:llog:assertions:validview}

The last part of the state interpretation
(\sref{sec:llog:instantiation})
asserts the existence of a view $\globalview$
such that items~(\ref{gvi2}), (\ref{gvi3}) and (\ref{gvi4})
of the global view invariant hold (\sref{sec:mocaml:lang:semantics}).
It also includes the ghost-state-ownership assertion
$\ownGhost{\gnameglobalview}{\authfull\globalview}$.
%
We now define the ``valid-view'' assertion as follows:
%
\[
  \valid\view
  \;\eqdef\;
  \ownGhost\gnameglobalview
    {\authfrag\view}
\]

The assertion $\valid\view$ guarantees that $\view$ is a fragment of,
or lower bound on, the global view;
that is, $\view\viewleq\globalview$ holds.
%
Because the camera of views is idempotent (that is, $\view\viewjoin\view=\view$),
this assertion is persistent (\sref{sec:iris:pers}).
Intuitively, updates to the authoritative fragment~\(\authfull \globalview\)
must preserve the ``frame'',
which consists of the existing assertions~\(\valid \view\).
Hence, once \(\valid \view\) holds, it holds forever.
This conveys the idea that the global view~$\globalview$ only grows over time,
with more write events.

A validity assertion appears in almost all of the reasoning rules that we
present in the next subsection (\sref{sec:llog:rules}):
the rules effectively require (and allow) every thread to keep track
of the fact that its current view is valid.
This encodes item~(\ref{gvi1}) of the global view invariant.
%
The root cause of this phenomenon lies in the rule \basenaread, where the
global view invariant must be exploited in order to prove that a non-atomic
read instruction can make progress.
Then, the other rules must preserve that invariant.

In order to initiate the verification of a full program, a user then
needs the validity of the initial (empty) view. This is easily obtained
through the following theorem, whose proof is trivial.

\begin{theorem}[Validity of the empty view]
The entailment $\vdash \pvs \valid \emptyview$ holds in \llog.
\end{theorem}

% ------------------------------------------------------------------------------

% Specific deduction rules for \mocaml.

\section{\mocaml-specific rules}
\label{sec:llog:rules}

\input{fig-llog-rules}

There remains to give a set of \mocaml-specific deduction rules that allow
establishing Hoare triples, reflecting the operational semantics of \mocaml.
\fref{fig:llog:rules:nomem} shows standard rules \citep[\S6.2]{iris},
that do not interact with the shared memory,
while \fref{fig:llog:rules:mem} shows the rules that govern the memory access operations.
%
In these rules, some preconditions are prefixed with the ``later'' modality of Iris.
In a first reading, we can ignore them.
See \citet[\S6.2]{iris} for details.

A program that is already a value performs no computation at all (\baseval),
and its return value is itself.

Running a pure step of computation (\basepure) affects neither the ghost state nor the thread’s view.
Therefore, if $\expr \exprReduces{\noMemEvent} \expr'$ is the unique possible
reduction step starting from~$\expr$,
then proving a specification about $\expr'$ with some view~$\thisview$
suffices to prove the same specification about $\expr$ with the same view~$\thisview$.

Running a ``fork'' operation (\basefork) returns the unit value
and spawns a new thread whose return value is ignored.
Still, safety of the full program requires safety of the newly-spawned thread $\expr$,
hence the rule requires proving a triple about $\expr$ with a trivial
postcondition.
The newly-spawned thread inherits the view~$\thisview$ of its parent thread.

Rules in \fref{fig:llog:rules:mem} are ``small axioms''~\citep{ohearn-19}, that is,
triples that describe the minimum resources required by each operation.
%
Each of them is just a single triple $\hoare\prop{\mkexpr\expr\thisview}\pred$,
which, for greater readability, we display vertically:
%
\begin{mathpar}
  \hoareV
    {\prop\qquad}
    {\!\mkexpr\expr\thisview}
    {\pred\qquad}
\end{mathpar}

\pagebreak % final layout tweak

\subsection{Operations on non-atomic cells}

\paragraph{Allocation of non-atomic cells}

Allocating a non-atomic memory block (\basenaalloc)
returns a memory location~$\loc$ and does not change the view of
the current thread. The points-to assertion
$\arrayhistNA\loc{\listliteral{\mapliteral{\mapbinding 0\val}, \dots, \mapliteral{\mapbinding 0\val}}}$
in the postcondition
represents the full ownership of all cells of the newly-allocated memory block
and guarantees that their histories contain a single write
of the value~$\val$ at timestamp~$0$.

From the previous rule,
it is easy to derive a simplified rule for the allocation of non-atomic references (\basenaallocref).

\paragraph{Non-atomic read}

Reading a non-atomic memory cell (\basenaread) requires (possibly shared)
ownership of this memory cell, which is why the points-to assertion
${\histNA[q]\nacell\hist}$ appears in the precondition.
%
What can be said of the value~$\val'$ produced by this instruction?
%
A non-atomic read can read from any write whose timestamp is high enough,
according to this thread's view of the cell~$\nacell$. Thus, for some
timestamp~$\timest$ such that $\timest \in \Dom\hist$ holds (i.e., $\timest$ is a
valid timestamp) and $\viewapp\thisview\nacell \timeleq \timest$ holds (i.e., the
view $\thisview$ allows reading from this timestamp), the value~$\val'$ must
be the value that was written at time $\timest$, that is, $\histapp\hist\timest$.
%
The thread's view is unaffected,
and the points-to assertion is preserved.

In order to justify a non-atomic read, the validity of the current view is
required: the assertion $\valid\thisview$ appears in the precondition of
\basenaread. Without this requirement, we would not be able to establish this
triple. Indeed, we must prove that this read instruction can make progress,
that is, it cannot be stuck. In other words, we must prove that the
history~$\hist$ contains a write event whose timestamp is at least
$\viewapp\thisview\nacell$. Quite obviously, in the absence of any hypothesis about
the view~$\thisview$, it would be impossible to prove such a fact.
%
Thanks to the validity hypothesis $\valid\thisview$,
we find that that $\thisview$ must be a fragment of
the global view~$\globalview$. This implies
$\viewapp\thisview\nacell \timeleq \viewapp\globalview\nacell$.
Furthermore, the state interpretation guarantees
$\viewapp\globalview\nacell \in \Dom\hist$.
%
Therefore, this read instruction can read (at least)
from the write event whose timestamp is $\viewapp\globalview\nacell$.
Therefore, it is not stuck.

\paragraph{Non-atomic write}

Writing a non-atomic memory cell (\basenawrite) requires exclusive
ownership of this memory cell, which is expressed by the points-to
assertion $\histNA\nacell\hist$. It also requires the validity of the current
view~$\thisview$, as this information is needed to prove that
the updated view~$\thisview'$
is valid. In accordance with the operational semantics, the history~$\hist$ is
extended with a write event at some timestamp~$\timest$ that is both fresh
for the history~$\hist$ and permitted by the view~$\thisview$. The updated
points-to assertion $\histNA\nacell{\mapupdate\hist\timest\val}$ reflects this
updated history. The thread's new view~$\thisview'$ is obtained by
updating~$\thisview$ with a mapping of~$\nacell$ to the timestamp~$\timest$.

%\pagebreak % final layout tweak

\subsection{Operations on atomic cells}

The rules that govern atomic memory cells are a little heavy, due to
the fact that atomic memory cells play two independent roles: they store a
value and a view. Regarding the ``value'' aspect, these rules are identical
to the standard rules of \CSL with fractional permissions, which reflects the
sequentially consistent behavior of atomic memory cells.
Regarding the ``view'' aspect, these rules describe how views are written and
read by the atomic memory instructions.
As can be seen, the stored view accumulates the knowledge of the writers
and transmits it to the readers.
This reflects the ``release/acquire'' behavior of these instructions
and is analogous to the fact that, in axiomatic memory models
such as that given by~\citeauthor{dolan-18} for \mocaml,
there is a happens-before relation
from a write operation to a read operation which reads from it.

\paragraph{Allocation of atomic cells}

When an atomic memory block is allocated (\baseatalloc),
its cells are initialized with the view~$\thisview$ of the current thread.
This is expressed by the points-to assertion
$\arraypointstoAT \loc {\listliteral{\mkval\val\thisview, \dots, \mkval\val\thisview}}$
in the postcondition.
%
Maintaining the global view invariant (\sref{sec:mocaml:lang:semantics}) requires that
the views of the new atomic cells be contained in the global view, that is,
that these views are valid;
for this reason, $\valid\thisview$ is a precondition of this rule.

Again it is easy to derive a simplified rule for the allocation of atomic references (\baseatallocref).

\paragraph{Atomic read}

When an atomic memory cell is read (\baseatread),
the view stored at this cell
is merged into the view of the current thread:
this is an ``acquire read''.
This is expressed by the inequality
$\viewatloc \viewjoin \thisview \viewleq \thisview'$.
We cannot expect to obtain an equality
$\viewatloc \viewjoin \thisview = \thisview'$
because, according to our definition of the atomic points-to assertion
(\sref{sec:llog:assertions}),
$\view$ is only a fragment
of the view that is stored at cell~$\atcell$.
Thus, we get only a lower bound on the
thread's new view~$\thisview'$.
This does not hinder practical expressiveness.
Nevertheless, we can prove that $\thisview'$ is valid.

\paragraph{Atomic write}

When performing an atomic write (\baseatwrite),
the same ``acquisition'' phenomenon occurs: we get
$\viewatloc \viewjoin \thisview \viewleq \thisview'$.
Furthermore, this thread's view is merged into the view
stored at this cell: this is a ``release write''.
This is expressed by the updated points-to assertion
$\pointstoAT \atcell {\mkval\val{\viewatloc \viewjoin \thisview}}$.

\paragraph{(Atomic) Compare-and-set}

The two rules associated with the CAS instruction respectively describe the
case where this instruction fails (\basecasfailure) and the case where it
succeeds (\basecassuccess). These rules express the idea that a~CAS behaves
as a read followed (in case of success) with a write.

%\pagebreak % final layout tweak

\paragraph{}

In this chapter, we have built a program logic for reasoning about \mocaml programs.
Compared to working with the bare operational semantics (\sref{sec:mocaml}),
the benefits of \llog are clear.
%
We have at our disposal a rich specification language
which contains the host logic (that of Coq)
and an expressive ghost state framework.
%
A sound---and mechanized---proof system lets us prove program safety
and partial functional correctness.
%
By the virtue of \CSL,
specifications are reduced to their minimal footprints
and their proofs are composable:
a program fragment is specified and verified in isolation.
Interference by other program fragments need not be considered.
In particular, when verifying a program with several threads,
we do not need to consider the many possible interleavings
of the instructions of concurrent threads.

However, the program logic that we have just presented is still low-level,
as it exposes tedious details of the \mocaml memory model.
Several aspects of it can be criticized:
%
\begin{enumerate}
\item
\label{enum:history}
The assertion $\histNA[q]\nacell\hist$ exposes the fact that a non-atomic memory
cell stores a history~$\hist$, as opposed to a single value~$\val$. The
rules \basenaread and \basenawrite paraphrase the operational semantics and
reveal the timestamp machinery. This makes it difficult to reason
% in a lightweight and natural manner
about non-atomic memory cells. Yet, at least
in the absence of data races on these cells, one would like to reason in a
simpler way. Is it possible to offer higher-level points-to assertions and
rules, so that a non-atomic cell appears to store a single value?
\item
\label{enum:explicitview}
The view~$\thisview$ of the current thread is explicitly named in every triple
$\hoare\prop{\mkexpr\expr\thisview}\pred$, and its validity is typically
explicitly asserted as part of every pre- and postcondition. This seems heavy.
Is it possible to make this view everywhere implicit by default, and to have a
way of referring to it only where needed?
\end{enumerate}

In the next chapter, we answer these questions in the affirmative.
