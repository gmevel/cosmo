\chapter{Related Work}
\label{sec:related}

\section{Program verification in weak memory models}
\label{sec:related:cslweak}

There is a wide variety of work on weak memory models and on
approaches to program verification with respect to weak memory models.
We restrict our attention to program verification based on extensions
of \CSL, because this is the most closely related work and because we
believe that the abstraction and compositionality of \SL are features
that will be absolutely essential in the long run.
%
% Indeed, the complexities of fine-grained concurrent data structures
% and weak memory must be hidden from the end user.
%
\citet{vafeiadis-cav-17} offers a good survey of some of the papers that we cite now.

% All of the papers that we cite have been verified in Coq...

% ------------------------------------------------------------------------------

% RSL (OOPSLA 2013).

The first instance of \SL for weak memory appears to be RSL
\citep{vafeiadis-narayan-13}.
%
It is based on an axiomatic semantics of a fragment of the C11 memory model.
%
It supports non-atomic accesses, where it enforces the absence of data races;
release/acquire accesses, with reasoning rules that allow ownership transfers
from writer to reader; and relaxed accesses, without any ownership transfer.
%
The logic involves a permission $\mathit{Rel}(\ell, Q)$ to perform a release write
at location $\ell$ of a value $v$ while relinquishing the assertion $Q\;v$.
Symmetrically, there is a permission $\mathit{Acq}(\ell, Q)$ to perform an
acquire read at location $\ell$ of a value~$v$ and obtain the assertion~$Q\;v$.
The release and acquire permissions are created, and the predicate $Q$ is fixed,
when the location is allocated. % (page 7)
%
RSL~can verify simple concurrent data structures, such as a spin lock.
However, because it lacks invariants and ghost state, its expressive
power is limited.
% fp: Faut-il une comparaison spécifique avec \hlog?
% jh: on dit déjà quelque chose (pas d'invariants/ghost state), et en
% rajouter prendrait de la place et risquerait d'introduire des
% comparaisons discutables.

% The abstract says that the paper supports SC atomics with arbitrary
% ownership transfer, but this is actually incorrect; SC atomics are not
% supported at all. This is noted in the conclusion of the RustBelt Relaxed
% paper.

% Rel(l, Q) is a permission to do a release write at location l of
%   any value v that satisfies Q v.
% This permission seems duplicable. (Rel-Split.)

% Acq(l, Q) is a permission to do an acquire read at location l
%   and find a value v that satisfies Q v.
% Not duplicable, but splittable by splitting Q.
%
% Somewhat heavy and ad hoc rules for CAS.

% Formalized in Coq.

%  According to the GPS paper,
%  ``RSL supports simple, high-level reasoning about resource invariants
%    and ownership transfer [...] but provides no support for ghost state
%    or more complex forms of protocols and ownership transfer''.

% The spin lock seems to work only because it involves release/acquire
% writes at a single location, so one can express the invariant in the
% formula Q carried by the Rel and Acq permissions.

% ------------------------------------------------------------------------------

% FSL and FSL++. (Cited here because they descend from RSL.)

FSL \citep{doko-vafeiadis-16} extends RSL with support for release/acquire
fences. A release write can be replaced with a release fence followed with a
relaxed write; symmetrically, an acquire read can be replaced with a relaxed
read followed with an acquire fence. Two new assertions $\Delta P$ and $\nabla
P$ witness that $P$ has been released by the last release fence or will be
acquired by the next acquire fence, respectively.
%
% Comparaison spécifique avec nous:
In \hlog, both of these assertions would be replaced by $\prop\opat\view$, for
a well-chosen view~$\view$. \mocaml has no fences; instead, atomic read and
write instructions are used to transmit views.
%
FSL++ \citep{doko-vafeiadis-17} extends FSL with shared read permissions for
non-atomic accesses, support for CAS, and ghost state.
As an application, the authors of FSL++ prove the correctness of the Rust
library ``ARC'' (atomic reference counter).

% FSL (VMCAI 2016).
%   Descends from RSL.
%   Non-Atomics + Atomics (release/acquire, relaxed).
%   Rel(l, Q) is a permission to do a release write at location l of
%     any value v that satisfies Q v.
%   This permission also allows performing a relaxed write,
%   but in that case, it must be helped by a release fence.
%   The postcondition $\Delta P$ of a release fence
%   is a witness that $P$ has been made ready for transfer
%   (therefore cannot be used any more by this thread).
%   Symmetrically, Acq(l, Q) allows performing an acquire read.
%   A relaxed read can be used with the help of an acquire fence.
%   The assertion $\Nabla P$ means that after the next acquire fence
%   we will have $P$.

% FSL++
%   ``Even though FSL supports relaxed accesses and memory fences, it lacks
%     some key features which makes it inapplicable beyond simple toy examples.''
%   FSL++ extends FSL with fractional read permissions for non-atomic accesses,
%   support for CAS, and ghost state.
%   As an application, they prove the Rust library ARC (atomic reference counter).
%   FSL++ is about 22Kloc of Coq.

% ------------------------------------------------------------------------------

% GPS (2014).

GPS \citep{turon-vafeiadis-dreyer-14} supports a fragment of C11 that includes
non-atomic accesses and release/acquire accesses. Like the papers cited above,
it is based on an axiomatic presentation of the C11 memory model.
%
It introduces ghost state and a notion of per-location protocol that governs
a single atomic memory location.
%
At the cost of rather complex reasoning rules, it offers good expressive
power. The case studies described in the paper include a spin lock, a bounded
ticket lock, a Michael-Scott queue, and a circular FIFO queue.
%
In comparison, \hlog does not need per-location protocols.
% for atomic memory locations.
Because atomic memory locations in \mocaml have sequentially
consistent behavior, our ``atomic points-to'' predicate is objective.
Therefore, in \hlog, an invariant can refer to one or more atomic memory
locations if desired. This has been illustrated in \sref{sec:examples:ticketlock}
and \sref{sec:examples:peterson}.
%
% fp: Il serait intéressant de regarder comment le ticket lock est prouvé
% en GPS ou iGPS.
%
On these examples, \hlog is significantly simpler to use than GPS: the proof of
the ticket lock in \hlog is about 170~lines of Coq code%
\footnote{\url{https://gitlab.inria.fr/cambium/cosmo/-/blob/master/theories/examples/ticketlock.v}}
(specifications and proofs combined) whereas the corresponding proof in iGPS (a
reconstruction of GPS, covered later) is about 700~lines%
\footnote{\url{https://gitlab.mpi-sws.org/FP/igps/-/blob/master/theories/examples/ticket_lock.v}}.

% For C11. Supports non-atomics and release/acquire atomics. No relaxed accesses.
% Roughly analogous to our setting, insofar as races on non-atomics
% are forbidden and synchronization must go through atomic locations.
% However, their atomics are weaker than ours, I think.
% Apparently based on an axiomatic semantics of C11. Formalized in Coq.
% Ghost state.
% Per-location protocols (for atomic locations).
% The client can assert a lower bound on the protocol state
%   (e.g., ``this location has been observed in state s'').
% Complex rules.
% They prove the spin lock example using a single-location invariant,
% that is, a single-location protocol with only one state.
% Escrow: a duplicable permission to abandon P and get Q
%   (both of which are exclusive).
% Case studies: Michael-Scott queue, circular buffers, bounded ticket lock.

% \citet{he-18}.
% From the FSL++ paper:
% He et al. [14] have proposed an extension of GPS with FSL-style modalities,
% to give it support for relaxed accesses and memory fences. As the original
% FSL, this extension of GPS does not have support for atomic updates, which
% makes it inapplicable to programs like ARC. Additionally, unlike FSL, this
% extension of GPS lacks a soundness proof.

% ------------------------------------------------------------------------------

\citet{sieczkowski-15}, already cited earlier (\sref{sec:intro}), present
iCAP-TSO, a variant of \CSL that is sound with respect to the TSO memory model.
The logic includes a high-level fragment whose reasoning rules are those of
\CSL, where the pre- and postcondition are interpreted relative to the current
thread’s point of view.
%
Informally, an assertion holds from the point of view of a thread if it holds
of the global state updated with the thread's store buffer and no other thread
has pending writes that could affect this assertion. Thus, the subjective
points-to assertion $x \mapsto v$ guarantees that a read instruction will
return the value~$v$.
%
The logic is proved sound with respect to an operational semantics where store
buffers are explicit. This work and ours seem quite close in spirit, but differ
due to the choice of a different memory model. In particular, Sieczkowski et
al.\ have no notion of view. In order to reason about the behavior of store
buffers, they propose a few ad hoc logical constructs, such as an ``until''
modality $P \mathrel{\mathcal{U}_t} Q$, which means that $P$ holds until an
update from thread $t$ is flushed to main memory, at which point $Q$ holds.
%
By contrast with \hlog,  iCAP-TSO’s high-level logic cannot reason about
transfers of ownership. This kind of reasoning must be carried out in
a lower-level logic. It is possible to verify a data structure (e.g., a lock) in
the low-level logic and to establish a specification expressed in the high-level logic.

% iCAP-TSO.
%
% In their ``SC'' logic, they use standard \SL triples, where the pre- and
% postcondition are interpreted relative to the current thread's
% point-of-view. Informally, an assertion holds from the point of view of a
% thread if it holds of the global state updated with the thread's store
% buffer and no other thread has pending writes that could affect this
% assertion. Thus, the subjective points-to assertion $x \mapsto v$ guarantees
% that a read instruction will return the value~$v$.
%
% The ``SC'' logic cannot reason about transfers of ownership. This kind of
% reasoning must be carried out in a lower-level ``TSO'' logic. It is possible
% to verify a data structure (e.g., a lock) in the logic ``TSO'' and to
% establish a spec expressed in ``SC'', which can be used by ``SC'' clients.
%
% The spec of locks is restricted to "stable" invariants.
%
% The verification of the spin lock (which uses a single release write to
% release the lock) relies on the idea that ``by the time the buffered release
% write makes it to main memory, [the resource invariant] holds objectively''.
% More precisely, in the unlocked state, the lock invariant states that
% either locked is false in memory and R holds
% or there is a release write on the way and by the time it reaches
%   main memory, R holds.
%
% Built on top of iCAP.
%
% (Section 4) An assertion P in the logic SC can be embedded as
% $\lceil r P \kw{in} t\rceil$ in the logic TSO; this means $P$ holds in the
% eyes of the thread $t$, as described above. There is another embedding
% $\lceil P\rceil$ which means that $P$ holds in the eyes of every thread.
%
% The subjective embedding $\lceil r P \kw{in} t\rceil$ is used to give
% meaning to SC triples on top of TSO triples.
%
% The logic TSO also has an "until" operator, $P \mathcal{U}_t Q$, which
% means $P$ holds until (at a certain time) an update from thread $t$ is
% flushed to main memory, at which point $Q$ holds. This assertion is
% *unstable*, as the write could be flushed at any time. There is an
% explicit stabilization operator (page 15) that turns any assertion P
% into its stabilized version.
%
% Assertions are relative to a thread ID.
% The global state of the TSO logic includes the pool of store buffers.

% ------------------------------------------------------------------------------

% iGPS and iRSL.

\citet{kaiser-17} propose the first instantiation of Iris in a weak-memory
setting. This involves abandoning the axiomatic memory models used by the
papers cited above and switching to an operational semantics. The
paper proposes such a semantics for a fragment of C11 that includes two kinds
of memory locations, namely non-atomic locations and release/acquire locations.
Like Dolan et al.'s semantics of \mocaml (\citeyear{dolan-18}), this semantics
involves timestamps and histories. It also includes a ``race detector'' which
ensures that data races on non-atomic memory locations lead to undefined
behavior. In comparison, Dolan et al.'s semantics does not need a race
detector: every \mocaml program has a well-defined set of permitted behaviors.
%
Several aspects of our work are modeled after Kaiser et al.'s paper. Indeed,
in a first step, they instantiate Iris, yielding a low-level ``base logic''.
Then, in a second step, they define a higher-level logic, whose
reasoning rules are easier to use, and whose assertions are implicitly
parameterized with the thread's view. We follow this approach.
%
Kaiser et~al.\ construct not one, but two high-level logics, iGPS and iRSL,
which are inspired by GPS and RSL, and benefit from the power of Iris.
%
iGPS introduces a new feature, namely single-writer protocols.
%
\hlog follows a different route: as explained above, it does not need
per-location protocols. Furthermore, it puts emphasis on explicit user-level
reasoning with abstract views, via assertions like $\prop\opat\view$ and
$\seen\view$.
%
% iRC11 also involves reasoning about views, but only in some of the
% intermediate-level layers.

% Reconstruct GPS and RSL on top of Iris.
%
% This time, based on an operational semantics of the non-atomic+RA
% fragment of C11. The semantics includes a ``race detector'' so
% that data races on non-atomics are considered unsafe.
% (In contrast, we do not need a race detector.)
% They also have a two-level construction: base logic, iGPS or iRSL.
% In the high-level logic, assertions are also parameterized with a view.
%
% At the low level, the predicate Hist(l, h) is very much like \llog's
% non-atomic points-to predicate.
%   But the history h records only the writes of the current era.
% Seen(pi, V) is different from what we have, as it mentions a thread id.
%
% iGPS has a new feature: single-writer protocols.
%
% New in \hlog, explicit reasoning about (abstract) views.

% ------------------------------------------------------------------------------

iRC11 \citep{dang-20} extends iGPS with additional features of the C11 memory
model, namely relaxed accesses and release/acquire fences. It is based on
ORC11, an operational presentation of the Repaired C11 memory model
\citep{rc11}. One of its key features is support for cancellable invariants, an
abstraction whose implementation in Iris in an SC setting has been
well-understood for some time,\footnote{%
For example, in RustBelt~\citep{rustbelt-18},
non-atomic persistent borrows are a form of cancellable invariant.}
but whose implementation in a weak-memory
setting is significantly more challenging. In particular, the tokens that
represent a fraction of the ownership of an invariant are not objective, and
therefore cannot appear in an invariant; they must be transmitted from one
thread to another via a synchronization operation. Dang et al.'s
implementation of cancellable invariants involves explicit reasoning about
views, yet this is not apparent in the cancellable invariant API,
a~remarkable achievement.
A user of iRC11 need not know about views.

% The rules of ``raw cancellable invariants'' involve views
% and a ``view-join modality''.

% fp: Should we say that we do not anticipate a great need for
% cancellable invariants in our setting because \mocaml is equipped with a
% garbage collector?
% jh: ce n'est pas clair. Il y a des ressources qui ne sont pas gérées
% par le garbage collector. Et puis on peut avoir besoin de prêter des
% ressources en les réutilisant vraiment après (i.e., c'est un peu
% comme les ressources read-only de votre papier).

% fp: Should we say that (if desired) we can use standard Iris-SC
% cancellable invariants, with an objective assertion inside?
% jh: non : l'intérêt des invariants cancellables dont on parle ici,
% c'est de synchroniser au moment où on annule l'invariant.

On top of iRC11, Dang et al.\ reconstruct Lifetime Logic and the model of the
Rust type system previously built by Jung et al.\ in an SC setting
(\citeyear{rustbelt-18}). Furthermore, they prove the soundness of Rust's
ARC library.

% The rules for fences use \Delta and \nabla as in FSL.

% It has single-location invariants. (Single-location protocols are not
% described, for simplicity.)

% This requires adapting cancellable invariants to the weak-memory setting.
%   (No previous work did so.)
% One key difficulty is that the tokens that represent (fractional) ownership
% of a cancellable invariant cannot be ghost state, otherwise they would be
% objective, therefore freely transferrable via invariants, which would be
% unsound. Instead, the authors propose ``synchronized ghost state'', that
% is, ghost state tokens implicitly keep track of the view of the thread
% that owns the token. Runtime synchronization is required to transfer such
% a token from one thread to another.
%
% Dang et al. reconstruct Lifetime Logic, and the model of the Rust type
% system, on top of iRC11. They prove several Rust libraries correct.

% ------------------------------------------------------------------------------

% Not cited:

% https://ilyasergey.net/other/CSL-Family-Tree.pdf

% Tom Ridge. A rely-guarantee proof system for x86-TSO. VSTTE 2010.
% Not Separation Logic.

% Summers-Muller:
% http://pm.inf.ethz.ch/publications/getpdf.php?bibname=Own&id=SummersMueller18.pdf
% -- want to automate the use of several relaxed logics

% Verification under causally consistent shared memory
% https://www.cs.tau.ac.il/~orilahav/papers/siglog19.pdf
% (Short survey.)

% ------------------------------------------------------------------------------

\section{Verification of fine-grained concurrent data structures}

Putting weak-memory concerns aside, the verification of fine-grained concurrent data structures is a well-studied problem with a particularly rich literature.
Several approaches are tried, targeting various verification frameworks, various data structures in different contexts.

The  notion of \emph{linearizability} is traditionally regarded as central for specifying such libraries.
\citet{dongol2015verifying} give a survey of the different techniques used to prove linearizability of concurrent libraries.
Of particular interest in the context of separation logic is the technique of \emph{logical atomicity}. In a sequentially consistent context, logically atomic specifications have recently been proved to imply linearizability~\citep{gueneau2021theorems}.
Logical atomicity has been developed through several iterations over the last decade~\citep{da2014tada,iris-15,jacobs2011expressive,svendsen-birkedal-parkinson-hocap-13,jung-prophecies-20}.
In this dissertation, we adapt a modern version of Iris's logically atomic triples~\citep{jung-slides-2019} to the setting of \cosmo{}.
%This is, to the best of our knowledge, the first use of logical atomicity in a weakly consistent setting.

% TODO : je ne sais pas quoi dire de ça. Dans la mesure où ce n'est qu'un preprint arxiv, je propose de ne pas en parler pour l'instant.
% Concurrent Data Structures Linked in Time
% Delbianco et al.
% https://germand.github.io/pubs/relink-ECOOP17.pdf
% c'est de la logique de séparation, avec preuve de linéarisabilité,
% et état fantôme pour l'historique

Another popular approach for proving the correctness of concurrent libraries is the use of refinement with respect to a simpler implementation.
This is the track chosen by ReLoC~\citep{reloc}, which has recently been combined with logical atomicity~\citep{frumin2020reloc}.
Interestingly, ReloC has been recently used for proving the correctness of several concurrent queue implementations~\citep{vindum-birkedal-21,vindum-frumin-birkedal-21}, one of which is very close to ours.
However, these proofs do not handle relaxed memory behaviors, so that they do not provide a solution to the problem of specifying the lack of happens-before relationship between some data structure accesses, which we discussed in \sref{sec:queue:spec:weak}.
Because it lacks some happens-before relationships, our queue implementation \emph{is not} a refinement of a naive implementation which would use a lock to protect a sequential implementation, so a refinement-based approach would not be useful for proving our library correct.
The refinement approach has also been used to prove correct some data structures used in a concurrent garbage collector~\citep{zakowski2018verified}.


In a weakly consistent setting, new problems arise.
As discussed in \sref{sec:queue:spec:weak}, even the definition of linearizability needs special care.
\citet{smith2019linearizability} propose new definitions of linearizability for the case of weak memory models.
% Ce papier est le seul sur le sujet qui est vraiment publié ailleurs qu'arxiv. Vu que je n'ai pas lu les autres, je préfère ne pas les mentionner, de peur que ce soit n'importe quoi:
% On Library Correctness under Weak Memory Consistency
% https://www.soundandcomplete.org/papers/Libraries-POPL-2019.pdf
% Causal Linearizability, Doherty and Derrick 2016
% Des mêmes auteurs: https://arxiv.org/abs/1810.09612v1
In contrast, other authors develop new kinds of specifications which allow for weak behaviors of the library itself~\citep{krishna-emmi-enea-jovanovic-20,emmi-enea-19,raad-19}.
Notably, \citeauthor{raad-19} propose a compositional framework in which the specification of a library describes the events associated to the library and their allowed orderings. In other words, each library interface describes its own axiomatic memory model, and there is no built-in model: models such as RC11 and TSO are implemented as the specification of a library of memory access primitives. \citet{dang2022compass} use this approach to verify a finer specification for a concurrent queue similar to ours. This formalism is heavier than \hlog---as it involves manipulating event graphs---and might be more expressive, although that point should be investigated.
We found that our method of combining logically atomic triples with views is expressive and allows for concise specifications at the same time.
%% % Constantin Enea, "Verifying Visibility-Based Weak Consistency".
%% % Technique de preuve à base de simulations (ce n'est pas de la
%% % logique de séparation), avec mise en oeuvre basé sur des prouveurs
%% % automatiques.
%% \nocite{krishna-emmi-enea-jovanovic-20}
%% \nocite{emmi-enea-19}
%% \begin{comment}
%% % Extrait de mon journal, POPL 2019:
%% Constantin Enea. "Weak-Consistency Specification via Visibility Relaxation".
%% Objets concurrents (dans le JDK par exemple) ont des specs faibles. Par
%% exemple, la méthode size() sur une collection renvoie un résultat plus ou
%% moins fiable s'il y a des updates en parallèle. Il cherche une façon simple et
%% générique de spécifier ces comportements relâchés. Approche axiomatique:
%% parler de linéarisation, mais parler aussi de visibilité. Pour chaque
%% linéarisation des opérations précédentes, on se demande lesquelles sont
%% visibles. Je n'y comprends pas grand-chose. Il y a des relations po, hb, etc.
%% En gros, c'est un modèle mémoire faible axiomatique, mais pas pour la mémoire
%% primitive; pour un objet concurrent avec des opérations add, remove, size,
%% etc. Tout ça donne des version relâchées de la notion de linéarisabilité, je
%% suppose. Ensuite, il y a du test; il veut annoter des méthodes du JDK avec des
%% specs dans son langage, et détecter (par le test) si ces specs sont bien
%% respectées. Je ne suis pas. Jens Palsberg demande comment on peut vérifier un
%% client qui utilise plusieurs objets différents avec des specs différentes.
%% Réponse pas très claire.
%% \end{comment}
Previous works include the generalization of various methods to weak consistency: \citet{le2013correctws,le2013correctfifo} use manual methods directly tied to the axiomatic memory model to prove the correctness of a queue and of a work-stealing algorithm, while \citet{lahav2015owicki} adapt the Owicki-Gries methodology to the release-acquire fragment of the C11 memory model and apply it to verify a read-copy-update library.
Although various earlier works develop the idea of a separation logic for programs with a relaxed memory semantics, as shown in~\sref{sec:related:cslweak},
few of these papers address the problem of the full functional correctness of a data structure.
In particular, the specification proposed for a circular buffer in GPS~\citep{turon-vafeiadis-dreyer-14} is a weak specification in the style of the persistent specification given at the end of \sref{sec:queue:spec:sc}: in contrast to ours, it does not specify in which order the elements leave the queue.


% TODO pour plus tard:



% Approche semi-automatisée sans mémoire faible:
% Automated Verification of CountDownLatch
% https://arxiv.org/abs/1908.09758
% Approche semi-automatisée avec mémoire faible (FSL automatisée dans Viper):
% https://www.cs.ubc.ca/~alexsumm/papers/SummersMueller18.pdf
%   => Dans les deux cas : pas pertinent pour ce papier. cela ne parle pas de véification de structure de données, ni d'atomicité logique


% model-checking
% https://www.cis.upenn.edu/~alur/Pldi07.pdf

% Checking Concurrent Data Structures Under the C/C++11 Memory Model
% https://dl.acm.org/doi/10.1145/3155284.3018749
% => model checking pour les structures de données en mémoire faible

% Verifying linearizability with hindsight
% O'Hearn et al.
% plus tard, cf. Guerraoui et al.
% et plus tard,
% Order out of Chaos: Proving Linearizability Using Local Views
% https://arxiv.org/pdf/1805.03992.pdf
% et encore plus tard,
% Proving Highly-Concurrent Traversals Correct
% https://arxiv.org/pdf/2010.00911.pdf


% Dodds et al. (2015)
% A Scalable, Correct Time-Stamped Stack
% pas de logique de séparation je crois, mais une preuve (manuelle?) de linéarisabilité
% pas de mémoire faible (utilisation d'atomiques SC de C11)
%   => Ça me semble pas intéressant pour nous. En plus, le focus est surtout sur l'algorithme lui-même plutôt que sur sa preuve.

% TODO : dans le style raffinement, il y a aussi les articles de Turon

% POPL 2007, Modular verification of a non-blocking stack. parkinson-bornat-ohearn-07
% Il semblerait que la spec est très faible : la stack n'est en fait, je crois, qu'un bag de pointeurs.
% quand on fait push, il faut donner l'ownership du pointer qu'on y met, quand on fait pop, on récupère un pointeur, mais on ne sait rien dessus (pas de spécification fonctionnelle)
% TODO : quoi dire là-dessus. Pour l'instant, j'en parle pas, ça a l'aire vraiment trop éloigné de ce qu'on fait.
