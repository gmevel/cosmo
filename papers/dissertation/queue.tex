\chapter[A bounded MPMC queue]{A bounded multiple-producer multiple-consumer queue}
\label{sec:queue}

To confirm \hlog as a practical tool,
we now turn to studying a realistic concurrent data structure.
We demonstrate that \hlog lets us give a precise specification
to such a data structure
and lets us verify a non-trivial implementation of it.
%
A sequential data structure implementation can always be made robust in the face of concurrency
simply by guarding all of its opeations with a lock.
While such a \emph{coarse-grained} implementation of a concurrent data structure
is certainly correct,
%
there often exist implementations
which yield better performance, especially under heavy contention,
based on subtle \emph{fine-grained} memory accesses.
These implementations are delicate and
often rely on subtle properties of the memory model.
An informal correctness argument is difficult, likely unreliable, hence unconvincing.
Thus, concurrent data structures are prime candidates for formal verification.
Many machine-checked proofs of concurrent data structures have appeared in the literature 
already~\citep{parkinson-bornat-ohearn-07,reloc,frumin2020reloc,zakowski2018verified},
but relatively few verification efforts take place
in a weak-memory setting~\citep{le2013correctws,le2013correctfifo},
and fewer still rely on a modular methodology,
where a proof of a concurrent data structure
and a proof of its client (perhaps a concurrent application,
or another concurrent data structure)
can be modularly combined.

A concurrent queue is an archetypal example of a realistic concurrent data structure:
it is widely used in practice---%
for example to manage a sequence of tasks that are
generated and handled by different threads---%
and it admits fine-grained implementations.
%
In this chapter, we present a specification of a concurrent queue,
and we formally verify that a particular fine-grained implementation satisfies this specification.
While other such formalizations already exist in a sequentially-consistent
setting~\citep{vindum-birkedal-21,vindum-frumin-birkedal-21},
we consider a weak-memory setting.
Such a formalization effort is innovative and challenging in several aspects.

  First, weak memory models are infamous for the subtlety of the reasoning that they impose.
    In this regard, we believe that \hlog and the \mocaml memory model strike
    a good balance between the ease of reasoning enabled by the logic
    and the flexibility and performance allowed by the memory model.

  Second, the specification of the concurrent queue should indicate that it behaves
    as if all of its operations acted atomically on a common shared state,
    even though in reality they access distinct parts of the memory
    and require many machine instructions.
    To address this challenge, we use the recently-developed concept of 
    \emph{logical atomicity}~\citep{iris-15,jung-slides-2019,da2014tada},
    which we transport to the setting of \cosmo.
    %
    To the best of our knowledge, this is the first use of logical atomicity in a weak-memory setting.
    This raises new questions: for instance, even though our implementation
    realizes a total order on the operations on a queue,
    it offers strictly weaker guarantees than
    would be offered by
    a coarse-grained implementation.
    Indeed, in the context of a weak memory model,
    the specification of a concurrent data structure
    must describe not only the result of its operations,
    but also the manner in which these are synchronized,
    that is, the \emph{happens-before} relationships that exist between these.
    This additional information allows reasoning about accesses
    to areas of memory \emph{outside} of the data structure itself.
    This is crucial, for example,
    if a queue is used to transfer the ownership of a piece of memory
    from a producer to a consumer:
    there must exist a happens-before relationship between the \enqueue
    operation and the corresponding \dequeue operation,
    so as to ensure that the consumer acquires the producer's view of this piece of memory.
    Our specification faithfully captures a subtle behavior of the implementation:
    even though operations are totally ordered by logical atomicity,
    not all operations are ordered by happens-before---but \emph{some} are.

We believe our approach, whose key ingredients are Cosmo and logical atomicity,
scales to other memory models and other data structures.
Indeed, first, the core of Cosmo (beyond basic Separation Logic)
is a logic for reasoning with \emph{views},
an operational description of the memory model;
other memory models than that of \mocaml can also be termed in this fashion,
as iGPS~\citep{kaiser-17} and iRC11~\citep{dang-20} have demonstrated for C11.
Second, logical atomicity has already successfully been used
for various data structures in the Iris 
community~\citep{iris-examples,frumin2020reloc}.

The chapter opens with an explanation of the specification
of a concurrent queue~(\sref{sec:queue:spec}).
Then, we present an implementation of the queue in \mocaml~(\sref{sec:queue:impl}),
and explain our proof of its correctness~(\sref{sec:queue:proof}).
Next, we demonstrate that our specification is indeed usable,
by exploiting it in the context of a simple piece of client code,
where the concurrent queue is used to establish a pipeline between
a set of producers and a set of consumers~(\sref{sec:pipeline}).

\input{queue-spec}
\input{queue-impl}
\input{queue-proof}
\input{pipeline}
