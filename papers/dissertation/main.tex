\documentclass[a4paper, 11pt, twoside, openany]{book} % add "draft" option to spot overfull stuff
\usepackage[a4paper, textwidth=16cm, tmargin=4cm, bmargin=4cm]{geometry}

% Packages: general.
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french,english]{babel}
\usepackage{mlmodern} % a thicker variant of LatinModern

% Packages: links and bibliography.
\usepackage[numbered]{bookmark} % \bookmarksetup (this package includes hyperref)
\usepackage{natbib} % \citeyear, \citet, \citep
\usepackage{tocbibind} % include list of figures and bib in ToC and PDF bookmarks
% (option [nottoc] disable ToC in ToC, options [notlof,notlot] disable list of
% figures/tables in ToC)

% Packages: figures.
\usepackage{caption,subcaption} % subfigures
%\usepackage{graphicx}
\usepackage{tikz}
  \usetikzlibrary{positioning}
\usepackage{mathpartir}
  \renewcommand{\TirNameStyle}[1]{\hypertarget{#1}{\textsc{\small #1}}}

% Packages: misc.
\usepackage{comment}
\usepackage{xspace}
\usepackage{everypage} % \AddThispageHook for changing page geometry for single figure pages
\usepackage{epigraph}
\usepackage{fancyhdr}

% Packages: symbols.
\usepackage{amsmath}
\usepackage{amssymb}
%\usepackage{extpfeil} % fancy long arrows (has to be sourced before stmaryrd)
%\usepackage{stmaryrd}
\usepackage{iris}
\input{notations}

% recall a footnote with \cref (must be loaded after amsmath!)
\usepackage{cleveref}[2012/02/15]
  \crefformat{footnote}{#2\footnotemark[#1]#3}

% Header/footer styles.
% the header/footer style for most of this dissertation:
\renewcommand{\chaptermark}[1]{\MakeUppercase{\markboth{#1}{}}}
\renewcommand{\sectionmark}[1]{\MakeUppercase{\markright{#1}}}
\fancypagestyle{book}{%
  \fancyhead[EL,OR]{\thepage} % page number
  % chapter title:
  \fancyhead[ER]{\ifnum\value{chapter}>0\MakeUppercase{Chapter}~\thechapter.\ \ \fi\leftmark}
  % section title:
  \fancyhead[OL]{\ifnum\value{section}>0 \S\thesection.\ \ \fi\rightmark}
  \fancyfoot{} % no footer
  \renewcommand{\headrulewidth}{0pt} % no line
}
% the header/footer style for front matter pages,
% where chapter/section titles are not shown:
\fancypagestyle{headerwithouttitle}{%
  \fancyhead[EL,OR]{\thepage} % page number
  \fancyhead[ER,OL]{} % no chapter/section title
  \fancyfoot{} % no footer
  \renewcommand{\headrulewidth}{0pt} % no line
}

% Bibliography style.
% See here for styles compatible with natbib:
%   https://fr.overleaf.com/learn/latex/Natbib_bibliography_styles
% not all of them support \citet and friends.
\bibliographystyle{plainnat}
% Use round brackets or square brackets?
\setcitestyle{authoryear,round}
% Preserve the order of papers inside a \cite command.
%\setcitestyle{nosort}

% "theorem" environment.
\newtheorem{theorem}{Theorem}

% allow to input guillemots in French by typing their Unicode characters «/».
\frenchsetup{og=«,fg=»}

% ------------------------------------------------------------------------------
% Headings.

\title{%
  \vspace{-20mm}
  %
  % "Université Paris Cité" logo, on top-left corner,
  % absolute positioning that does not affect the layout of other stuff:
  % https://tex.stackexchange.com/questions/386324/place-image-in-absolute-position-independently-from-text
  \begin{tikzpicture}[remember picture,overlay,shift={(current page.north west)}]%
    \node[anchor=north west,xshift=10mm,yshift=-10mm]{%
      \includegraphics[width=4cm]{logo-UPC}%
    };%
  \end{tikzpicture}%
  % Inria logo:
  % https://www.inria.fr/fr/charte-dutilisation-de-lidentite-visuelle-dinria
  \begin{tikzpicture}[remember picture,overlay,shift={(current page.north)}]%
    \node[anchor=north,xshift=5mm,yshift=-9mm]{%
      % xshift > 0 because the Inria logo is asymetric
      \includegraphics[width=4cm]{logo-Inria}%
    };%
  \end{tikzpicture}%
  % LMF logo:
  \begin{tikzpicture}[remember picture,overlay,shift={(current page.north east)}]%
    \node[anchor=north east,xshift=-10mm,yshift=-9mm]{%
      \includegraphics[width=4cm]{logo-LMF}%
    };%
  \end{tikzpicture}%
  %
  {\LARGE Université Paris Cité} \\
  {\large École doctorale de sciences mathématiques de Paris-Centre (ED~386)} \\
  {\large Inria}
  \\[2cm]
  \textsc{\Huge
    A mechanized program logic \\
    for concurrent programs \\
    with the weak memory model \\
    of Multicore OCaml \\ % without that linebreak, spacing ABOVE last line is wrong
  }
}

\author{%
  \\[1cm]
  \hspace{-11mm} % for some reason the author is not exactly centered???
  {\LARGE\bf par Glen M\Large{}ÉVEL} % ugly hack for bold small-caps (\bf\smallcaps don’t work as-is because of mlmodern)
  \\[2cm]
  \hspace{-11mm}
  Thèse de doctorat d’informatique
  \\[8mm]
  \hspace{-11mm}
  \textit{dirigée par} François \textsc{Pottier}
  \\[8mm]
  \hspace{-11mm}
  \textit{présentée et soutenue publiquement le 14~décembre~2022}
  \\[8mm]
  \hspace{-11mm}
  \textit{devant un jury composé de :} \\[3mm]
  %
  \hspace{-11mm}
  \begin{tabular}{rll}
    Aleksandar \textsc{Nanevski}
      & associate research professor, IMDEA, Madrid
      & (rapporteur) \\[1mm]
    Mark \textsc{Batty}
      & professor, University of Kent
      & (rapporteur) \\[1mm]
    Azalea \textsc{Raad}
      & assistant professor, Imperial College, Londres
      & (examinatrice) \\[1mm]
    Stephen \textsc{Dolan}
      & software developer, Jane Street, Londres
      & (examinateur) \\[1mm]
    Ralf \textsc{Treinen}
      & professeur des universités, Université Paris-Cité
      & (examinateur) \\[1mm]
    François \textsc{Pottier}
      & directeur de recherches, Inria, Paris
      & (directeur) \\[1mm]
    Jacques-Henri \textsc{Jourdan}
      & chargé de recherches,
        CNRS \&
      & (co-encadrant,
    \\[0mm]
      & Laboratoire Méthodes Formelles, Gif-sur-Yvette
      & \hphantom{(}membre invité)
  \end{tabular}
  \vspace{-30mm}
}

\date{}

\begin{document}

\frontmatter

\maketitle

% don’t show chapter and section title in headers:
\pagestyle{headerwithouttitle}

\include{abstract-fr}
\include{abstract}

\tableofcontents
\listoffigures
%\listoftables

\include{epigraph}

% show chapter and section title in headers:
\pagestyle{book}

\include{long-abstract-fr}

% ------------------------------------------------------------------------------
% Main contents.

\mainmatter

% force chapters to start on odd pages:
\makeatletter \@openrighttrue \makeatother

\include{intro}

\include{background}

%\part{A program logic for weak memory}

\include{llog}
\include{hlog}

%\part{Case studies}

\include{examples}
%\addtocontents{toc}{\protect\vspace*{\baselineskip}\protect} % horrible trick, see \bookmarksetup below
\include{queue}

% this puts following chapters outside of the preceding part in PDF bookmarks;
% however this does not put the appropriate amount of vertical space in the ToC
% to separate the chapter from the preceding part, so we do it ourselves with
% the \addtocontents line above (which is placed weirdly to compensate a bug).
%   https://tex.stackexchange.com/a/12544
%\bookmarksetup{startatroot}

\include{related}
\include{conclusion}

\backmatter

% ------------------------------------------------------------------------------
% Bibliography.

% Note: english.bib is maintained by François. Do not edit.
%       Add entries to local.bib instead.
\bibliography{local,english}

\end{document}
