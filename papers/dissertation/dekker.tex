\section{Dekker's algorithm}
\label{sec:examples:dekker}

\input{fig-dekker-impl}
\input{fig-dekker-inv}

% TODO Coq: prove the "final" (higher-order) spec for Dekker
% TODO Coq: define "locked" for Dekker


Dekker's algorithm
%TODO: citation
is among the first mutual exclusion algorithms to be invented.
It ensures some form of fairness
and its implementation requires only read and write operations
on sequentially consistent registers: it does not require a CAS operation.
%
Dekker's algorithm remains valid in \mocaml, provided atomic references are
used in its implementation. This is not an entirely trivial claim, as the
user-chosen invariant~$\prop$ may be subjective.

The code, shown in \fref{fig:dekker:impl}, uses three atomic references:
\begin{itemize}%
  \item Each of the two threads $i \in \{0,1\}$ uses a Boolean register
    $\petersonflag_i$ to indicate it currently holds the lock or
    intends to acquire it.
    Both threads read $\petersonflag_i$ but only thread~$i$ writes it.
  \item An integer register \petersonturn indicates which thread has priority,
    should both threads simultaneously attempt to acquire the lock.
    It is read and written by both threads.
  \end{itemize}

When, as one of the two participant threads, we want to acquire the lock,
we signal it by setting our own flag to $\True$.
We then wait for the other thread to either release the lock
or to retract from its intention to acquire it,
by waiting until that other thread’s flag is $\False$.
%
While waiting, we check who has priority,
as given by the reference~$\petersonturn$.
If priority goes to the other thread,
then we retract by setting our flag back to $\False$---%
thus letting the other thread proceed to the critical section---%
, we wait until priority switches to us,
and we signal again our intention to acquire the lock.

When releasing the lock,
we switch $\petersonturn$ so as to give priority to the other thread,
then we set our own flag to $\False$.

We now outline our proof that the code in \fref{fig:dekker:impl} satisfies the
specification in \fref{fig:mutualexcl:spec}.
%
%As we use a logic of partial correctness,
%we do not prove deadlock-freedom or fairness.
%We only verify that this data structure behaves like a lock.
%
The algorithm's possible states are as follows:
at any time, each of the participant threads is in one of three possible
states: either it is outside of a critical section (\petersonoutside),
% delimited by \release and \acquire
or it has set its flag to $\True$ and is now waiting (\petersonwaiting),
or it is inside a critical section (\petersoncritical).
\fref{fig:dekker:states} summarizes these states and the transitions between them.
%
\fref{fig:dekker:inv} presents the algorithm's invariant.
We use two ghost variables $\gpetersonstate 0$ and $\gpetersonstate 1$ to keep
track of the logical state of each thread.

Note that, while the use of $\petersonturn$ avoids deadlocks and ensures fairness,
it plays no role in functional correctness.
According to the invariant, neither the value of $\petersonturn$ or,
in a weak memory setting, the synchronization it performs, are relevant.
Effectively, should this reference be removed
and the outer loop in \acquire be reduced to an empty body,
just spinning until $\petersonotherflag$ is $\False$,
then the code would still satisfy the functional specification---%
but it may deadlock.
%
Hence, it is important to emphasize that we use a logic of partial correctness,
in which we do not prove deadlock-freedom or fairness.
We only verify that this data structure behaves like a lock.

Let us sketch the crucial step in the proof of \acquire.
While waiting to acquire the lock, a thread knows that its own state is
$\petersonwaiting$ (because it holds $\smash{\ownGhost{\gpetersonstate i}{\authfrag \petersonwaiting}}$).
As per the conjunct
${(\vpetersonstate_{1-i} = \petersonoutside \lor \vpetersonflag_{1-i} = \True)}$
of the invariant,
when the waiting thread reads $\False$ in $\petersonotherflag$,
it learns that the other thread’s state is $\petersonoutside$.
Thus, at that moment, none of the two participants are in state~$\petersoncritical$
so,
from the last conjunct of the invariant,
the waiting thread can take ${\prop \opat (\view_0 \viewjoin \view_1)}$
by switching its own state to $\petersoncritical$.

The key novelty, with respect to the proof that one could carry out in a
sequentially consistent setting, is that the invariant must record the
view~$\view_i$ that was the view of thread~$i$ when this thread last released
the lock. This view
% (or a better view)
is stored at the atomic reference $\petersonflag_i$.
%
When the lock is available, because the lock could have been last released by
either thread, the assertion~$\prop$ holds either at $\view_0$ or at
$\view_1$. Because \hlog assertions are monotonic, this implies that
$\prop$~holds at the combined view $\view_0 \viewjoin \view_1$. The
invariant records this fact.

Thus, when the outer loop in \acquire terminates, the invariant can give us
$\prop\opat(\view_0 \viewjoin \view_1)$. A key step is to argue that we also
have $\seen{\view_0}$ and $\seen{\view_1}$ at this moment of the program,
so as to then combine the pieces via \splitso and obtain $\prop$.
Let us sketch why this is the case.

\begin{itemize}

  \item
Establishing $\seen{\view_i}$ is easy enough, because $\view_i$ is a past view
of this very thread. One way of recording this information is to let the token
$\isPeterson~i$ carry it; indeed, the role of this token is precisely to carry
information from a \release to the next \acquire.
%
% fp: note that this token is an example of subjective ghost state,
%       following \citet{dang-20}
%
% jh: Ok, c'est vrai, mais on pense que ça nécessiterait une
% explication assez étoffée, et on n'a pas la place, et ce n'est pas le
% sujet ici.
%
We include in the definition of $\isPeterson~i$ an assertion $\seen\view$
and set up a ghost variable $\gpetersonview{i}$ whose purpose is to allow
us to prove that $\view$ is in fact~$\view_i$.
%
% fp: c'est un peu lourd visuellement, même si ça semble en fait
%           assez naturel. On peut se demander quel sucre de plus haut
%           niveau on pourrait introduire (en Coq et dans l'article)
%           pour rendre cela moins lourd. En gros l'invariant devait
%           être un record avec des champs fantômes appartenant à
%           des monoïdes Auth(Ex T), et on devrait pouvoir utiliser
%           le nom du champ directement pour faire référence à son contenu...
% jh, glen: ok, mais rajouter une nouvelle abstraction et l'expliquer
% prendrait plus de place que cela ne permettrait de gagner en clarté.
%
% remarque cachée, la preuve est déjà suffisamment compliquée:
%
%Instead of carrying $\view_i$ via a predicate $\seen \view$, we could as well
%recover it from the \mocaml-specific fact that the write to $\petersonturn_i$
%also performs read-like synchronization; but we still need the ghost variable,
%to ensure that the view $\view_i$ of the invariant is not altered between the
%moment we write to $\petersonturn_i$ and the moment we finally claim $\prop$.

  \item
The wait terminates immediately after reading $\petersonflag_{1-i}$
and getting the value~$\False$.
Thus, thanks to the invariant, the thread acquires $\view_{1-i}$ via this read.

\end{itemize}
