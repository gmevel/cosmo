\subsection{Specification in a sequential setting}
\label{sec:queue:spec:seq}

\input{fig-queue-spec-seq}

Let us start by assuming a sequential setting. We can then use standard \SL
(\citenop{reynolds-02}; briefly presented in~\sref{sec:csl})
to reason about programs and the resources they manipulate.
%
In \SL, a queue~$\queue$ holding $\nbelems$ items \(\elemList*\),
where the left extremity of the list is the tail and the right extremity is the head,
can be represented with an assertion:
%
\[
  \isQueueSEQ \elemList
\]

As is usual in \SL, this \emph{representation predicate} asserts the unique
ownership of the entire data~structure. It is exclusive.
When holding it, we can safely manipulate the queue
without risk of invalidating other assertions about resources
that may alias parts of our queue.
In particular, the representation predicate cannot be duplicated.

The operations of the queue admit a simple sequential specification which is
presented in \fref{fig:queue:spec:seq}.

\begin{itemize}
  \item
    The function~$\queuemake$ has no prerequisite and gives us the ownership of a new
    empty queue.
  \item
    If we own a queue~$\queue$, then we can $\enqueue$ some item~$\elem$ into~$\queue$;
    if this operation ever returns, then it must return the unit value~$\Unit$
    and give us back the ownership of~$\queue$, where $\elem$ has been appended at the head.
  \item
    Conversely, if we own a queue~$\queue$, then we can $\dequeue$ from~$\queue$;
    if this operation ever returns, then it must return the first item~$\elem_0$
    found at the tail of~$\queue$, and it gives us back the ownership of~$\queue$,
    where that first item has been removed.
\end{itemize}

This specification implies that $\dequeue$ cannot possibly return when the queue is empty
($\nbelems = 0$); in this case, it must loop forever.
This is pointless in a sequential setting,
but becomes meaningful in the presence of concurrency,
where it makes sense for $\dequeue$ to wait until an item is enqueued.
%
This specification also applies to bounded queues, where (somewhat analogously)
\enqueue loops when the capacity is reached ($\nbelems = \capacity$), waiting
until room becomes available.
