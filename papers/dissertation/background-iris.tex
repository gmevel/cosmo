\subsection{Iris}
\label{sec:iris}

Iris~\citep{iris} is a fine-grain concurrent separation logic framework. 
Its starting idea is the observation that ghost state, 
implemented by parameterizable partial monoids, 
gives rise to a particularly expressive logic, 
able to subsume many previous proposals.
%
Thus, at the core of Iris is a notion of ghost state%
---initially partial commutative monoids, now a generalization called cameras. % (\sref{sec:iris:cmra})

Like earlier logics, Iris tackles fine-grain concurrency through the use of \emph{invariants}, 
that is, assertions whose resources are available by all threads equally and, thus, can never be revoked.

A distinctive trait of Iris invariants is that they are impredicative: 
the fact that an invariant holds is itself a first-class assertion, 
which can be put inside an invariant.
This feature has implications on the logic itself: 
to tackle impredicativity, Iris is \emph{step-indexed},
which means that it features a ``later'' modality $\later \prop$
\citep[\S5.5]{iris}.
It is not necessary to understand the details of step-indexing for the scope of our work;
when reading this dissertation, later modalities may be simply ignored.

% TODO: logical atomicity
%Iris is expressive enough that it can encode a notion of \emph{logical atomicity},
%drawing on ReLoC.

A strength of Iris is that it is implemented and mechanically proven sound in Coq. 
Besides, the Coq development ships with a deep embedding of the logic 
and provides a ``Proof Mode'', 
an interactive way of carrying proofs within the Iris logic, 
in a manner similar to how one would prove pure Coq goals
\citep{krebbers-17}.
%
Lastly, Iris can take advantage of the expressiveness of the host logic:
there is an injection from the type~$\typeCoqProp$ of Coq propositions%
---so-called \emph{pure} assertions, which do not own any resource---%
to the type~$\typeIProp$ of Iris assertions.
In this dissertation, we leave that injection implicit.
% notation for lifting from Prop to iProp: \liftpure{}

It is worth noting that
Iris has a notion of Hoare triples as first-class assertions,
which may own resources.

In this section, we give a simplified overview of the Iris notions that we use
throughout this dissertation. For a complete presentation of Iris, we refer the
interested reader to \citet{iris}.

\subsubsection{Ghost state and ghost updates}
\label{sec:iris:cmra}

In the original \SL, assertions are predicates over heap fragments.
This idea can be generalized to other notions of ownable \emph{resources}:
file handlers, printers, permissions to perform some actions...
%
Ghost state in Iris is a flexible tool for defining custom resources.
In fact, even invariants and the heap are encoded as pieces of ghost state.
Ghost state takes values from algebraic structures called \emph{cameras}.
%or step-indexed resource algebras
In this dissertation, we only give an informal description of cameras,
whose actual definition is quite technical;
we refer the interested reader to \citet[\S2.1, \S4.4]{iris}.
%
%For the purpose of this dissertation, it is enough to think of a camera as a set
%equipped with a binary composition law $(\mtimes)$ that is partial,
%associative and commutative.%
%  \footnote{%
%    The actual notion of camera is slightly more general.
%    Moreover, it has a notion of step indexing.
%    Indeed, in order to tackle impredicativity, the Iris logic is step-indexed.
%  }
%
%The composition law of a given camera induces an order between its elements:
%$x \mincl y$ holds if there exists~$z$ such that $x \mtimes z = y$.

Ghost values are assigned to \emph{ghost variables}:
for any ghost variable~$\gname$ and any element~$x$ of any camera,
there is a separation logic assertion~$\ownGhost\gname{x}$,
whose intuitive reading is that
we own a fragment of~$\gname$
and that this fragment has value~$x$.
%
Unlike what happens with a traditional points-to assertion,
the ownership \emph{and value} of a ghost variable can be split into fragments.
A camera structure comes with
an associative and commutative composition law~$(\mtimes)$
which dictates how ghost state can be split and combined,
according to the following rule (where $\iequiv$ is the logical equivalence):
%
\begin{mathpar}
  \infer[GhostOp]
    {}
    {\ownGhost\gname{x \mtimes y} \iequiv
      \ownGhost\gname{x} \isep \ownGhost\gname{y}}
\end{mathpar}
%
This composition law is partial, that is,
the composition $x \mtimes y$ is not necessarily \emph{valid}
for all pairs of elements $x$~and~$y$.
In other words, some pairs of resources may be incompatible.

We can update fragments of the ghost state but not in arbitrary ways:
every update must preserve the validity of the whole ghost state,
that is, the new value of the fragment
must be compatible with any other fragment that might exist prior to the update;
these other fragments are left unchanged.
Thus, we say that a \emph{frame-preserving update}
is possible from~$x$ to~$y$ (both elements of some camera~$M$),
and we write $x \mupd y$,
when any element~$z$ of~$M$ that is compatible with~$x$
(that is, such that the composition $x \mtimes z$ is valid)
is also compatible with~$y$ \citep[\S2.1]{iris}.
Elements~$z$ are the possible “frames”.

In the Iris logic, changes to the ghost state
go through an \emph{update modality}:
the assertion $\pvs P$ is intuitively read as
“after some ghost update is performed, the assertion~$P$ will hold”.
We also define the notation $P \vs Q$ as sugar for $P \wand \pvs Q$,
that is, “we can update the assertion~$P$ to the assertion~$Q$”.
%
Iris has the following reasoning rules for manipulating ghost state
within the logic:
%
\begin{mathpar}%
  \infer[GhostAlloc]
    {x \in M}
    {\pvs \Exists \gname. \ownGhost \gname x}

  \infer[GhostUpdate]
    {x, y \in M \\ x \mupd y}
    {\ownGhost \gname x \vs \ownGhost \gname y}
\end{mathpar}%
%
Rule~\textsc{GhostAlloc} says that,
by performing an update to the ghost state,
we can initialize a fresh ghost variable~$\gname$ to any ghost value~$x$.
%
Rule~\textsc{GhostUpdate} says that,
if there is a frame-preserving update from~$x$ to~$y$,
then we can transform the value of a fragment of ghost state from~$x$ to~$y$,
again by performing an update to the ghost state.

The Iris toolkit provides several useful camera constructions, presented now.

\paragraph{Exclusive camera}
The \emph{exclusive camera} \citep[\S3.1]{iris}
$\exm(X)$ over some type%
  \footnote{%
    \label{fn:ofe}
    Technically speaking, $X$ must be an OFE,
    short for \emph{ordered family of equivalences},
    which is a step-indexed analog of the usual notion of type
    \citep[\S4.2]{iris}.
  }~$X$
has $X$ as its carrier.
We denote by $\exinj$ the injection from~$X$ to~$\exm(X)$.
The composition law is such that there cannot be more than one fragment of the
resource in existence:
that is, the composition $\exinj(x) \mtimes \exinj(y)$ is never valid.
It follows that elements of this camera can be updated freely:
$\exinj(x) \mupd \exinj(y)$ for any $x$ and $y$.

More generally, an element~$x$ of a camera~$M$ is said to be \emph{exclusive}
when it cannot be composed with any element of~$M$.
In that case, we have $x \mupd y$ for any element~$y \in M$.

\paragraph{Agreement camera}
The \emph{agreement camera} \citep[\S3.1, \S4.3]{iris}
$\agm(X)$ over some type\cref{fn:ofe}~$X$
%has $\aginj(x)$ as its elements, where $x$ is an element of $X$.
has $X$ as its carrier.
We denote by $\aginj$ the injection from~$X$ to~$\agm(X)$.
The composition law achieves agreement on some element of~$X$:
that is, $\aginj(x) \mtimes \aginj(x)$ is $\aginj(x)$,
and $\aginj(x) \mtimes \aginj(y)$ is invalid if $x \neq y$.
This camera allows no non-trivial update.
%$\aginj(x) \mupd \aginj(y)$ only when $x = y$.

\paragraph{Authoritative camera}
The \emph{authoritative camera} \citep[\S6.3.3]{iris}
$\authm(M)$ over some (unitary) camera~$M$
has both \emph{authoritative elements} of the form $\authfull{x}$
and \emph{fragmentary elements} of the form~$\authfrag{y}$,
where $x$ and $y$ are elements of~$M$;
it also has elements of the form $\authfullfrag{(x,y)}$,
so as to represent compositions of elements of the other two forms.
An authoritative element represents the full knowledge
of something, and cannot be split:
the composition ${(\authfull{x_1}) \mtimes (\authfull{x_2})}$ is never valid.
A fragmentary element represents partial knowledge,
and can be split and joined:
the composition $(\authfrag{y_1}) \mtimes (\authfrag{y_2})$ is
defined as $\authfrag{(y_1\mtimes y_2)}$.
Because a fragment must be a part of the whole,
the composition $(\authfull{x}) \mtimes (\authfrag{y})$
is valid if and only if
$y$ is \emph{included} in $x$, that is, if
there exists~$z$ such that $x = y \mtimes z$
(which we denote as $y \mincl x$).

To describe allowed updates in an authoritative camera,
it is useful to define a notion of \emph{local update}
\citep[\S4.8]{iris36-doc}:
we say that we can perform a local update from~$x$ and~$y$ to~$x'$ and~$y'$,
and we write $(x, y) \localupd (x', y')$,
when every element~$z$ such that $x = y \mtimes z$
satisfies $x' = y' \mtimes z$.
We then have the following rule for updating the authority
along with a fragment:
%
\begin{mathpar}%
  \infer[AuthUpdate]
    {(x, y) \localupd (x', y')}
    {\authfull x \mtimes \authfrag y \mupd \authfull x' \mtimes \authfrag y'}
\end{mathpar}%
%
In other words, we can update a part of the whole from~$y$ to~$y'$
if, while doing so, we preserve the remaining parts (the “frame”)~$z$.

\paragraph{Map camera}
The \emph{finite map camera} $X \maparrow M$
from some arbitrary type~$X$ to some camera~$M$
has finite maps from~$X$ to~$M$ as its elements;
composition and updates are pointwise.

\paragraph{Product camera}
The \emph{product camera} $M_1 \times M_2$
of two cameras $M_1$ and $M_2$
has pairs as its elements;
composition and updates are pointwise.

\paragraph{Sum camera}
The \emph{sum camera} $M_1 + M_2$ \citep[\S3.1]{iris}
of two (unitary) cameras $M_1$ and $M_2$
%has both $\cinl(x_1)$ and $\cinr(x_2)$ as its elements,
%where $x_1$ and $x_2$ are elements of $M_1$ and $M_2$, respectively.
has the sum type as its carrier.
We denote by $\cinl$ and $\cinr$ the two associated injections.
Composition in the sum camera is given by those of the two underlying cameras:
$\cinl(x) \mtimes \cinl(y) = \cinl(x \mtimes y)$ and
$\cinr(x) \mtimes \cinr(y) = \cinr(x \mtimes y)$;
the composition $\cinl(x_1) \mtimes \cinr(x_2)$ is invalid.

This camera supports pointwise updates: if $x \mupd x'$ then
$\cinl(x) \mupd \cinl(x')$, and similarly with $\cinr$.
Under certain circumstances,
we can also switch from one branch to the other:
if $x \in M_1$ is exclusive, then so is $\cinl(x)$,
hence $\cinl(x) \mupd \cinr(y)$ for any $y \in M_2$;
and similarly from $\cinr$ to $\cinl$.

%From now on, we omit writing the injections $\cinl$ and $\cinr$.

\paragraph{Fraction camera}
The \emph{fraction camera} $\fracm$ has the fractions \(q \in \Q_{> 0}\)
as its elements and addition as its composition law.
Updates are unrestricted.
%
Using that camera, we can encode fractional ownership of resources
\citep[\S6.3.3, \S7.3]{iris}.
%For instance, given any camera~$M$, the camera~$\agm(M) \times \fracm$ blabla.
Note that the fraction~$0$ is excluded;
hence, if our encoding of fractional ownership of a resource
constrains the fraction to be at most~$1$,
then a fragment of that resource with the fraction~$1$
cannot be composed with any other fragment.
In other words, the fraction~$1$ represents exclusive ownership.

\paragraph{The global ghost state}
The Iris logical framework is parameterized by a single camera structure,
called the \emph{global ghost state} \citep[\S3.2]{iris}.
However, thanks to the product camera construction,
we can build a global ghost state
as the product of however many cameras we need.
Hence, in practice, when we say that we use a given camera,
we simply assert that the global ghost state is a product
which contains at least the camera we are interested in.

\subsubsection{Persistence}
\label{sec:iris:pers}

Ghost state in Iris can encode ownership of \emph{resources}
that are not duplicable in~general%
---that is, these resources can be owned only once and they can be revoked---%
but it is also able to represent shareable \emph{knowledge}, that remains true forever.
More precisely, some camera elements~$x$ are idempotent, that is,
they do satisfy the equation $x = x \mtimes x$,
which implies that the assertion $\ownGhost \gname x$ is duplicable, that is,
the entailment
$\ownGhost \gname x \vdash \ownGhost \gname x \isep \ownGhost \gname x$ holds.
%
Examples include:
%
\begin{itemize}%
  \item $\aginj(x)$ for any~$x$;
  \item $\authfrag x$ when $x$ is idempotent;
  \item $\mapliteral{\mapbinding{k_1}{x_1}; \ldots; \mapbinding{k_n}{x_n}}$
    when $x_1, \ldots, x_n$ are idempotent;
  \item $(x, y)$ when $x$ and $y$ are idempotent.
\end{itemize}%

More generally, Iris has a notion of \emph{persistent} assertions
\citep[\S2.3]{iris}.
Once true, these assertions hold true forever.
They are duplicable, that is, the entailment
$\prop \vdash \prop \isep \prop$ holds for any persistent assertion~$\prop$.%
%
  \footnote{%
    Persistence in Iris is a strictly stronger notion than duplicability.
    See \citep[\S2.3]{iris} for details.
  }
%
Examples of persistent assertions include:

\begin{itemize}%
  \item any pure Coq proposition, that is,
    $\liftpure\prop$ for any proposition $\prop$ of type $\typeCoqProp$;
    for instance, an equality between Coq values;
  \item ownership of idempotent ghost state, $\ownGhost \gname x$
    when $x = x \mtimes x$;
  \item knowledge of an invariant, $\knowInv {} \prop$
    for any assertion~$\prop$
    (see next subsection);
    %(\sref{sec:iris:invariants});
  \item any Hoare triple, $\hoare \prop \expr \propB$;
  \item $\prop \isep \propB$ when $\prop$ and $\propB$ are persistent;
  \item $\Exists x. \prop$ and $\Forall x. \prop$
    when, for all~$x$, $\prop$ is persistent.
\end{itemize}%

Besides, Iris has a logic modality
for asserting that an assertion holds \emph{persistently},
that is, it ``holds without asserting any exclusive ownership''
\citep[\S5.3]{iris}.
%
For a given assertion~$\prop$, this modality is denoted as $\always\prop$.
It satisfies several reasoning rules, here are a few:
%
\begin{mathpar}%
  \infer[PersistElim]
    {\always \prop}
    {\prop}

  \infer[PersistIdemp]
    {\always \prop}
    {\always \always \prop}

  \infer[PersistConj]
    {(\always \prop) \land \propB}
    {(\always \prop) \isep \propB}

  \infer[PersistForall]
    {\Forall x. \always \prop}
    {\always \Forall x. \prop}

  \infer[PersistExists]
    {\Exists x. \always \prop}
    {\always \Exists x. \prop}
\end{mathpar}%
%
For any assertion~$\prop$, the assertion $\always \prop$ is persistent.
Rule~\RULE{PersistElim} shows that $\always \prop$ is stronger than $\prop$.
In fact, $\prop$ is persistent if and only if
the reciprocal entailment $\prop \vdash \always \prop$ also holds.
Adding the modality is helpful when $\prop$ only contains duplicable resources
but it is not obvious from the syntax of~$\prop$ that $\prop$ is persistent.

For instance, Hoare triples in Iris are in fact
syntactic sugar for weakest-preconditions assertions, defined like so
\citep[\S6]{iris}:
%
\[
  \hoare \prop \expr \pred
  \eqdef
  \always (\prop \wand \WP \expr \pred)
\]

\subsubsection{Invariants}
\label{sec:iris:invariants}

%\todo{invariant imprédicatif -> nécessaire pour faire des invariants de façon
%composable (= i.e.\ ne pas devoir dire dès le début du monde tous les
%invariants utiles) (map des noms d’invariants vers les assertions)}

An invariant in Iris \citep[\S2.2]{iris} is an assertion
that shares the ownership of a given resource to all threads equally.
This resource can be accessed by any thread, in some restricted way.
Because it must remain available to other threads,
an invariant can never be falsified: it~holds true forever, that is,
it is persistent.
%
Selected rules about invariants are shown below.
%
\begin{mathpar}%
  \infer[InvAlloc]{%
    \later I
  }{%
    \pvs \knowInv {} I
  }
\and
  \infer[HoareInv]{%
    \hoare {\later I \isep P} {e} {\Lam x. Q \isep \later I}
    \\ \text{$e$ runs for at most one step of execution}
  }{%
    \knowInv{}{I} \vdash \hoare {P} {e} {\Lam x. Q}
  }
\end{mathpar}%
%
Rule~\RULE{InvAlloc} shows how to form an invariant:
for any assertion~$I$, we can consume~$I$
in order to form a new invariant that we denote as~$\knowInv {} I$.
In other words, we give up on our exclusive ownership of~$I$ and place it
in an invariant where it is owned by everyone.
%
Then, rule~\RULE{HoareInv} shows how to use it:
we can \emph{open} it around a program fragment~$e$
and obtain its resource~$I$ (under a ``later'' modality)
for the duration of the execution of~$e$.
We must reestablish the invariant after $e$ has completed.

To ensure soundness in the face of concurrency,
Iris imposes a strict constraint:
$e$ cannot run for more than one step of execution.
%
It is as if there was a global runtime lock
which would protect all elementary steps of computation,
and whose associated invariant would be
the conjunction of all Iris invariants in existence.

Here we have omitted a technical detail which is needed for soundness:
in the actual logic, invariants and Hoare triples are annotated with ``namespaces''
in order to forbid that an invariant be opened several times simultaneously.
