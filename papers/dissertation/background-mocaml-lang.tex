\subsection{The programming language}
\label{sec:mocaml:lang}

We now present
% the syntax and operational semantics of
a core calculus that is representative of the \mocaml programming language
as regards shared-memory concurrency.
It is equipped with a dynamic thread creation operation
and with the two access modes
presented earlier. % (\sref{sec:mocaml:model}).
For simplicity, we refer to this calculus as \mocaml.

\subsubsection{Syntax}
\label{sec:mocaml:lang:syntax}

\input{fig-lang-syntax}

The syntax of our idealized version of \mocaml{} appears in \fref{fig:lang:syntax}.
%
It is untyped (contrary to the actual \mocaml)
and equipped with a standard call-by-value, left-to-right evaluation.
It features recursive functions,
primitive operations on Boolean and integer values,
tuples, optional values,
and standard control constructs, including conditionals and sequencing.
%
The last group of constructs in this figure are syntactic sugar,
easily implemented by other constructs,
thus there is no need to give semantics to the derived constructs.
%
This calculus is extended with shared-memory concurrency, as follows.
%
% \begin{itemize}
% \item
First,
the construct $\Fork\expr$ spawns a new thread. In the parent thread, this
operation immediately returns the unit value~$\Unit$. In the new thread, the
expression~$\expr$ is executed, and its result is discarded.
% \item
Second, the language supports the standard operations of reading and writing,
on both non-atomic and atomic memory cells.
%
In addition, atomic memory cells
support a compare-and-set (CAS) operation.
% \end{itemize}

The expression $\ArrayAllocANY\nbelems\val$ allocates
a block of $\nbelems$~cells whose access mode is~$\accmode$ and whose
initial value is $\val$.
%
There is no deallocation operation.
%
Reading from a cell with access mode~$\accmode$ at offset~$\idx$ of
location~$\loc$ is written $\ArrayReadANY\loc\idx$.
%
Writing a value~$\val$ to a cell with access mode~$\accmode$ at offset~$\idx$ of
location~$\loc$ is written $\ArrayWriteANY\loc\idx\val$.
%
In addition, atomic cells support the usual compare-and-set operation:
%
$\ArrayCAS\loc\idx{\val_1}{\val_2}$
reads the atomic cell at offset~$\idx$ of location~$\loc$,
tests whether its value is equal to $\val_1$, overwrites it with $\val_2$ if that
is the case, and returns the Boolean result of the test; importantly, the
read and the write operations happen \emph{atomically}.

All memory reads and updates expect a pair of a block location and an offset:
in other words, cells are not first-class values, and there is no pointer arithmetic.
%
There is syntactic sugar for single-cell blocks, or ``references'':
$\AllocANY\val$ allocates a block of length~one, $\ReadANY\loc$ reads at
offset~zero, $\WriteANY\loc\val$ writes at offset~zero, and
$\CAS\loc{\val_1}{\val_2}$ performs a compare-and-set operation at offset~zero.%
\footnote{%
  In the surface language, non-atomics have type
  \texttt{'a ref}. Their operations are
  \texttt{ref},~\texttt{!}, and~\texttt{:=}.
  % Mutable record fields are also non-atomic.
  % Mutable arrays are non-atomic.
  Atomics have type
  \href{https://github.com/ocaml-multicore/ocaml-multicore/blob/master/stdlib/atomic.mli}
    {\texttt{'a Atomic.t}}.
  Their operations are
  \texttt{Atomic.make}, \texttt{Atomic.get}, \texttt{Atomic.set},
  and \texttt{Atomic.compare\_and\_set}.
}

\subsubsection{Operational semantics}
\label{sec:mocaml:lang:semantics}

\input{fig-lang-langsem}

The operational semantics of \mocaml is shown in~\fref{fig:lang:langsem}.
It is defined in two layers.

The ``per-thread'' reduction relation
$\expr \exprReduces{\smash\someMemEvent} \expr', \expr'_1, \ldots, \expr'_n$
defined in Figures~\ref{fig:lang:langsem:expr:pure}
and~\ref{fig:lang:langsem:expr:impure}
describes how an expression $\expr$ takes a step and reduces to a new
expression $\expr'$,
possibly interacting with the memory subsystem via an event $\someMemEvent$,
and possibly spawning a number of new threads $\expr'_1, \ldots, \expr'_n$.%
\footnote{In fact, a reduction step can either emit a memory event or spawn
  new threads, but not both. Also, the number of newly spawned threads is
  always zero or one.}
%
\fref{fig:lang:langsem:expr:pure} presents the pure reduction rules, that is,
the rules which do not involve an interaction with the memory subsystem
(they are annotated with the silent event $\noMemEvent$)
and do not spawn new threads.
Together with the rule for reducing under evaluation contexts
(which are defined in \fref{fig:lang:langsem:ectx}),
these rules form a standard small-step reduction semantics
for right-to-left call-by-value \lc.
%
\fref{fig:lang:langsem:expr:impure}
presents the reduction rules for the memory operations, which interact
with the memory subsystem via a memory event, and the reduction rule for
``fork'', which spawns one new thread.
%
From this point on, when $\cell = \locoff\loc\offset$ is a cell address,
we allow ourselves to write
$\ReadANY \cell$ for $\ArrayReadANY \loc \offset$,
$\WriteANY \cell \val$ for $\ArrayWriteANY \loc \offset \val$ and
$\CAS \cell {\val_1} {\val_2}$ for $\ArrayCAS \loc \offset {\val_1} {\val_2}$.
Recall that cell addresses are not first-class values in our language.

Note that a successful compare-and-set (CAS) instruction
and an atomic write instruction
are both modeled by a memory event of the shape $\memEventUpdate\_\_\_$.
Indeed, as per \citeauthor{dolan-18}’s memory model,
an atomic write has both a ``release'' and an ``acquire'' effect.

The ``thread-pool'' reduction relation
$\store; \pool \threadPoolReduces \store'; \pool'$
defined in \fref{fig:lang:langsem:pool}
describes how the machine steps
between two configurations $\store; \pool$ and $\store'; \pool'$.
In such a configuration, $\store$ is a store,
while $\pool$ is a thread pool, that is, a list % (or multiset)
of threads,
where each thread $\mkexpr\expr\thisview$ is a pair of
an expression~$\expr$
% (the code that this thread is running)
and a view~$\thisview$.
% (the view of this thread)
%
The left-hand rule allows the per-thread execution system and the memory
subsystem to synchronize on an event~$\someMemEvent$. The right-hand
rule shows how new threads are spawned; a newly-spawned thread inherits the
view of its parent thread. Because a per-thread reduction step cannot both
access memory and spawn new threads, these two rules suffice.

The operational semantics of \mocaml is the reflexive transitive
closure of the thread-pool reduction relation. It is therefore an interleaving
semantics, albeit not a sequentially consistent semantics, as it
involves a store whose behavior is nonstandard.

\label{def:stuck}
A machine configuration $\store; \pool$ is considered \emph{stuck} if the
thread pool~$\pool$ contains at least one thread that cannot take a step yet
has not reached a value.
%
A stuck configuration represents an undesirable event, a~runtime error. There
are many ways of constructing stuck configurations: examples include applying
a primitive operation to arguments of incorrect nature, attempting to call
a value other than a function, and so on. All such errors are ruled out by the
OCaml type system:\footnote{No changes to the type system are required
  by the move from OCaml to \mocaml. The type \texttt{'a Atomic.t} is a new
  (primitive) abstract type.} the execution of a well-typed program cannot
lead to a stuck configuration. Although this claim does not appear to have been
formally established, it seems clear that a syntactic proof of type soundness
for~ML with references~\citep{wright-felleisen-94} can be adapted to the
semantics of \mocaml.
%
% The key is to note that every value in a history must be well-typed
% according to the store typing.
%

A careful reader might note that a non-atomic read instruction (\memnaread)
\emph{could} potentially be stuck under certain circumstances. This could be
the case, for instance, if the history~$\hist$ is empty, or if the
timestamp~$\viewapp\thisview\nacell$ is too high, causing all write events in~$\hist$ to be
considered outdated. In reality, though, neither of these situations can
arise, because only a subset of \emph{well-formed} machine configurations can
be reached. In a well-formed configuration, every non-atomic cell ever
allocated must have a nonempty history, and no thread can get hold of an
unallocated cell, thereby removing the concern that $\hist$ might be empty.
% A similar invariant exists in ML with references.
% fp: Je crois que chez nous, on n'a pas besoin de cet invariant, car en
%     produisant une preuve de possession (fractionnelle) de la référence,
%     on prouve qu'elle est allouée. Ensuite le global view invariant
%     ci-dessous suffit à garantir que son historique est non vide.
Furthermore, a well-formed configuration must satisfy the following
\emph{global view invariant}: there exists a \emph{global view}~$\globalview$
such that:
\begin{enumerate}
\item
\label{gvi1}
  every thread's view~$\thisview$ is contained in $\globalview$,
  that is,
  $ \All\mkexpr\expr\thisview\in\pool.\;\thisview\viewleq\globalview $ holds,
  where $\pool$ is the thread pool;
\item
\label{gvi2}
  the view of every atomic cell is contained in $\globalview$,
  that is,
  \mbox{$
  \All \atcell, \val, \viewatloc.\;
  \storeapp\store\atcell = \mkval\val\viewatloc \IMPLIES
  \viewatloc \viewleq \globalview
  $} holds;
\item
\label{gvi3}
  the global view maps every non-atomic cell to a timestamp
  that exists in the history of this cell, that is,
  $
  \All \nacell, \hist.\;
  \storeapp\store\nacell = \hist \IMPLIES
  \viewapp\globalview\nacell \in \Dom\hist
  $ holds;
\item
\label{gvi4}
  the global view maps every currently unallocated cell
  to the minimal timestamp, that is,
  \mbox{$
  \All \cell \notin \Dom\store.\;
  \viewapp\globalview\cell = \timeMin
  $} holds.
\end{enumerate}
%
In fact, the global view~$\globalview$ is the join of all thread’s views,
and maps every allocated non-atomic cell to the maximal timestamp of its history.
In other words, there always exists a thread which has observed the most recent
write to a given cell---the thread that performed that write, at least.
This fact, however, is not needed in our proofs.
Besides, it would not hold anymore if we allowed thread deletion.

One can check that the above set of properties is indeed an invariant (i.e., it is preserved by
every reduction step) and that this invariant implies that a non-atomic read
instruction at an allocated non-atomic cell cannot be stuck.
%
% Items~(\ref{gvi1}) and (\ref{gvi3}) together guarantee that
% a non-atomic read instruction cannot be stuck.
% %
% Item~(\ref{gvi2}) ensures that an atomic read operation preserves
% item~(\ref{gvi1}).
% %
% Item~(\ref{gvi4}) ensures that the allocation of a non-atomic location
% preserves item~(\ref{gvi3}).
%
In fact, in the next chapter, we must prove this claim, and exploit this
invariant, otherwise we would be unable to prove that our logic is sound:
a~sound program logic must guarantee that a verified program cannot get stuck.
Thus, when we instantiate Iris for \mocaml, we build the global view invariant
into our ``state interpretation'' invariant
(\sref{sec:llog:instantiation}), and when we establish the reasoning rule
for non-atomic read instructions (rule \basenaread in \fref{fig:llog:rules:mem}),
we exploit it (\sref{sec:llog:rules}).
