% ------------------------------------------------------------------------------

% Overview of the implementation of the bounded queue with a ring buffer.

\section{Implementation of a MPMC queue using a ring buffer}
\label{sec:queue:impl}

We now present an implementation of a bounded MPMC queue,
that satisfies the specification devised in~\sref{sec:queue:spec}.
We show the code
(\sref{sec:queue:impl:presentation},~\sref{sec:queue:impl:code})
and give intuitions about its mode of operation
(\sref{sec:queue:impl:invariants},~\sref{sec:queue:impl:contention}).

The presented implementation takes inspiration
from a well-established algorithm~\citep{rigtorp}
that has been used in production in several applications.
%
In addition to supporting multiple producers and multiple consumers,
a feature of this queue implementation is that it is \emph{bounded},
that is, it occupies no more than a fixed memory size.
A motivation for that trait is that items may be enqueued
more frequently than they are dequeued;
in this situation, a bounded queue has no risk of exhausting system memory;
instead, if the maximum size is reached, enqueuing either blocks or fails.
% FP ce n'est pas seulement une question de limiter l'occupation mémoire
%    de la file, mais aussi une façon de forcer les producteurs à attendre
%    une fois que la file est pleine; donc c'est un mécanisme de contrôle
%    de flux.

\subsection{Overview of the data structure}
\label{sec:queue:impl:presentation}

\input{fig-queue-impl}

The code of the concurrent queue library we consider appears in \fref{fig:queue:impl}.
It uses a ring buffer of fixed capacity~$\capacity \geq 1$.
The buffer is represented by two arrays of
length~\capacity, \refstatuses and \refelements; in each slot (whose offset ranges from~$0$
included to~$\capacity$ excluded), the buffer thus stores a \emph{status} in an
atomic field and an \emph{item} in a non-atomic field. The data~structure also
has two integers stored in atomic references, \refhead and \reftail.

Items are identified by their \emph{rank} (starting at zero) of insertion in the queue
since its creation. The item of rank~\idx\ is stored in slot
``$\idx \mmod \capacity$'', which from now on we denote as $\modcap\idx$.
%
The reference~\refhead stores
the rank of the next item to be enqueued, that is,
the number of items that have been enqueued since
the creation of the queue, including those that have since been dequeued.
%
Similarly, \reftail stores
the rank of the next item to be dequeued, that is,
the number of items that have been dequeued.

% J’écris "each offset" et non "each cell" car on a en fait deux tableaux,
% donc deux "cells" au sens du langage de programmation.
% Je prends soin de distinguer ces termes:
%   - "offset" (indice d’un "slot", relatif au buffer, donc entre 0 et \capacity);
%   - "rank" (indice d’un "item" depuis la création de la queue, donc entre 0 et \head).
Each slot is in one of two states: either it is \emph{occupied},
meaning that it stores some item of the queue; or it is \emph{available},
meaning that the value it stores is irrelevant.
%
In addition, for the concurrent queue operations to work properly, we must
remember for which rank each slot was last used.%
  \footnote{%
    Actually, we need not remember the full rank~\idx:
    only the \emph{cycle}, $\idx \div \capacity$, is needed.
  }
%
The status encodes this information in a single integer, as follows:
%
\begin{itemize}%
%
  \item an even status $2\idx$ indicates that slot~$\modcap\idx$ is available
    for storing a future item of rank~\idx;
%
  \item an odd status $2\idx+1$ indicates that slot~$\modcap\idx$ is currently
    occupied by the item of rank~\idx.
%
\end{itemize}%

Let~\head\ and~\tail\ be the value of \refhead and \reftail, respectively.
%
At any time, the ranks of items that are stored in the queue---or in the process of being stored---%
range from~\tail\ included to~\head\ excluded,
and there cannot be more than \capacity\ such items.
Thus, a property of the queue is:
%
\[
  0 \leq \tail \leq \head \leq \tail+\capacity
\]

\subsection{Explanation of the code}
\label{sec:queue:impl:code}

The function~\enqueue repeatedly calls \tryenqueue until it succeeds;
the latter can fail either because the buffer is full or because of a competing enqueuer.%
%
\footnote{\label{fn:distinguishfail}%
  The code does not distinguish between these two causes, but this is feasible
  with only one more test.
}

When calling \tryenqueue, we start by reading the current value~\head\ of the
reference~\refhead. To check that slot~$\modcap\head$ is available for
rank~\head, we read its status.
%
If it differs from~$2\head$, we fail: a status less than~$2\head$ indicates that the buffer is full\footnote{Technically, the public state of the queue may contain less than $C$ elements in this case, so that we may consider it is not full. Here, by ``full'' we mean that the next buffer slot is either not reclaimed by anyone or still in the process of being emptied by another thread. Even though the buffer is ``full'', it may have available slots if dequeuing has completed more rapidly in these other slots.}, a status greater than~$2\head$ indicates interference from another competing enqueuing thread, which has been attributed rank~\head{} before we have.

If the status is $2\head$, then we try to increment \refhead from~\head\
to~$\head+1$.
%
If the CAS fails, we fail: again, another competing enqueuer has been attributed rank~\head{}.

If the CAS succeeds, then we are attributed rank~\head\ and can proceed to
inserting an item into slot~$\modcap\head$. As we will explain later, this implies that its status has
not changed since we read it: the slot is still available.
%
We write the new item, and then we update the status accordingly. This update must
come last, as it serves as a signal that the slot is now occupied with an item
and ready for dequeuers.
%
Notice that, under weak memory, another reason why the order of these two writes matters is that the
atomic write to \refstatuses must propagate the information that the non-atomic
write to \refelements has taken place. Thus, a thread which dequeues this item
(after reading its status) is certain to read a correct value from the array
\refelements. This is a typical release/acquire idiom.

Similarly, \dequeue repeatedly calls \trydequeue until it succeeds;
the latter works analogously to \tryenqueue,%
%
\footnote{%
  Overwriting the extracted value with a unit value~$\Unit$ is unnecessary for
  functional correctness but it prevents memory leaks.
}
%
and can fail either because the buffer is empty or because of a competing dequeuer.%
%
\cref{fn:distinguishfail}


\subsection{Monotonicity of the internal state of the queue}
\label{sec:queue:impl:invariants}

Once the queue has been created, the reference~\refhead is only accessed from
function~\tryenqueue. The only place where it is modified is the compare-and-set
operation in this function, which attempts to increment it by one, using
a compare-and-set operation. Hence this counter is strictly monotonic, and we
can regard a successful increment from~\head\ to~$\head+1$ as uniquely
attributing rank~\head\ to the candidate enqueuer. Only then it is allowed to
write into slot~$\modcap\head$.

Similarly, \reftail is only accessed from function~\trydequeue, is strictly
monotonic, and a successful increment from~\tail\ to~$\tail+1$ uniquely
attributes rank~\tail\ to the candidate dequeuer. Only then it is allowed to
write into slot~$\modcap\tail$.

The status of a given slot is strictly monotonic too. Indeed, there are two
places where it is updated.
As an enqueuer, when we write $2\head+1$, no other enqueuer updated the status
since we read it to be $2\head$, because only we have been attributed
rank~\head. In particular, it remained even, so no dequeuer tried to obtain
rank~\head\ and update the status of slot~$\modcap\head$. Hence, the status is
still $2\head$ when we overwrite it with $2\head+1$.
%
Symmetrically, the status is still $2\tail+1$ when a dequeuer overwrites it with
$2(\tail+\capacity)$.

\subsection{Notes on contention in the queue}
\label{sec:queue:impl:contention}

A noteworthy feature of this implementation is that
it tries to limit competition between enqueuers and dequeuers.
Indeed, enqueuers and dequeuers generally operate on separate references:
enqueuers never access \reftail and dequeuers never access \refhead.
Hence in favorable situations%
---when the buffer is neither empty nor full---%
there are no enqueuer-dequeuer competitions
beyond ones between an enqueuer and a dequeuer of the same rank.

A weakness of this implementation, however, is that it does not enjoy any non-blocking property~\citep[Chapter 2]{fraser2004practical}:
if an enqueuer or a dequeuer halts after it has been attributed a rank but before
it updates the corresponding slot, then after some time, any other thread trying
to enqueue or dequeue fails.
