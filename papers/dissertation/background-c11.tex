\subsection{C11}
\label{sec:c11}

Another prominent memory model is that commonly referred to as the ``C11 memory 
model'', which is used by C, C++ and Rust at least.
%
%Although it is less relevant to our work than Java’s, we briefly present it here 
%for the sake of completeness.
%
It was initially proposed for the 2011~revision of the C and C++ 
standards~\citep{c11mm}.
Not unlike in Java’s history, this initial model was later recognized to present 
several flaws: notably,
it was plagued by the out-of-thin-air problem~\citep{vafeiadis-narayan-13},
it prevented sensible optimizations~\citep{vafeiadis2015common} and, worse,
proposed compilation schemes to some architectures were unsound~\citep{rc11}.
Thus the model has been amended; an improved model
is known as the ``repaired C11 model''~\citep{rc11}.

As in Java, the C11 memory model is described axiomatically.
Here, rather than diving into the details of this axiomatic model,
we give some intuitions.
%
The C11 model distinguishes between \emph{atomic} and \emph{non-atomic} locations.
There are several modes of access to atomic locations, from stronger to weaker:
\emph{sequentially consistent (SC), release/acquire (RA), relaxed}.%
  \footnote{%
    The standard also defines ``consume'' reads, which we leave out of this 
    short introduction.
  }
The semantics of these access modes can be summarized as follows.
%
\begin{itemize}%
  \item
    SC accesses are the strongest and most costly. Atomic locations which are 
    used with only SC accesses should behave as a sequentially consistent piece 
    of memory, that is, at any time, they store single values that are seen 
    coherently by all threads.
    This is summarized by the \emph{DRF-SC theorem}, a variant of the 
    data-race-freedom theorem (\sref{sec:java}) which applies to C~programs 
    where atomic locations are used only through SC accesses.
\item
    Release/acquire accesses are weaker, but suffice to implement the 
    message-passing idiom shown in~\sref{sec:java}.
    They can be described in terms of allowed reorderings:
    %\todofp{confusing for non-experts?}
    an \emph{acquire read} cannot be moved after a subsequent read,
    and a \emph{release write} cannot be moved before a preceding write.
\item
    Relaxed accesses are the weakest possible form of atomic accesses.
    By itself, they do not prevent any reordering.
    Said otherwise, they guarantee no synchronization.
    Still, by contrast with a data race on a non-atomic location, a data race 
    involving relaxed atomic accesses has a defined behavior.
\end{itemize}%

Unlike in Java (and now \mocaml), several access modes can be mixed on a given 
atomic location. The interplay between access modes on a same location is 
responsible for a good share of the complexity of the C11 model. For one, in the 
original model, it causes SC accesses to exhibit surprising behaviors, weaker 
than one might expect, and is a motivation for the repaired model~\citep{rc11}.

Unlike in Java (and now \mocaml), the semantics is ``catch-fire'': a program 
that exhibits a data race on a non-atomic location has an entirely undefined 
behavior, which means that, as per the standard, anything is allowed to happen.

The C11 memory model as a whole is deemed particularly complex and, so far, many 
verification frameworks for that model
(e.g.\ \citet{vafeiadis-narayan-13,turon-vafeiadis-dreyer-14,kaiser-17}) tackle 
only a fragment of it.
