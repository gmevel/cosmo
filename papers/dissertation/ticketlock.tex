\section{A ticket lock}
\label{sec:examples:ticketlock}

% ------------------------------------------------------------------------------

% Explication du code.

The ticket lock is a variant of the spin lock where a ``ticket dispenser'' is
used to serve threads in the order that they arrive, thereby achieving a
certain level of fairness.
%
A simple implementation of the ticket lock appears in \fref{fig:ticketlock:impl}.
%
A lock is a pair $\Pair\ticketserv\ticketnext$ of two atomic references
$\ticketserv$ and $\ticketnext$, each of which stores an integer value.
%
The counter $\ticketserv$ displays the number of the ticket that is currently
being served, or ready to be served. The counter $\ticketnext$ displays the
number of the next available ticket.

A thread wishing to acquire the lock first obtains a unique number~$\vticketnext$
from the counter~$\ticketnext$, which it increments at the same time.
%
% The code of acquire contains an inlined implementation
% of fetch-and-add on top of CAS. It would be desirable to isolate it.
% However, that would require a logically-atomic spec.
%
This number is known as a \emph{ticket}. Then, the thread waits for its number
to be called: that is, it waits for the counter~$\ticketserv$ to contain the
value~$\vticketnext$. When it observes that this is the case,
it concludes that it has acquired the lock.

A thread that wishes to release the lock simply increments the
counter~$\ticketserv$, so that the next thread in line is allowed to proceed
and take the lock. To do so, a CAS instruction is unnecessary: a sequence of a
read instruction, an addition, and a write instruction suffices. Indeed,
because the lock is held and can be released by only one thread, no
interference from other threads is possible.
%
 This argument must be explicitly made somewhere in the proof,
 so an exclusive token ``$\locked$'' is required.
%

% ------------------------------------------------------------------------------

% Explication de la preuve.

\input{fig-ticketlock-impl}
\input{fig-ticketlock-inv}

We now sketch a proof of this ticket lock implementation. Our proof requires a
straightforward modification of the proof carried out by Birkedal and Bizjak
in a sequentially consistent setting (\citeyear[\S9]{iris-lecture-notes}). That is
precisely our point: it is our belief and our hope that, in many cases, a
traditional Iris proof can be easily ported to \hlog. The process is mostly a
matter of identifying which atomic memory cells also serve as information
transmission channels (thanks to the ``release/acquire'' semantics of atomic
writes and reads) and of decorating every Iris invariant with explicit views,
where needed, so as to meet the requirement that every invariant must be
objective.
%
Here, a moment's thought reveals that only the view stored at the
reference~$\ticketserv$ matters; the view stored at~$\ticketnext$ is
irrelevant.

The ghost state, the invariant and the definition of ``\locked''%
  \footnote{%
    Recall that these are established in the body of \lockmake,
    where the newly-allocated references $\ticketserv$ and $\ticketnext$ are in scope.
    %The proofs of the triples for \acquire, \wait, and \release are also carried
    %out in this scope.
  }
used in the verification of the ticket lock appear in \fref{fig:ticketlock:inv}.
%
\begin{itemize}
\item
  The ghost variable $\gticketserv$
  stores an element of the camera $\authm(\exm(\nat))$.
  % \citep[\S9]{iris-lecture-notes}
  %
  The exclusive assertion
  $\smash{\ownGhost\gticketserv{\authfrag\vticketserv}}$
  represents both the knowledge that ticket~$\vticketserv$ is currently being served
  and a permission to change who is being served.
  %
  % fp: this unquantified assertion plays a role in justifying
  %     that the value of $\ticketserv$ cannot change between
  %     the read instruction and the write instruction in \release.
  %
  The exclusive assertion ``$\locked$'', defined as
  $\smash{\Exists\vticketserv.\ownGhost\gticketserv{\authfrag\vticketserv}}$,
  represents a permission to change who is being served,
  therefore a permission to release the lock.
\item
  The ghost variable $\gticketissued$ keeps track of which tickets have been
  issued since the lock was created.
  It stores an element of $\authm(\setdisjm(\nat))$, where
  $\setdisjm~\nat$ is the camera whose elements are finite sets of
  integers and whose partial composition law is disjoint set union.
  The assertion $\issued\vticketnext$ represents the ownership of
  the ticket numbered~$\vticketnext$. It is exclusive in the sense that
  $\issued{\vticketnext_1} \isep \issued{\vticketnext_2}$
  entails $\vticketnext_1 \not= \vticketnext_2$.
%\pagebreak % final layout tweak
\item
  The invariant $\isTicketLock$ synchronizes the physical state and the
  ghost state by mentioning the auxiliary variables $\vticketserv$ and $\vticketnext$
  both in points-to assertions and in ghost state ownership assertions.
  %
  The same technique as in the previous subsection (\sref{sec:examples:spinlock})
  is used to make this invariant objective.
  %
  The last conjunct in its definition states that
  either no thread holds the lock
  and the user assertion~$\prop$ holds
  \emph{in the eyes of the thread that last released the lock},
  or
  the invariant owns the ticket numbered~$\vticketserv$.
  This implies that, in order to acquire the lock while maintaining the invariant,
  a thread must present and relinquish the ticket numbered~$\vticketserv$.
\end{itemize}

%Let us give the main ideas of the proof.
%A full description of an analogous proof,
%in a sequentially consistent setting,
%is given by \citet[\S9]{iris-lecture-notes}.

A thread that wishes to enter the waiting line
manufactures a fresh ticket by incrementing $\ticketnext$,
then is allowed to wait,
as a specification for the waiting loop is:
\[
  %\All\vticketnext.
  \hoare
    {\issued\vticketnext}
    {\While {\ReadAT \ticketserv \neq \vticketnext} \Unit}
    {\Lam\Unit. \locked \isep \prop}
\]

This specification is proven as follows.
If a thread can present the exclusive ticket numbered~$\vticketserv$
where $\vticketserv$ is the value of~$\ticketserv$,
then the invariant cannot simultaneously hold that ticket,
hence the left-hand disjunct of the disjunction
${(\locked \isep \prop\opat\view) \lor \issued\vticketserv}$
in the invariant currently holds.%
  \footnote{%
    This is the ``golden idol'' metaphor of~\citet{kaiser-17}.
  }
This allows the thread to take ownership of that left-hand disjunct,
and transition the invariant to the right-hand disjunct
by giving up its ticket.

Conversely, a thread that releases the lock
does not have the required ticket at hand,
so must re-establish the invariant.

By comparison to a proof in a sequentially consistent setting,
the only addition to account for weak memory is the fact that
the read of $\ticketserv$ in $\acquire$ acquires a view
and the write of $\ticketserv$ in $\release$ releases a view.

























\endinput

% Old-style specification of the ticket lock:

\begin{mathpar}
  \hoareV
    {\prop}
    {\lockmake~\Unit}
    {\Lam \lock. \Exists \gname. \isTicketLock~\gname~\lock~\prop}

  \hoareV
    {\isTicketLock~\gname~\lock~\prop}
    {\acquire~\lock}
    {\Lam \Unit. \locked~\gname \isep \prop}

  \hoareV
    {
      \isTicketLock~\gname~\lock~\prop
      \isep
      \locked~\gname \isep \prop
    }
    {\release~\lock}
    {\Lam \Unit. \TRUE}

  \hoareV
    {
      \isTicketLock~\gname~\lock~\prop
      \isep
      \issued~\gamma~\vticketnext
    }
    {\wait~\vticketnext~\lock}
    {\Lam \Unit. \locked~\gname \isep \prop}
\end{mathpar}
