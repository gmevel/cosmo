\begin{figure}

\begin{subfigure}{\textwidth}
\[\begin{array}{c@{\;}r@{\;}ll}%
  \lctx   &\cceq& \Box
                    & \text{--- pure contexts} \\
          % λ-calculus stuff:
          & \mid& \expr\;\lctx
            \mid  \lctx\;\val
            \mid  \expr \AnyOp \lctx
            \mid  \lctx \AnyOp \val \\
          % additional programming language features:
          & \mid& \Some\lctx
            \mid  \Tuple{\expr, \ldots, \expr, \lctx, \val, \ldots, \val} \\
          & \mid& \IfThenElse\lctx\expr\expr \\
          & \mid& \MatchWith\lctx{\Some\var}\expr\None\expr \\
          & \mid& \LetIn{\Tuple{\var, \ldots, \var}}\lctx\expr \\
          &     &   & \text{--- impure contexts} \\
          % allocation (blocks):
          & \mid& \ArrayAllocANY\expr\lctx
            \mid  \ArrayAllocANY\lctx\val \\
          % memory accesses (cells):
          & \mid& \ArrayReadANY\expr\lctx
            \mid  \ArrayReadANY\lctx\val
            \mid  \ArrayWriteANY\expr\expr\lctx
            \mid  \ArrayWriteANY\expr\lctx\val
            \mid  \ArrayWriteANY\lctx\val\val \\
          & \mid& \ArrayCAS\expr\expr\expr\lctx
            \mid  \ArrayCAS\expr\expr\lctx\val
            \mid  \ArrayCAS\expr\lctx\val\val
            \mid  \ArrayCAS\lctx\val\val\val \\
          & \mid& \Length\lctx \\
\end{array}\]
\caption{Evaluation contexts}
\label{fig:lang:langsem:ectx}
\end{subfigure}

\begin{subfigure}{\textwidth}
\begin{mathpar}
%
% evaluation context:
\inferrule
  {%
    \expr
    \exprReduces{\smash\someMemEvent}
    \expr', \expr'_1, \ldots, \expr'_n
  }
  {%
    \lctxapp\lctx\expr
    \exprReduces{\smash\someMemEvent}
    \lctxapp\lctx{\expr'}, \expr'_1, \ldots, \expr'_n
  }

% application:
\inferrule{}
  {%
    (\Rec\far\var\expr)\;\val
    \exprReduces{\noMemEvent}
    \varsubst{\varsubst\expr
      \far {\Rec\far\var\expr}}
      \var \val
  }

% binary operator:
\inferrule
  {\val_1 \AnyOp \val_2 = \val'}
  {%
    \val_1 \AnyOp \val_2
    \exprReduces{\noMemEvent}
    \val'
  }
\\
% if true:
\inferrule{}
  {%
    \IfThenElse\True{\expr_1}{\expr_2}
    \exprReduces{\noMemEvent}
    \expr_1
  }

% match Some:
\inferrule{}
  {%
    (\MatchWith{\Some\val}{\Some\var}{\expr_1}\None{\expr_2})
    \exprReduces{\noMemEvent}
    \varsubst{\expr_1}\var\val
  }

% if false:
\inferrule{}
  {%
    \IfThenElse\False{\expr_1}{\expr_2}
    \exprReduces{\noMemEvent}
    \expr_2
  }

% match None:
\inferrule{}
  {%
    (\MatchWith\None{\Some\var}{\expr_1}\None{\expr_2})
    \exprReduces{\noMemEvent}
    \expr_2
  }

% tuple destruction:
\inferrule{}
  {%
    \LetIn {\Tuple{\var_1, \ldots, \var_n}} {\Tuple{\val_1, \ldots, \val_n}} \expr
    \exprReduces{\noMemEvent}
    \varsubst{\varsubst\expr{\var_1}{\val_1} \ldots}{\var_n}{\val_n}
  }
\end{mathpar}
\caption{Thread-local reduction:
$\expr \exprReduces{\smash\someMemEvent} \expr', \expr'_1, \ldots, \expr'_n$
--- pure steps}
\label{fig:lang:langsem:expr:pure}
\end{subfigure}

\begin{subfigure}{\textwidth}
\begin{mathpar}
% non-atomic alloc:
\inferrule{}
  {%
    \ArrayAllocNA\val\nbelems
    \exprReduces{\memEventAllocNA\loc\nbelems\val}
    \loc
  }

% non-atomic read:
\inferrule{}
  {%
    \ReadNA\nacell
    \exprReduces{\memEventReadNA\nacell\val}
    \val
  }

% non-atomic write:
\inferrule
  {}
  {%
    \WriteNA\nacell\val
    \exprReduces{\memEventWriteNA\nacell\val}
    \Unit
  }

% atomic alloc:
\inferrule{}
  {%
    \ArrayAllocAT\val\nbelems
    \exprReduces{\memEventAllocAT\loc\nbelems\val}
    \loc
  }

% atomic read:
\inferrule{}
  {%
    \ReadAT\atcell
    \exprReduces{\memEventReadAT\atcell\val}
    \val
  }

% atomic write:
\inferrule{}
  {%
    \WriteAT\atcell\val
    \exprReduces{\memEventUpdate\atcell{\val_1}\val}
    \Unit
  }

% CAS failure:
\inferrule
  {\val_0 \neq \val_1}
  {%
    \CAS{\atcell}{\val_1}{\val_2}
    \exprReduces{\memEventReadAT{\atcell}{\val_0}}
    \False
  }
  % Note: KC says there no spurious CAS failures in Multicore OCaml.
  % CAS is implemented using __sync_bool_compare_and_swap, which (as
  % far as I understand) has "strong" semantics.
  % Pointers:
  % https://github.com/ocaml-multicore/ocaml-multicore/pull/109#issuecomment-292218554
  % http://en.cppreference.com/w/cpp/atomic/atomic/compare_exchange
  %
  % In the Coq development, CAS is restricted to literals, that is, values of
  % base types (no lambdas, no pairs, no sum types...).

% CAS success:
\inferrule{}
  {%
    \CAS{\atcell}{\val_1}{\val_2}
    \exprReduces{\memEventUpdate{\atcell}{\val_1}{\val_2}}
    \True
  }
\\
% length:
\inferrule{}
  {%
    \Length\loc
    \exprReduces{\memEventLength\loc\nbelems}
    \nbelems
  }

% fork:
\inferrule{}
  {%
    \Fork\expr
    \exprReduces{\noMemEvent}
    \Unit, \expr
  }
\end{mathpar}
\caption{Thread-local reduction:
$\expr \exprReduces{\smash\someMemEvent} \expr', \expr'_1, \ldots, \expr'_n$
--- impure steps}
\label{fig:lang:langsem:expr:impure}
\end{subfigure}

\begin{subfigure}{\textwidth}
\begin{mathpar}
\inferrule
  {%
    \expr \exprReduces\someMemEvent \expr'
    \\
    \store; \thisview
    \memReduces\someMemEvent
    \store'; \thisview'
  }
  {%
    \begin{array}{rl}
                       & \store; \pool_1, \mkexpr{\expr}\thisview, \pool_2 \cr
    \threadPoolReduces & \store'; \pool_1, \mkexpr{\expr'}{\thisview'}, \pool_2
    \end{array}
  }

\inferrule
  {%
    \expr \exprReduces\noMemEvent \expr', \expr'_1, \ldots, \expr'_n
  }
  {%
    \begin{array}{rl}
                       & \store; \pool_1, \mkexpr\expr\thisview, \pool_2 \cr
    \threadPoolReduces & \store; \pool_1, \mkexpr{\expr'}\thisview, \pool_2,
      \mkexpr{\expr'_1}\thisview, \ldots, \mkexpr{\expr'_n}\thisview
    \end{array}
  }
\end{mathpar}
\caption{Thread-pool reduction:
$\store; \pool \threadPoolReduces \store'; \pool'$}
\label{fig:lang:langsem:pool}
\end{subfigure}

\caption{Small-step operational semantics of \mocaml}
\label{fig:lang:langsem}
\end{figure}
