\subsection{Java}
\label{sec:java}

In the 1980s, memory models were limited to
informal descriptions by hardware vendors, often incomplete and subject to
backwards-incompatible changes~\citep{adve2010memory}.
Parallel computing was therefore reserved to programmers
with an expert knowledge not only of their target architecture,
but also of the specific optimizations that their compiler of choice
would or would not do.
At a time when multicore architectures were becoming more and more common,
lack of a clearly-defined semantics at the software level was impeding
broader usage of parallel features by programmers---and conversely,
hardware vendors could only guess what programmers would expect or use.
In~1995 Java---then a new language, whose design had included multithreading 
from the beginning---made an attempt at describing its memory guarantees;
however this initial specification was unclear, buggy,
and prevented valuable optimizations~\citep{pugh2000java}.
%
Thus, in the 2000s, considerable effort has been put into
designing a clear and workable specification for the memory model
of a high-level programming language.
This has led to a new memory model, adopted in Java~5.0~\citep{jmm}.

The Java memory model is described
in terms of relations between memory events in a given execution;
which executions are allowed is then specified as a set of constraints
on these relations---typically, some relations are required to be acyclic.
This kind of formalism is called an \emph{axiomatic model}.
At the heart of Java’s model is a partial order called the \emph{happens-before} relation.

Java features ordinary variables, \emph{volatile} variables and locks
(also called ``monitors'').
By contrast with ordinary variables,
accesses to volatile variables perform synchronization.
%
Technically speaking, for a given volatile variable,
there is a happens-before relation from a write event
to a read event pulling the value of that write.
%
Furthermore, the happens-before relation is transitive and contains the program order
(that is, the order in which instructions are written in source code).
This allows the following concurrent programming idiom,
called \emph{message passing}.
%
\begin{gather*}
\begin{array}{c}%
      \text{$x$ is an ordinary variable}
  \\  \text{$y$ is a volatile variable}
  \\  \text{initially $[x] = 0$ and $[y] = \False$}
\end{array}\\
\begin{array}{l||l}%
    \relax  [x] \gets 42     & a \gets [y]
  \\\relax  [y] \gets \True  & b \gets [x]
  \\\relax                   & \rlap{$\texttt{assert }(a = \False \lor b = 42)$}
\end{array}\end{gather*}%
%
In the left thread, the write to~$x$ is program-ordered before the write to~$y$.
In the case when the right thread reads $\True$ in~$y$, the write to~$y$
happens-before the read of~$y$, which is program-ordered before the read of~$x$.
Thus, the write of~$42$ to~$x$ happens-before the read of~$x$.
%
Schematically, the graph of memory events of such an execution may be
represented as follows.
%
\begin{center}\begin{tikzpicture}[
    ->,>=stealth',
    main node/.style={%
      node distance=1cm and 1cm,
      rectangle, draw,
      font=\bfseries
    }
  ]

  \node[main node,align=center] (Wx)                     {ordinary write \\ of $42$ to $x$};
  \node[main node,align=center] (Wy) [below=of Wx]       {volatile write \\ of $\True$ to $y$};
  \node[main node,align=center] (Ry) [below right=of Wy] {volatile read \\ of $\True$ from $y$};
  \node[main node,align=center] (Rx) [below=of Ry]       {ordinary read \\ of $42$ from $x$};

  \path[every node/.style={inner sep=0pt,font=\sffamily\small}]
    (Wx) edge node[right=2mm] {program order} (Wy)
    (Wy) edge node[above right=2mm] {happens before} (Ry)
    (Ry) edge node[right=2mm] {program order} (Rx)
    (Wx) edge[out=-160,in=-185,style=dashed] node[below left=2mm] {happens before} (Rx)
    ;

\end{tikzpicture}\end{center}
%
In this situation, the right thread indeed observes the value~$42$ in~$x$,
because the Java memory model mandates that an ordinary read pulls its value
from the last write that happens-before this read.

%This ensures that all memory events issued by the writer thread prior to the write
%``happen before'' all memory events issued by the reader thread after the read.

For a given execution, we formally define a \emph{data~race}
as a pair of two accesses to the same variable,
at least one of them being a write,
which are not ordered by the happens-before relation.
%
Data~races on non-volatile variables capture a class of concurrency bugs:
they cannot be given a reliable behavior
and are the sign of a lack of synchronization,
a mistake from the programmer.
%
A program is said to be \emph{correctly synchronized} when
no sequentially-consistent execution of the program exhibits a data race.
The Java memory model enjoys the \emph{data-race-freedom (DRF) property}:
if a program is correctly synchronized,
then any execution of the program is equivalent to a sequentially consistent execution
\citep[\S9.2.1]{jmm}.
This property allows programmers to reason about the correctness of their code:
to prove that a program is correct, it is enough to prove it in a memory model
where data races have undefined behavior
\citep{aspinall2007formalising}.

Some memory models have a ``catch-fire'' approach to data~races:
racy programs are considered incorrect
and absolutely nothing is guaranteed about their behavior.
However, in~Java, even racy programs must observe
minimal safety and security guarantees.
For instance, the memory model strives to disallow ``out-of-thin-air'' values,
that is, the fact that a location ends up storing a value that has not been
produced by any expression of the source code.
The possibility of such a situation is regarded as a serious flaw, for one, 
because it could produce an invalid pointer, thus breaking memory 
safety~\citep[\S2.2]{jmm}, type safety, and object invariants.
Hence, part of the challenge in designing a memory model for Java
has consisted in giving semantics to racy programs as well,
based on a notion of causality~\citep[\S4]{jmm}.

