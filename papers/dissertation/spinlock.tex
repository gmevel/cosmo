% ------------------------------------------------------------------------------

% The spin lock.

\section{A spin lock}
\label{sec:examples:spinlock}

\input{fig-spinlock-impl}

A spin lock is a simple implementation of a lock. It relies on a single atomic
memory reference~$\lock$, which holds a Boolean value. Its implementation in
\mocaml appears in \fref{fig:spinlock:impl}. The implementation of \acquire
involves busy-waiting in a loop, whence the name ``spin lock''.

An Iris proof of correctness of a spin lock \citep[Example
  7.36]{iris-lecture-notes}, in a traditional sequentially consistent setting,
would rely on the following Iris invariant, which states that either the lock~$\lock$
is currently available and its user assertion~$\prop$ holds, or the lock is
currently held:
%
\[
  \fname{isSpinLockSC}
  \eqdef
  \knowInv{}{
    (\pointsto\lock\False \isep \prop)
    \;\lor\;
    (\pointsto\lock\True)
  }
\]
%
In our setting, however, such an invariant does not make sense, and cannot
even be expressed, because the assertion~$\prop$ is an arbitrary
assertion of type $\typeVProp$, whereas \hlog requires every Iris
invariant $\knowInv{}{I}$ to be formed with an \emph{objective} assertion~$I$
of type $\typeIProp$ (\sref{sec:hlog:assertions:langindpt}).

We work around this restriction by reformulating this invariant under an
objective form: ``either the lock is currently available and the user
assertion~$\prop$ holds \emph{in the eyes of the thread that last released the
  lock}, or the lock is currently held''. \hlog allows expressing this easily:
%
\[
  \isSpinLock
  \eqdef
  \knowInv{}{%
    (\Exists \view. \pointstoAT\lock{\mkval\False\view} \isep \prop\opat\view)
    \;\lor\;
    (\pointstoAT\lock\True)
  }
\]
%

The left-hand disjunct, which describes the situation where the lock is
available, now involves an existential quantification over a view~$\view$. The
atomic points-to assertion $\pointstoAT\lock{\mkval\False\view}$ indicates
that $\view$ is the view currently stored at reference~$\lock$. The objective
assertion $\prop\opat\view$ indicates that $\prop$~holds at this view. That
is, the assertion~$\prop$ holds in the eyes of whomever last wrote the
reference~$\lock$. (We use the present-tense ``holds'', as opposed to the
past-tense ``held'', because every \hlog assertion is monotonic in its
implicit view parameter.) In other words, $\prop$ holds in the eyes of the
thread that last released the lock.

The right-hand disjunct, which describes the case where the lock is held,
uses the simplified points-to assertion $\pointstoAT\lock\True$, which is
sugar for $\pointstoAT\lock{\mkval\True\emptyview}$
(\sref{sec:hlog:rules}). In this case, the view stored at
reference~$\lock$ is irrelevant.

Proving that the spin lock satisfies the specification in \fref{fig:lock:spec}
then goes as follows.
We prove the triple of function~\lockmake.
%
In the body of \lockmake,
for the lock~$\lock$ that has just been allocated
and the user assertion~$\prop$ that was chosen by the user,
we establish the invariant~$\isSpinLock$ shown above.
%
We do not need any additional ghost state.
%
Since a spin lock has no need for a ``\locked'' token,
we let~``\locked'' be $\TRUE$.
%
With the lock invariant in context,
we then proceed to proving the triples for \acquire and \release.
Their proofs are sketched in the following.

The proof of \acquire amounts to establishing the following triple for
the CAS instruction:
% glen: Löb induction is then required to justify the loop in \acquire.
\[
  \hoare
    {\isSpinLock}
    {\CAS\lock\False\True}
    %{\Lam b. \text{if $b$ then $\prop$ else $\TRUE$}}
    {\Lam b. b = \True \implies \prop}
\]
This triple guarantees that
if the CAS succeeds then one can extract the assertion~$\prop$.
%
To establish it, we must open the invariant $\isSpinLock$
for the duration of the CAS instruction \citep[\S2.2]{iris}
%
% (\hlog, like Iris, allows opening an invariant for the duration of an
%  atomic instruction.)
%
and reason separately about the case where the lock is available and the case
where it is held. In the latter case, we apply the reasoning rule \casfailure
(\fref{fig:hlog:rules:mem}) and conclude easily. In the former case, we have
$\pointstoAT\lock{\mkval\False\view}$ and $\prop\opat\view$, for an unknown
view~$\view$. The rule \cassuccess (instantiated with the empty view
for $\view'$) shows that the outcome of the CAS instruction in this case is
$\pointstoAT\lock\True \isep \seen\view$.
%
In other words, the CAS instruction achieves the double effect of writing
$\True$ to the reference~$\lock$ and acquiring the view~$\view$ that was stored
at this reference by the last write.
%
This is exactly what we need.
%
Indeed, the axiom \splitso lets us combine $\prop\opat\view$ and $\seen\view$
to obtain $\prop$. By performing an ``acquire'' read, we have ensured that
$\prop$ holds in the eyes of the thread that has just acquired the lock.
%
Furthermore, the points-to assertion $\pointstoAT\lock\True$ allows us to
establish the right-hand disjunct of the invariant~$\isSpinLock$ and to
close it again.

The proof of \release exploits \splitso in the reverse direction. Per the
precondition of \release, we have~$\prop$. We split it into two assertions
$\prop\opat\view$ and $\seen \view$, where $\view$ is technically a fresh
unknown view, but can be thought of as this thread's view. Then, we open the
invariant $\isSpinLock$ for the duration of the write instruction. We do not
know which of the two disjuncts is currently satisfied (indeed, we have no
guarantee that the lock is currently held),
%
% glen: The value of \lock before releasing is unknown.
% In other words, the spin lock does not forbid spurious releases,
% as long as the thread that invokes release is able to provide P.
%
% If we wanted to have a guarantee that the lock is currently held,
% we could define \locked to be a ghost token.
%
but
% and by throwing away some information
we find that, in either case, we have $\pointstoAT\lock\_$. This allows us to
apply the reasoning rule \atwrite (\fref{fig:hlog:rules:mem}), which guarantees
that, after the write instruction, we have
$\pointstoAT\lock{\mkval{\False}{\view}}$. In other words, because we have
performed a ``release'' write, we know that, after this write, our view of
memory is stored at reference~$\lock$.
Because we have $\pointstoAT\lock{\mkval{\False}{\view}}$
and $\prop\opat\view$, we are able to prove that the
left-hand disjunct of~$\isSpinLock$ holds and to close the invariant.
