% travaux précédents sur la preuve de Peterson?
%
% «Peterson separation logic» dans Google Scholar ne donne aucun résultat.
% Avec «mutual exclusion separation logic» j’ai trouvé la thèse de Vafeiadis:
%     https://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-726.html
% Apparemment il a une preuve de Peterson mécanisée dans son outil SmallfootRG
% qui implémente sa logique RGSep (Rely/Guarantee + Separation logic)
% dans l’outil Smallfoot:
%     http://www0.cs.ucl.ac.uk/staff/p.ohearn/papers/smallfoot_v0.1.pdf
% (outil de vérification automatique de certaines specs «légères» en logique de
% séparation); mais aucun détail n’est donné.

\section{Peterson's algorithm}
\label{sec:examples:peterson}

\input{fig-peterson-impl}
\input{fig-peterson-inv}

Peterson's algorithm~\citep[\S2.1.2]{raynal}
is an alteration of Dekker’s (previous section)
which also implements fair mutual exclusion of two participants.
%
It still uses three atomic references $\petersonflag_1$, $\petersonflag_2$,
$\petersonturn$ with the same meaning.
The code, shown in \fref{fig:peterson:impl},
is shorter and apparently simpler
than that of Dekker’s algorithm.
%
In spite of this (or perhaps because of this), it is harder to understand.
Indeed, its proof of correctness, that we now outline, is more involved.

Instead of three, there are \emph{four} possible states for each participant,
summarized in~\fref{fig:peterson:states}:
%
either the participant is outside of a critical section (\petersonoutside),
% delimited by \release and \acquire
or it has just entered \acquire and set its flag to $\True$ (\petersonstartacq),
or it has written \petersonturn and is now waiting (\petersonwaiting),
or it is inside a critical section (\petersoncritical).
%
The algorithm’s invariant appears in \fref{fig:peterson:inv}.

Again, when the loop in \acquire terminates,
we need to argue that the other participant is not in state~$\petersoncritical$
(so that the invariant can give us ${\prop\opat(\view_0 \viewjoin \view_1)}$),
and that we have $\seen{\view_0}$ and $\seen{\view_1}$.
%
Thread~$i$’s own view~$\seen{\view_i}$ is obtained via the token $\isPeterson~i$
as in Dekker’s algorithm.
%
Proving the other two facts requires a case analysis,
because the waiting loop has two exit points,
due to the conjunction in the conditional statement:
%
\begin{itemize}
  \item
    The loop can terminate immediately after reading $\petersonflag_{1-i}$
    yields the value~$\False$.
    Then, as in Dekker’s algorithm,
    the thread acquires $\view_{1-i}$ via this read,
    and learns that the other thread is in state~$\petersonoutside$
    from the fact that its flag is $\False$.
  \item
    Or, the loop terminates after reading \petersonturn yields the value~$i$.
    Then, we must prove that the thread acquires~$\view_{1-i}$ via this read.
    This is nonobvious: it requires us to argue that the view~$\view$ stored at
    reference~\petersonturn must in fact contain $\view_{1-i}$.
    %
    The intuitive reason why this is true is that the two participants write
    \petersonturn in a polite way, always giving priority to the other thread.
    Thus, thread~$0$ writes only the value~$1$ to \petersonturn and vice~versa.
    Therefore, when thread~$i$ reads~$i$ from \petersonturn, this value must
    have been written by thread $1-i$, which implies that this read allows
    thread~$i$ to acquire the view~$\view_{1-i}$.
    %
    Technically, this argument is reflected in the invariant by the proposition
    %
    $
      \left(\vpetersonturn = 0 \land \vpetersonstate_0 = \petersonwaiting\right)
      \implies
      \left(\vpetersonstate_1 = \petersonwaiting \land \view = \view_1\right)
    $
    and its symmetric counterpart.
    %
    This proposition also lets thread~$i$ learn that the other thread is still
    in state~$\petersonwaiting$, which it entered when writing to $\petersonturn$.
\end{itemize}
% fp: joli!

Observe that, by contrast with Dekker’s algorithm,
the reference $\petersonturn$ plays an active role in the functional correctness
of Peterson’s algorithm:
not only its value is relevant,
the synchronization it performs also is.

\paragraph{}

This last case study highlights
what we believe is a strength of \hlog:
without loss of generality,
we have reduced the problem of transferring arbitrary assertions
to that of transferring a simple class of \emph{persistent} assertions,
that assert no ownership.
%
This allows the verification of programs, such as Peterson’s algorithm,
where a given resource may be transferred through varying channels:
the ownership of the resource does not need to go through these channels,
it is put in a single global invariant
and only persistent knowledge goes through these channels.

To illustrate that point,
we may define syntactic sugar for ownership of a \mocaml atomic cell
that is used to transfer an arbitrary assertion~$\prop$:
%
\[
  \pointstoAT \atcell {\mkval\val\prop}
  \eqdef
  \Exists \view. \pointstoAT \atcell {\mkval\val\view} \isep (\prop \opat \view)
\]
%
This new points-to assertion is objective and enjoys the following reasoning rules:
%
\begin{mathpar}
  % Atomic read.
  \hoareV
    {\pointstoAT \atcell {\mkval\val{\prop \isep \propB}}}
    {\ReadAT{\atcell}}
    {\Lam \val'. \val' = \val \isep \pointstoAT \atcell {\mkval\val\prop} \isep \propB}

  % Atomic write.
  \hoareV
    {\pointstoAT \atcell {\mkval\_{\prop \isep \propB}} \isep \propC}
    {\WriteAT{\atcell}{\val}}
    {\Lam \Unit.  \pointstoAT \atcell {\mkval\val{\prop \isep \propC}} \isep \propB}
\end{mathpar}
%
However, in this new framework, when reading (or writing) cell~$\atcell$,
we must choose immediately
which portion~$\propB$ of the stored assertion we take for ourselves
and which portion~$\prop$ we leave in the cell;
symmetrically, when writing, we must choose immediately the assertion~$\propC$
to store.
%
This restricts the reasoning that we are able to conduct.
%For instance, we would not be able to verify Peterson’s algorithm in this style:
%when releasing the lock, we would have to choose whether trying
%to store the user assertion~$\prop$ in either $\petersonflag_i$ or $\petersonturn$.
%
By contrast, with the view framework, one does not need to choose:
an assertion~$\seen\view$ is persistent, therefore duplicable.
