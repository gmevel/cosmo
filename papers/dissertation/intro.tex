\chapter{Introduction}
\label{sec:intro}

Advances in multicore hardware during the last two decades have created a need 
for powerful tools for writing efficient and trustworthy multicore software.
These tools include well-designed programming languages and their compilers, efficient thread-safe libraries, and expressive program logics for proving the correctness of these and of the applications that exploit them.
While some such verification tools already exist,
researchers are only beginning
to explore whether and how these tools can be exploited to
modularly specify and verify realistic libraries
that support fine-grained shared-memory concurrency.

Most of the programming languages that support multicore programming
present shared memory as the primitive means of communication between threads.
Although this design choice offers the greatest flexibility for writing efficient programs,
it comes at an important cost: in order to achieve maximal efficiency,
shared-memory concurrency cannot follow an intuitive \emph{sequentially 
consistent} semantics~\citep{lamport-79},
according to which a program would behave as an interleaving of the actions of 
its threads, all having the same view of memory at every time.
%
To allow compilers to perform more aggressive software optimizations
and to better exploit hardware optimizations,
many mainstream programming languages
adopt some subtle \emph{weak memory model}.
%
This is the case of low-level languages such as C, C++ and Rust,
which share the \emph{C11 memory model}~\citep{c11mm,rc11};
and of higher-level languages such as Java and other languages based on the 
JVM~\citep{jmm,lochbihler-jmm-12,bender-palsberg-19}.

\mocaml~\citep{dolan-18,mocaml} follows this trend.
It extends the OCaml programming language~\citep{ocaml} with support for 
shared-memory concurrency. \citeauthor{dolan-18} give it a well-defined 
semantics and, in particular, a \emph{memory model},
which specifies how threads interact through shared memory locations.
%which specifies how the memory accesses performed by different threads may 
%interact with each other.
Therefore, one may already ask: what reasoning rules can or should a \mocaml 
programmer rely upon in order to verify their code?
%
Furthermore, as mentioned, \mocaml's memory model is \emph{weak}: it
does not enforce sequential consistency.
%
Although it is expected that most application programmers will
not need to worry about weak memory, because they will rely on a library of
concurrent data structures written by domain experts, adopting a weak memory
model allows said experts to write more efficient code---if they know what
they are doing, that is!

% FP peut-être rappeler que le problème (dans un cadre SC) a été étudié depuis
% 50 ans (Owicki-Gries, 1976, par exemple), mais qu'il est encore un objet
% d'études intenses et que la démocratisation du multicore et de la mémoire
% faible renforce son intérêt et sa difficulté.

Because shared-memory concurrency is subtle, we believe that there is a need for 
a set of reasoning rules---in other words, a program logic---that both 
populations of programmers can rely upon. Concurrency experts, who wish to 
implement efficient concurrent data structures, must be able to verify that 
their code is correct, even though they have removed as many synchronization 
operations as they thought possible. Furthermore, they must be able to verify 
that their code implements a simple, high-level abstraction, thereby allowing 
application programmers, in turn, to use it as a black box and reason about
application code in a simple manner. In short, the system must allow 
compositional reasoning, and must allow both low-level reasoning, where the 
details of the \mocaml memory model are apparent, and high-level reasoning, 
which is independent of this memory model.

% Our terminology in this work:
%   low-level = specific to the \mocaml memory model
%   high-level = independent of the memory model
% Our base logic \llog is definitely low-level.
% Our derived logic \hlog is also low-level
%   but includes a high-level fragment
%   where one can be unaware of the memory model
%   (e.g., by using only locks and non-atomic locations).

Just in the past twenty years, a large variety of concurrent separation logics 
have been designed in order to meet the challenge of formally specifying and 
verifying programs that exploit shared-memory concurrency.
\citet{brookes-04} and \citet{ohearn-07}
introduced Concurrent Separation Logic~\citep{brookes-ohearn-16},
which supported coarse-grain sharing of resources via mutexes.
Their approach was gradually improved over the years, leading to expressive,
higher-order separation logics, such as Iris~\citep{iris-15,iris}.
Iris is able to express complex concurrent protocols, thanks to
mechanisms such as \emph{ghost state} and \emph{invariants},
and supports reasoning about fine-grain concurrency,
at the level of individual memory accesses.
Concurrent data structures, such as mutexes,
need not be considered primitive any more;
they can be implemented and verified.
Still, plain Iris is restricted to sequentially consistent semantics:
it does not support weak memory models.
A new generation of logics remove this restriction, for various memory models:
iCAP-TSO~\citep{sieczkowski-15},
GPS~\citep{turon-vafeiadis-dreyer-14}, iGPS~\citep{kaiser-17} and 
iRC11~\citep{dang-20}
target fragments of the C11 memory model.
iGPS, iRC11 and now this work are based on Iris.

For instance,
\citet{sieczkowski-15} propose iCAP-TSO, a variant of \CSL that is sound with
respect to the TSO memory model. While iCAP-TSO allows explicit low-level
reasoning about weak memory, it also includes a high-level fragment, the ``SC
logic'', whose reasoning rules are the traditional rules of \CSL. These rules,
which are independent of the memory model, require all primitive accesses to
memory to be data-race-free. Therefore, they require synchronization to be
performed by other means. A typical means of achieving synchronization is to
implement a lock, a concurrent data structure whose specification can be
expressed in the SC logic, but whose proof of correctness must be carried out
at the lower level of iCAP-TSO.
%
As another influential example, \citet{kaiser-17} follow a similar approach:
they instantiate Iris~\citep{iris} for a fragment of the C11 memory model.
This yields a low-level ``base logic'', on top of which
Kaiser et al.\ proceed to define several higher-level logics, whose reasoning
rules are easier to use.
%
% I am not saying very clearly whether the higher-level logics
% abandon some expressive power compared to the base logic.
% I suppose they do, but I don't know exactly what is lost.
%
Our aim, in this work, is analogous. We wish to allow the verification of a
low-level concurrent data structure implementation, such as the implementation
of a lock. Such a verification effort must take place in a program logic that
exposes the details of the \mocaml memory model. At the same time, we would
like the program logic to offer a high-level fragment that is independent of
the memory model and in which data-race-free accesses to memory, mediated by
locks or other concurrent data structures, are permitted.

Compared with other memory models in existence today, the \mocaml
memory model is relatively simple. Only two access modes,
known as ``non-atomic'' and ``atomic'', are distinguished.
%
Every program has a well-defined set of permitted behaviors. In particular,
data races on non-atomic memory locations are permitted, and have ``limited
effect in space and in time'' \citep{dolan-18}. This is in contrast with the
C11 memory model, where racy programs are deemed to have ``undefined
behavior'', therefore must be ruled out by a sound program logic.
%
\citet{dolan-18} describe the \mocaml memory model via an operational semantics,
where the execution of the program is in fact an interleaving of the executions of its threads.
These threads interact with a memory whose behavior is more complex than
usual, and whose description involves concepts such as \emph{timestamps},
\emph{histories} and \emph{views} of the shared memory (\sref{sec:mocaml:model}).

In this work, we take Dolan \etal.'s operational semantics, which we recall
in Section~\ref{sec:mocaml}, as a foundation. We instantiate Iris
for this operational semantics. This yields a
low-level logic, \llog (\chref{sec:llog}), whose reasoning rules expose the
details of the \mocaml memory model.
% These rules are sound and possibly complete (a fact that we do not attempt
% to prove).
Because of this, these rules are not very pleasant to use. In particular, the
rules that govern access to non-atomic memory locations are rather unwieldy, as
they expose the fact that the store maps each non-atomic location to a history,
a set of write events.
%
In order to facilitate reasoning, on top of \llog, we build a higher-level
logic, \hlog (\chref{sec:hlog}), whose main features are as follows.
%
\begin{itemize}
\item
  % 1- On se restreint en expressivité afin de gagner en simplicité.
  \hlog forbids data races on non-atomic locations.
  Data races on atomic locations remain permitted:
  atomic locations are in fact
  the sole primitive means of synchronization.
  This design decision allows \hlog to offer
  a simplified set of reasoning rules,
  including:
  \begin{itemize}
  \item standard, straightforward rules for
    data-race-free access to non-atomic locations;
  \item standard, straightforward rules for possibly racy access to atomic
    locations, with the ability of exploiting the sequentially-consistent
    behavior of these locations; and
  \item nonstandard yet arguably simple rules for reasoning about
    the release/acquire behavior of atomic locations.
  \end{itemize}
  The last group of rules allow transferring a ``view'' of the non-atomic
  memory from one thread to another. By exploiting this
  mechanism, one can arrange to transfer an arbitrary assertion~$P$
  from one thread to another, even when the footprint of~$P$ involves non-atomic
  memory locations. This feature is used in all of our case studies
  (Chapters~\ref{sec:examples} and~\ref{sec:queue}).

  Although views have appeared in several papers
  \citep{kaiser-17,dang-20}, letting the user reason about
  release/acquire behavior in terms of abstract views seems
  novel and simpler than previous approaches.  In particular, we
  claim that combining objective invariants and the rule
  \splitso{} yields a simple reasoning scheme, which could be exploited in
  logics for other weak memory models.
\item
  % 2- (Optionnel) Si on se restreint à un fragment de la logique \hlog,
  %    alors on peut raisonner de façon indépendante du modèle mémoire.
  \hlog offers a high-level fragment
  where reasoning is independent of the memory model,
  that is,
  a~fragment whose reasoning rules are those of traditional \CSL.
  % Should we name this fragment?
  % Probably not, as we would need one more name,
  % and several fragments of Cosmo are actually memory-model-independent
  % e.g. with or without direct access to SC locations.
  In this fragment, there is no notion of view.
  This fragment consists at least of the rules that govern access to non-atomic locations
  % (that is, the rules of the first group above)
  and can be extended by allowing the use of concurrent data structures
  whose specification is independent of the memory model % i.e., no views are involved
  and whose correctness has been verified using full \hlog.
  The spin lock and ticket lock~(\chref{sec:examples})
  are examples of such data structures:
  their specification in \hlog
  is the traditional specification of a lock in \CSL
  \citep{gotsman-aplas-07,hobor-oracle-08}. % buisse-11,sieczkowski-15
  Thus, \hlog contains traditional \CSL,
  and allows reasoning about coarse-grained concurrent programs
  where all accesses to
  memory locations are mediated via locks.
  % (non-atomic memory locations only, in that case)
  % (one can also allow SC locations with races and invariants,
  %  but that's already a larger fragment)
\end{itemize}

We illustrate the use of \hlog via several case studies. \chref{sec:examples} 
shows the specification and verification of typical, elementary synchronization 
libraries:
a spin lock, a ticket lock, Dekker’s mutual exclusion algorithm and Peterson’s 
one.
% for implementing mutual exclusion between two threads
% by exploiting only read and
% write instructions on atomic memory locations.
%
% (no CAS)
%
\chref{sec:queue} aims to demonstrate the applicability of \hlog to the modular 
verification of larger multicore programs.
We specify a multi-producer multi-consumer queue,
a more involved data structure that is archetypal of concurrent programming;
we then prove the correctness of a realistic implementation.
% with respect to this specification.
Specifying such a concurrent data structure raises interesting questions,
and demonstrates the expressiveness of our logic.
%
For one, the specification indicates that the queue behaves
as if all of its operations acted atomically on a common shared state,
even though in reality they access distinct parts of the memory
and require many machine instructions;
this uses the concept of \emph{logical atomicity}~\citep{iris-15,jung-slides-2019,da2014tada},
transported to the setting of \cosmo.
%
Besides, in the weak memory setting,
the specification of the data structure
describes not only the result of its operations,
but also the manner in which these are synchronized.
Two operations synchronize when the thread performing the second operation
%obtains the knowledge of the thread performing the first operation
%about writes done to the shared memory.
% OR:
%learns about the writes done to the shared memory
%which were known by the thread that performed the first operation.
% OR:
obtains the view of memory
which was that of the thread that performed the first operation.
This is crucial if a queue is used to transfer ownership of a piece of memory 
from a producer to a consumer.
Interestingly, the specification guarantees some amount of 
synchronization---regardless of the implementation---but it leaves others 
unspecified.
Even though all operations of the queue behave as if they were atomic,
and are ordered in a linear history,
not all pairs of successive operations in that history
are guaranteed to be synchronized.
For instance, it is not required that a dequeuing operation synchronizes with
the later enqueuing of an element.
%
This weak specification allows for more relaxed and thus more efficient 
implementations than a coarse-grained one.

All of our results, including the soundness of \hlog and the verification of our
case studies, are machine-checked in Coq.
%
Our proofs
% TODO: can be browsed in the accompanying artifact and are also
are
available from our repository~\citep{repo}.

%%  LocalWords:  OCaml dolan optimizations compositional sieczkowski iCAP TSO
%%  LocalWords:  et al logics non-atomic lang acyclicity Coq
