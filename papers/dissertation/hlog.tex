\chapter{A higher-level logic: \hlog}
\label{sec:hlog}

In this chapter,
we address both of the concerns just outlined
by building a higher-level logic, \hlog,
on top of the low-level logic \llog.
%
In \hlog, the points-to assertion for non-atomic cells takes the traditional
form $\pointstoNA[q] \nacell \val$, as in \CSL with fractional
permissions~\citep{boyland-fractions-03,bornat-permission-accounting-05}. This
assertion is strong enough to guarantee that reading $\nacell$ will yield the
value $\val$. Therefore, at an intuitive level, one can take it to mean that
``the cell~$\nacell$ currently contains $\val$'', or slightly more
accurately, ``in~the eyes of this thread, the cell~$\nacell$ currently
contains $\val$''. In more technical terms, this assertion guarantees that
\emph{the most recent write to the cell~$\nacell$ is a write of the
  value~$\val$} and that \emph{this thread is aware of this write}.

The meaning of this simplified points-to assertion is relative to ``this
thread'', that is, to a certain thread about which one is presently reasoning.
More precisely, its meaning depends on ``this thread's view'', as the
assertion claims that a certain write event is part of this thread's view.
Therefore, to give meaning to this simplified points-to
assertion, we find that we must parameterize every assertion with a view: in
other words, a~\hlog assertion must denote \emph{a function of a view to a
  \llog assertion}. This change in perspective not only seems required in
order to address concern~\ref{enum:history} above, but also addresses
concern~\ref{enum:explicitview} at the same time.
%
% We could admittedly solve concern~1 while keeping this view explicit;
% this would require parameterizing the ``simplified'' points-to
% predicate with this thread's view. But that would lead to a nightmare,
% as every points-to assertion at hand would need to be updated every
% time this thread's view changes.
%

Following \citet{kaiser-17} and \citet{dang-20}, we require every \hlog
assertion to denote a \emph{monotone} function with respect to the information
ordering~$\viewleq$ (\sref{sec:mocaml:model:views}).
%
This guarantees that, as new memory events become visible to this thread, the
validity of every assertion is preserved.
This condition makes the frame rule sound at the level of \hlog.
% fp : I think it is the rule
% $\prop \isep \WP\expr\predB \entails \WP\expr{\prop\isep\predB}$.
% Do we prove this rule somewhere in Coq?
% Do we give it somewhere in the dissertation?
% jh : Il s'agit de wp_frame_l, prouvé dans weakestpre.v. Avec Glen,
% on pense que ça ne vaut pas la peine d'utiliser de la place pour
% ça. Tout le monde sait ce qu'est la règle frame !

In the following sections, we describe how \hlog is constructed on top of \llog.
A~user of \hlog need not be aware of this construction: the assertions and
reasoning rules of \hlog can be presented to her directly.
Nevertheless, a \hlog proof is a \llog proof, and it is
therefore easy to combine proofs carried out in the two logics,
should \hlog alone not be expressive enough.

% ------------------------------------------------------------------------------

\section{Language-independent \hlog assertions and rules}
\label{sec:hlog:assertions:langindpt}

\input{fig-hlog-assertions}
\input{fig-hlog-rules-views}

\fref{fig:hlog:assertions:langindpt} defines a number of ways of constructing
\hlog assertions.
%
As explained, \hlog assertions, whose type is $\typeVProp$, are monotonic
functions from views to \llog assertions.

We lift the standard connectives of \SL, including $\lor$, $\isep$, $\wand$, $\exists$, $\forall$,
from \llog up to \hlog. In the definition of the magic wand, a universal
quantification over a future view~$\view$ that contains $\thisview$ is used so
as to obtain a monotone function of $\thisview$; this is a standard technique.
%
We also define the lifting of a \llog assertion $\prop$ up to a \hlog
assertion~$\liftobj\prop$. This provides a means of communication between
\llog and \hlog. The definition is simple: $\liftobj\prop$ is the constant
function $\Lam\thisview.\prop$. It is the archetypical example of
an \emph{objective} assertion, that is, an assertion whose meaning is
independent of this thread's view.
%
We often write just $\prop$ for $\liftobj\prop$.
%
Here are several typical examples of \llog assertions that can be usefully
lifted up, yielding objective \hlog assertions. In each case, we omit the
brackets $\liftobj\cdot$.
%
\begin{itemize}
\item A pure (Coq) proposition
      $\liftpure\prop$ where $\prop \in \typeCoqProp$ (\sref{sec:iris}).
\item The ownership of a piece of ghost state
      $\ownGhost{\gname}{m}$ (\sref{sec:iris:cmra}).
\item The knowledge of an invariant
      $\smash{\knowInv{}{I}}$ where $I \in \typeIProp$ (\sref{sec:iris:invariants}).
\item A \hlog assertion at a fixed view $\prop \opat \view$ (to be defined shortly).
%\item The fractional ownership of an atomic memory cell
%      $\pointstoAT[q] \atcell {\mkval\val\view}$.
%\item The knowledge of the length of a memory block,
%      ``$\haslength \loc n$''.
\end{itemize}

Regarding invariants, we emphasize that $\knowInv{}{I}$ is an ordinary \llog
assertion, hence $I$ must also be a \llog assertion.
That is, the logic \hlog is subject to a restriction:
\emph{in every invariant $\knowInv{}{I}$, the assertion $I$ must be objective.}
In short, because an invariant represents knowledge that is shared by all
threads, it cannot depend on some thread's view.
%
At first, this restriction might seem problematic: in general, an arbitrary
\hlog assertion~$\prop$ cannot appear in an invariant, therefore cannot be
transmitted from one thread to another.
Fortunately, in many situations, it is possible to work around this limitation.
Before explaining how, we define two more forms of assertions, that allows
manipulating views from the logic.

Beyond lifted assertions, \hlog features two kinds of assertions to deal with views:
$\seen\view$ and $\prop \opat \view$.

We define the assertion $\seen\view$, pronounced ``\emph{I~have~$\view$}'',
which asserts that this thread's view contains the view~$\view$. It is simply
the function $\Lam\thisview. \view \viewleq \thisview$, where the formal
parameter $\thisview$ represents this thread's view. It is easy to see that
this is a monotone function of~$\thisview$. The assertion $\seen\view$ is the
archetypical example of a \emph{subjective} assertion, that is, an assertion
whose meaning depends on this thread's view.
%
% The assertion $\seen\view$ is persistent~\citep{iris}.
% fp: Nous n'avons pas défini ``persistent'' pour vProp.
%     Est-ce que P est persistent si quel que soit W, P@W est persistent?
%     On pourrait dire plus fort: $\seen\view$ est non seulement persistente,
%     mais pure.
It satisfies the first two rules in \fref{fig:hlog:rules:views}. \seenzero{}
states that having nothing is the same as having the empty view. \seentwo{}
states that having two views $\view_1$ and~$\view_2$ separately is the same
as having their join $\view_1 \viewjoin \view_2$. The latter implies that the
assertion $\seen\view$ is anti-monotone with respect to~$\view$
(if $\view_1 \viewleq \view_2$ then $\seen\view_2 \vdash \seen\view_1$),
and that it is duplicable ($\seen\view \vdash \seen\view \isep \seen\view$).
%
In fact, as can be seen from its definition, the assertion~$\seen\view$ does not
assert the ownership of any resource, hence it is persistent.

The nature of views is hidden from a user of \hlog. That
is, views can be presented to the user as an abstract type equipped with a
bounded semilattice structure, whose least element and join operation 
are denoted by $\emptyview$ and $\viewjoin$, respectively.
This means that the user need not think of views as ``functions of
non-atomic cells to timestamps'', or as ``sets of write events''. Instead,
the user should think of a view as a certain amount of ``information'' about
the (non-atomic part of the) shared memory. The deduction rules of the logic
allow the user to reason abstractly about the manner in which this information
is acquired and transmitted
% e.g. via reads and writes of atomic cells
and about the places where it is needed.
% e.g. when P@V and \seen{V} must be combined to yield P (not yet explained)

The last line in \fref{fig:hlog:assertions:langindpt} defines $\prop \opat \view$
as sugar for the function application $\prop(\view)$.
This notation provides a means of communication in the reverse direction:
if $\prop$ is a \hlog assertion, then $\prop \opat \view$ is a \llog assertion,
which can be read as ``\emph{$\prop$ holds at $\view$}''.
As already mentioned, we lift this assertion to an objective \hlog assertion.
%
Said otherwise: $\prop \opat \view$, as a \hlog assertion,
denotes the assertion~$\prop$ where $\view$ has been substituted for the ambient view;
as it does not depend on the ambient view anymore, it is objective.%
  \footnote{%
    An equivalent definition is the following:
    asserting $\prop \opat \view$ is asserting that, even without other knowledge, if we have the knowledge contained in~$\view$, then $\prop$ holds.
    In other words, $\prop \opat \view$ is objective and entails $\seen\view \wand \prop$.
    %The assertion~ $\prop$ requires no other knowledge than that of $\view$.
  }

Thanks to the constructs that have just been introduced, it is possible to
express the idea that any \hlog assertion~$\prop$ can be split into a subjective
component and an objective component. This is stated by the rule \splitso{} in
\fref{fig:hlog:rules:views}. When read from left to right, the rule splits $\prop$
into a subjective component $\seen\view$ and an objective component
$\prop\opat\view$, for some view $\view$. (The witness for $\view$
is this thread's view at the time of splitting.) When read from right to left,
the rule reunites these components and yields $\prop$ again.
% the rule states that if $\prop$ holds in the eyes of whomever has observed
% the events in $\view$, and if this thread has observed all of the events in
% $\view$, then $\prop$ holds in the eyes of this thread.
% This relies on the fact that $\prop$ must be a monotone function of views to
% \llog assertions.
%
This decomposition is crucial:
it allows to transmit any \hlog assertion~$\prop$ from one thread to another
even though, in general, $\prop$ cannot appear in an invariant.
We \emph{can} let its objective part~$\prop$ appear in a invariant, thereby
allowing it to be shared between threads;
and we can transmit its subjective part via explicit synchronization operations,
typically writes and reads of atomic cells.
%
% The invariant must be of the form
% $\Exists\view.(\ldots \isep \prop\opat\view)$,
% where the assertion $\ldots$
% must somehow relate the view $\view$
% with a view that is stored in an atomic cell...
% -- mais ça semble un peu trop compliqué pour l'expliquer ici.
%
We will use that idiom in all our case studies
(Chapters~\ref{sec:examples} and~\ref{sec:queue});
our spin lock implementation (\sref{sec:examples:spinlock}), for one,
offers a typical example.

\pagebreak % final layout tweak

\section{\mocaml-specific \hlog assertions}
\label{sec:hlog:assertions:mocamlspecific}

\fref{fig:hlog:assertions:mocamlspecific} defines \hlog assertions that allow
reasoning about \mocaml programs.
%
We want a fractional points-to assertion for non-atomic cells,
a fractional points-to assertion for atomic cells,
and a length assertion for blocks.
The last two are lifted directly from \llog, omitting the brackets $\liftobj\cdot$.
Thus, 
$\pointstoAT[q] \atcell {\mkval\val\view}$ and ``$\haslength \loc n$''
are objective \hlog assertions.

We may omit the view~$\view$ carried by atomic cells when it is not relevant:
we define ${\pointstoAT[q]\atcell\val}$ as sugar for
${\pointstoAT[q]\atcell{\mkval\val\emptyview}}$,
which is logically equivalent to
${\Exists\view.\pointstoAT[q]\atcell{\mkval\val\view}}$,
and is still objective.
We thus recover the standard points-to predicate of \CSL with fractional permissions.

Regarding the non-atomic points-to assertion,
although we could lift the corresponding assertion from \llog as-is,
there is little interest in doing so:
our aim is to simplify the manipulation of non-atomic cells
by not having to make their entire history explicit, nor to have to reason
about timestamps.
Rather, we adopt the definition shown in \fref{fig:hlog:assertions:mocamlspecific}.
As announced earlier, this assertion takes the traditional form
$\pointstoNA[q] \nacell \val$, and means that
\emph{the most recent write to the cell~$\nacell$ is a~write of the value~$\val$}
and that \emph{this thread is aware of this write}.
Its definition, whose right-hand side is a conjunction of four assertions,
reflects this:
\begin{enumerate}
\item
  % The conjunct
  $\histNA[q]{\nacell}{\hist}$
  claims
  % ownership of
  a fraction $q$ of the cell~$\nacell$
  and guarantees that its history is $\hist$.
\item
  % The conjunct
  $\timest = \max\;(\Dom\hist)$
  guarantees that $\timest$ is the timestamp of the most recent write event
  at~$\nacell$.
\item
  % The conjunct
  $\hist(\timest) = \val$
  indicates that $\val$ is the value written by this write event.
\item
  % The conjunct
  $\Exists \view.\;
    \liftobj{\viewapp\view\nacell = \timest} % pure, therefore lifted twice
    \isep
    \seen \view$
  guarantees that
  % this thread's view includes this write event.
  this write event is visible to this thread.
\end{enumerate}
%
Because $\seen\view$ is subjective,
the non-atomic points-to assertion
$\pointstoNA[q] \nacell \val$
is itself subjective.
% (The other conjuncts in the definition of the non-atomic points-to assertion
%  are objective.)
Therefore, it cannot appear in an invariant.
This is the price to pay for the apparent simplicity of this predicate.

As was done for \llog in \sref{sec:llog:assertions:haslength},
we can define a \hlog points-to assertion for whole blocks of memory,
$\arraypointstoANY[q] \loc {\listliteral{b_0, \dots, b_{\nbelems-1}}}$,
as syntactic sugar
(recalling that the atomic points-to assertion and the knowledge of the length
of a block are lifted from \llog to \hlog as-is).

% ------------------------------------------------------------------------------

\section{\hlog weakest-precondition assertions}
\label{sec:hlog:triples}

We have just presented the universe of \hlog assertions, which have type
$\typeVProp$, in contrast with \llog assertions, which have type $\typeIProp$.
%
We now wish to define a \hlog weakest-precondition assertion
$\WP\expr\predB$
whose postcondition $\predB$ is a function of a value
to a \hlog assertion.
%
This is in contrast with \llog's weakest-precondition assertion
${\WP{\mkexpr\expr\view}\pred}$ (\sref{sec:llog:assertions})
where $\pred$ is a function of a value and a view
to a \llog assertion.
We define the former on top of the latter,
as follows:
%
% Cf. wp_def in the file weakestpre.v.
%
\[\begin{array}{rcl}
  \WP\expr\predB % : \typeVProp
  & \eqdef &
    \Lam \thisview.
    \\&&\quad
    \All \view \viewgeq \thisview.
    \\&&\qquad
    \valid\view \wand
      \WP
        {\mkexpr\expr\view}
        {\Lam \mkexpr{\val'}{\view'}.
           \predB(\val')(\view')
           \isep
           \valid{\view'}
        }
\end{array}\]
%
Because $\WP\expr\predB$ must have type $\typeVProp$, it must be a monotone
function of this thread's view~$\thisview$. In order to make it monotone, we
quantify over a future view~$\view$ that contains~$\thisview$.
% A technique already used in the definition of the magic wand.
We use a \llog weakest-precondition assertion to require that, from the
view~$\view$, executing the expression~$\expr$ must be safe and must yield a
value and a view that satisfy~$\predB$. As a final touch, we
place validity assertions in the hypothesis and in the postcondition so as to
maintain the invariant that ``this thread's view is valid'', thus removing
from the user the burden of keeping track of this information.

The \hlog triple is derived from the \hlog weakest-precondition
assertion in the usual way \citep[\S6]{iris}:
$\hoare\prop\expr\predB$ stands for $\always(\prop \wand \WP\expr\predB)$.

% ------------------------------------------------------------------------------

\section{Soundness of \hlog}
\label{sec:hlog:soundness}

\hlog, equipped with the weakest-precondition assertion that was just defined,
is adequate.
%
This follows straightforwardly from the adequacy theorem of \llog
(\sref{sec:llog:soundness}).

\begin{theorem}[Adequacy of \hlog]
  For any expression~$\expr \in \typeExpr$ and any
  pure predicate $\pprop : \typeVal \rightarrow \typeCoqProp$,
  if the entailment ${} \vdash \WP \expr {\liftpureobj\pprop}$ holds
  then the configuration $\mapempty; \mkexpr\expr\emptyview$
  is adequate with respect to~$\Lam\mkexpr\val\view.\pprop(\val)$.
  In particular, this configuration is safe.
\end{theorem}

% fp: amusant qu'on puisse affirmer (et démontrer) que la logique est
% sûre avant d'en avoir donné les règles...

% ------------------------------------------------------------------------------

\section{\hlog rules}
\label{sec:hlog:rules}

\input{fig-hlog-rules}

Each of the \llog rules described in \sref{sec:llog:rules} can now be
used as a basis to establish a higher-level \hlog rule, whose statement is
often simpler.
%
The resulting rules appear in~\fref{fig:hlog:rules}.

The three rules in \fref{fig:hlog:rules:nomem},
for language constructs that do not interact with the memory,
reflect those in \fref{fig:llog:rules:nomem}.
%
Being \hlog assertions, the triples that appear in these inference rules depend
on an ambient view. When both the premises and the conclusion contain a triple,
they are to be interpreted at the same view.
%
For rule~\rulepure, this conveys the idea that when taking pure reduction steps
from $\expr$ to $\expr'$, the thread’s view is unchanged.
%
Similarly, for rule~\rulefork, this corresponds to the fact that a spawned
thread~$\expr$ inherits the view of its parent thread~${\Fork\expr}$.

\subsection{Operations on non-atomic cells}

The first four rules in~\fref{fig:hlog:rules:mem} describe the operations of
allocating, reading, and writing non-atomic memory cells.
%
Allocation (\naalloc) requires nothing and produces points-to assertions
${\pointstoNA {\locoff\loc\offset} \val}$ for $0 \leq \offset < \nbelems$,
which represent the exclusive ownership of the fresh memory cells~$\locoff\loc\offset$.
A simplified rule (\naallocref) is easily derived for allocating a non-atomic reference.
%
Reading (\naread) requires $\pointstoNA[q] \nacell \val$, which represents
possibly-shared ownership of the memory cell~$\nacell$, and preserves this
assertion.
%
Writing (\nawrite) requires $\pointstoNA \nacell \_$, which represents
exclusive ownership of~$\nacell$, and updates it to $\pointstoNA \nacell \val$.

We expect the reader to find these rules unsurprising: indeed, they are
identical to the rules that govern access to memory in \CSL with
fractional permissions \citep{bornat-permission-accounting-05}.
%
In the absence of a mechanism that allows a points-to assertion to be shared
between several threads,\footnote{%
  Sharing an assertion between several threads is usually permitted either
  via a runtime synchronization mechanism, such as a critical region
  % referred to as a ``resource'' by O'Hearn
  \citep[Section~4]{ohearn-07}
  or a lock~\citep{gotsman-aplas-07,hobor-oracle-08},
  or by a purely ghost mechanism,
  such as an invariant that can be accessed for the duration
  of an atomic instruction, as in some variants of \CSL~\citep{parkinson-bornat-ohearn-07}
  and in Iris \citep{iris}.%
}
%
these rules forbid data races.
%
In \hlog, as explained earlier, a~non-atomic points-to assertion cannot appear
in an invariant, as it is not an objective assertion. Therefore, \hlog forbids
data races on non-atomic memory cells. In other words, for a program to be
verifiable in \hlog, all accesses to non-atomic memory cells must be
properly synchronized via other means, such as reads and writes of atomic
memory cells.
%
By contrast, \llog allows to reason about all programs that are safe
with respect to the operational semantics of~\citeauthor{dolan-18},
and the latter gives a well-defined (albeit nondeterministic) semantics
to racy uses of non-atomic cells.

\subsection{Operations on atomic cells}

The next six rules in~\fref{fig:hlog:rules:mem} describe the operations on atomic memory cells,
namely allocation, reading, writing, and CAS\@. They are analogous to
their \llog counterparts (\fref{fig:llog:rules:mem}), yet
simpler, as the validity assertions have vanished, and this
thread's view is no longer named: instead, assertions of the form~$\seen\view$
are used to indicate partial knowledge of this thread's view.

These rules deal with two aspects of atomic memory cells, namely the fact
that an atomic memory cell holds a value, and the fact that it holds a view.
Fortunately, these two aspects are essentially independent of one another.
Furthermore, the second aspect can be ignored when it is not relevant:
indeed, from the rules of~\fref{fig:hlog:rules:mem},
one can easily derive a set of simplified rules,
shown in \fref{fig:hlog:rules:memnoview}.
These derived rules are the
standard rules that govern access to memory in \CSL with fractional
permissions.

In \hlog, because an atomic points-to assertion is objective, it \emph{can} be
shared between several threads, via an Iris invariant. Therefore, the
rules of both \fref{fig:hlog:rules:mem} and
\fref{fig:hlog:rules:memnoview} allow data races on atomic memory cells. By
using just the derived rules of \fref{fig:hlog:rules:memnoview}, one can reason
in \hlog about atomic memory cells exactly in the same way as one reasons
in \CSL under the assumption of sequential
consistency \citep{parkinson-bornat-ohearn-07}.

Allocating atomic memory cells (\atalloc) requires this thread to have
some view~$\view$, as witnessed by the precondition~$\seen\view$. In the
postcondition, one obtains atomic points-to assertions
${\pointstoAT {\locoff\loc\offset} {\mkval\val\view}}$
for $0 \leq \offset < \nbelems$,
which represent the exclusive ownership of the fresh
memory cells~$\locoff\loc\offset$. Such an assertion states that a memory
cell~$\locoff\loc\offset$ holds the value~$\val$ and a view that is at least as
good as the view~$\view$.
Again, a simplified rule (\atallocref) can be derived for allocating an atomic reference.
%
Notice that, since $\seen\view$ is persistent, it still holds in the
postcondition with no need to repeat it.
%
Besides, both these allocation rules offer flexibility regarding the choice of~$\view$.
Indeed, $\view$ need not reflect all of the information that is currently
available to this thread: it can be a partial view.
(Recall that $\seen\cdot$ is anti-monotone.)
In fact, if desired, one can always take $\view$ to be the empty view.
(Recall that $\seen\emptyview$ can be obtained out of thin air.)
This is how the derived rule \atallocrefsc is obtained.

Reading an atomic memory cell (\atread) requires having a fractional points-to
assertion ${\pointstoAT[q] \atcell {\mkval\val\view}}$ and preserves it. The
last conjunct of the postcondition, $\seen\view$, reflects the fact that the
view held at cell~$\atcell$ becomes part of this thread's view: this is an
``acquire read''. The derived rule \atreadsc is obtained by dropping this
information.

Writing an atomic memory cell (\atwrite) requires having an exclusive points-to
assertion ${\pointstoAT \atcell {\mkval\_\view}}$ as well as (possibly partial)
information about this thread's view~$\seen \view'$. In the postcondition, the
points-to assertion is updated to $\pointstoAT \atcell {\mkval\val{\view
  \viewjoin \view'}}$, which reflects the fact that both the value~$\val$ and
the view~$\view'$ are written to the memory cell~$\atcell$: this is a ``release
write''. Furthermore, the second conjunct of the postcondition, $\seen\view$,
indicates that the view~$\view$ is acquired by this thread; indeed,
% in the \mocaml memory model,
an atomic write has both ``release'' and ``acquire'' effects.
Again, $\seen\view'$ need not be repeated in postcondition,
the user can weaken the view~$\view'$ at will,
and it is possible to ignore these details when they are irrelevant.
In~particular, the rule \atwritesc is obtained by
letting~$\view$ remain undetermined, by letting~$\view'$ be the empty view,
and by dropping~$\seen\view$ from the postcondition.

\casfailure and \cassuccess combine the rules for reading and
writing an atomic cell.
A~failed CAS instruction does not affect the content of the
memory cell, but still acquires a view~$\view$ from it. A successful CAS
instruction writes a value and a view to the memory cell and acquires a
view from it. Again, if one does not care about these information transfers,
then one can use the simplified rules \casfailuresc and \cassuccesssc in
\fref{fig:hlog:rules:memnoview}.
