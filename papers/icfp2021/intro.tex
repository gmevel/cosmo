\section{Introduction}
\label{sec:intro}

Advances in multicore hardware during the last two decades have created a need for powerful tools for writing efficient and trustworthy multicore software.
These tools include well-designed programming languages and their compilers, efficient thread-safe libraries, and expressive program logics for proving the correctness of these and of the applications that exploit them.
While some such verification tools already exist,
researchers are only beginning
to explore whether and how these tools can be exploited to
modularly specify and verify realistic libraries
that support fine-grained shared-memory concurrency.

Most of the programming languages that support multicore programming
present shared memory as the primitive means of communication between threads.
Although this design choice offers the greatest flexibility for writing efficient programs,
it comes at an important cost: in order to achieve maximal efficiency, % in practice?
shared-memory concurrency cannot follow an intuitive \emph{sequentially consistent} semantics~\cite{lamport-79},
where all threads have the same view of memory at every time.
Instead, these programming languages usually come with a subtle \emph{weak memory model},
which defines precisely how the memory accesses performed by different threads may interact with each other.
This is the case, for example, of C, C++ and Rust, which share the \emph{C11 memory model}~\cite{c11mm,rc11};
of Java and of the languages based on the JVM~\cite{jmm,lochbihler-jmm-12,bender-palsberg-19};
and of the Multicore extension of the OCaml programming language~\cite{dolan-18,mocaml}.

Because its semantics is subtle, shared-memory concurrency creates
difficult and interesting challenges in modular program verification.
% FP peut-être rappeler que le problème (dans un cadre SC) a été étudié depuis
% 50 ans (Owicki-Gries, 1976, par exemple), mais qu'il est encore un objet
% d'études itenses et que la démocratisation du multicore et de la mémoire
% faible renforce son intérêt et sa difficulté.
Just in the past fifteen years, a large variety of concurrent separation logics have
been designed in order to meet the challenge of formally specifying and
verifying programs that exploit shared-memory concurrency.
\citet{brookes-04} and \citet{ohearn-07}
introduced Concurrent Separation Logic~\cite{brookes-ohearn-16},
which supported coarse-grain sharing of resources via mutexes.
Their approach was gradually improved over the years, leading to expressive,
higher-order separation logics, such as Iris~\cite{iris-15,iris}.
Iris is able to express complex concurrent protocols, thanks to
mechanisms such as \emph{ghost state} and \emph{invariants},
and supports reasoning about fine-grain concurrency,
at the level of individual memory accesses.
Concurrent data structures, such as mutexes,
need not be considered primitive any more;
they can be implemented and verified.
Still, plain Iris is restricted to sequentially consistent semantics: it does not support weak memory models.
A new generation of logics remove this restriction, for various memory models:
GPS~\cite{turon-vafeiadis-dreyer-14}, iGPS~\cite{kaiser-17} and iRC11~\cite{dang-20}
target fragments of the C11 memory model,
while \cosmo~\citecosmo targets the \mocaml memory model\@.
iGPS, iRC11 and \cosmo are based on Iris.

These logics settle a strong theoretical ground; their confirmation as practical tools, however,
needs a demonstration that they allow the modular verification of realistic multicore programs.
In particular, they must enable their users to precisely specify and verify concurrent data structure implementations.
A concurrent queue is an archetypal example of such a data structure:
it is widely used in practice, for example
to manage a sequence of tasks that are generated and handled by different threads.
While a coarse-grained implementation---that is, a sequential implementation
protected by a lock---%
would certainly be correct,
there exist implementations
which yield better performance, especially under heavy contention,
based on subtle fine-grained memory accesses.
These implementations are delicate and
often rely on subtle properties of the memory model.
An informal correctness argument is difficult, likely unreliable, hence unconvincing.
Thus, concurrent data structures are prime candidates for formal
verification. In fact, many machine-checked proofs of concurrent
data structures have appeared in the literature already~\cite{parkinson-bornat-ohearn-07,frumin2018reloc,frumin2020reloc,zakowski2018verified},
but relatively few verification efforts take place
in a weak-memory setting~\cite{le2013correctws,le2013correctfifo},
and fewer still rely on a modular methodology,
where a proof of a concurrent data structure
and a proof of its client (perhaps a concurrent application,
or another concurrent data structure)
can be modularly combined.

In this paper, we present a specification of a concurrent queue,
and we formally verify that a particular implementation satisfies this specification.
While other such formalizations already exist in a sequentially-consistent
setting~\cite{vindum-birkedal-21,vindum-frumin-birkedal-21}, % TODO : more citations
we consider a weak-memory setting.
Such a formalization effort is innovative and challenging in several aspects.
\begin{itemize}
\item Weak memory models are infamous for the subtlety of the reasoning that they impose.
We choose to use \cosmo~\citecosmo, a recently-developed concurrent separation logic
based on Iris which supports the weak memory model of \mocaml{}~\cite{mocaml,dolan-18}.
We believe that this memory model and program logic strike
a good balance between the ease of reasoning enabled by the logic and the flexibility and performance allowed by the memory model.
\item In our quest for applicability, we wish to propose a realistic queue implementation, which could be used in real-world programs.
Since \mocaml is still at an experimental stage
and does not offer a wide variety of concurrent data~structures yet,
its ecosystem may benefit from this new library.
We take inspiration from a well-established algorithm~\cite{rigtorp} that has been used in production in several applications.
\item The specification of the concurrent queue should indicate that it behaves
as if all of its operations acted atomically on a common shared state,
even though in reality they access distinct parts of the memory and require many machine instructions.
To address this challenge, we use the recently-developed concept of \emph{logical atomicity}~\cite{iris-15,jung-slides-2019,da2014tada},
which we transport to the setting of \cosmo.
\item To the best of our knowledge, this is the first use of logical atomicity in a weak-memory setting.
This raises new questions: for instance, even though our implementation realizes
a total order on the operations on a queue,
it offers strictly weaker guarantees than
would be offered by
a coarse-grained implementation.
Indeed, in the context of a weak memory model, the specification of a concurrent data structure
must describe not only the result of its operations,
but also the manner in which these are synchronized,
that is, the \emph{happens-before} relationships that exist between these.
This additional information allows reasoning about accesses to areas of memory \emph{outside} of the data structure itself.
This is crucial, for example,
if a queue is used to transfer the ownership of a piece of memory
from a producer to a consumer:
there must exist a happens-before relationship between the \enqueue
operation and the corresponding \dequeue operation,
so as to ensure that the consumer acquires the producer's view of this piece of memory.
Our specification faithfully captures a subtle behavior of the implementation:
even though operations are totally ordered by logical atomicity,
not all operations are ordered by happens-before---but \emph{some} are.
\item We use the Coq proof assistant~\cite{coq} to formally verify our proofs.
Our development is available from our repository~\citep{repo}.
\end{itemize}

We believe our approach, whose key ingredients are Cosmo and logical atomicity,
scales to other memory models and other data structures.
Indeed, first, the core of Cosmo (beyond basic Separation Logic)
is a logic for reasoning with \emph{views},
an operational description of the memory model;
other memory models than that of \mocaml can also be termed in this fashion,
as iGPS~\cite{kaiser-17} and iRC11~\cite{dang-20} have demonstrated for C11.
Second, logical atomicity has already successfully been used
for various data structures in the Iris community~\cite{iris-examples,frumin2020reloc}.

The paper begins with a detailed explanation of the specification
of a concurrent queue~(\sref{sec:queue:spec}).
Then, we present an implementation of
the queue in \mocaml~(\sref{sec:queue:impl}), and explain our proof of its correctness~(\sref{sec:queue:proof}).
Next, we demonstrate that our specification is indeed usable,
by exploiting it in the context of a simple piece of client code,
where the concurrent queue is used to establish a pipeline between
a set of producers and a set of consumers~(\sref{sec:pipeline}).
The paper ends with a review of the related work~(\sref{sec:related}).
