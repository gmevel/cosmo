\subsection{Specification under Weak Memory}
\label{sec:queue:spec:weak}

\begin{figure}
\input{figure-hlog-axioms}
\bigskip
\input{figure-queue-spec-weak}
\end{figure}

Up to now, we have ignored the weakly consistent behavior of the semantics of \mocaml{}.
In this section, we take this aspect into account
and propose a refined specification.

We use the separation logic \hlog{}~\citecosmo,
which provides a proof framework for the weak memory model of \mocaml{}.
This memory model has been described by~\citet{dolan-18}
under the form of a small-step operational semantics.
Following previous authors, such as~\citet{kaiser-17},
the semantics features a notion of \emph{view}\footnote{\citet{dolan-18} call these views \emph{frontiers}.}
which captures the essence of weak memory,
namely,
the fact
that each thread has different knowledge of the state of the shared memory.
A view represents this subjective knowledge.
\hlog{} builds on this operational semantics,
and brings views to the level of the program logic.
All assertions of the logic depend on an \emph{ambient view}, which corresponds to the current view of the subject thread.
Thus, \hlog{} assertions are in general \emph{subjective},
that is, implicitly dependent on an ambient view.

Because \hlog{} is based on Iris, logically atomic triples can also be defined in \hlog.
In fact, the specification shown in~\fref{fig:queue:spec:sc} still applies.

Yet, as such, it is of little value in a weakly consistent context.
Indeed, as explained in~\sref{sec:queue:spec:sc}, it is designed so that
$\isQueueSC\elemList$ can be shared among threads by means of an invariant.
But, in \hlog{}, invariants are restricted to a special class of assertions
called \emph{objective} assertions%
---those assertions that do not actually depend on the ambient view.
%
Hence, our first addition to the specification is to stipulate that
the representation predicate is objective.
%
This reflects the fact that there exists a total order on the updates to the logical state,
on which all threads objectively agree.

% FP voilà les grandes lignes de ce que je dirais sur la linéarisabilité:
%
%    linearizability is a (complex) technical criterion
%    that is defined only in an SC setting
%      (because it assumes, as its starting point, that
%       we have a *totally ordered* trace of call and
%       return events),
%    so one must be careful not to use this word in a weak-memory setting,
%    unless one has defined its meaning (possible citation of Smith, Winter, and Colvin, 2018).
%
%    In an SC setting, the purpose of linearizability is to imply a refinement relation:
%      "the fine-grained concurrent data structure
%      behaves like coarse-grained data structure,
%      that is, a sequential data structure guarded by a lock".
%    Here, even though our concurrent queue has "linearizable
%    internal state" in a certain sense (there exists only one
%    instance of the predicate $\isQueueSC\elemList$, which
%    at every time keeps track of "the current logical state"
%    of the queue), it does \emph{not} quite behave like a
%    coarse-grained data structure,
%    because it imposes fewer \emph{happens-before}
%    relationships between accesses. Indeed, all accesses to a lock are totally ordered
%    by \emph{happens-before}, whereas our data structure guarantees
%    the existence of \emph{happens-before} relationships between
%    some, but not all, pairs of operations.

Even with this addition, the specification given in~\sref{sec:queue:spec:sc} is not precise enough
to verify interesting clients such as the one described in \sref{sec:pipeline}.
Indeed, in a weakly consistent setting, one typically expects a concurrent data structure to establish
synchronization between some of its concurrent accesses.
For example, imagine that thread~$A$ enqueues a pointer to a complex data structure (say, a hash table).
Then, when thread~$B$ dequeues this pointer, $B$ should obtain the unique ownership
of the hash table
and be able to access it accordingly.
In a weakly consistent memory model, $B$ expects to see all of the changes that $A$ has made to the data~structure.
This is guaranteed only if there is a \emph{happens-before} relationship from
the enqueuing event to the dequeuing event.

One possibility would be to guarantee that our concurrent queue implementation behaves like its coarse-grained alternative, that is, a sequential implementation guarded by a lock.
This would correspond to an intuitive definition of linearizability, even though this notion is difficult to define precisely outside of the world of sequential consistency~\cite{smith2019linearizability}.
However, our concurrent queue library is weaker than that: it does guarantee \emph{some} happens-before relationships, but not between all pairs of accesses.
Namely, it guarantees a happens-before relationship:
%
\begin{enumerate}%
  \item from an enqueuer to the dequeuer that obtains the corresponding item;
  \item from an enqueuer to the following enqueuers;
  \item from a dequeuer to the following dequeuers.
\end{enumerate}%
The first one permits resource transfer through the queue as described in the example above.

In \hlog{}, happens-before relationships can be expressed as \emph{transfers of views}.
To the user of this logic, views (denoted in this paper by calligraphic capital letters, such as $\mathcal{T,H,V,S}$) are abstract values
equipped with lattice structure:
the least view~$\emptyview$ is the empty view,
and larger views correspond to more recent knowledge.
Cosmo features two kinds of assertions to deal with views.
First, if $\view$ is a view, then
    the persistent assertion~$\seen\view$
    indicates that the current thread has the knowledge contained in~$\view$.
Second, if $\view$ is a view and $\prop$ is a (subjective) assertion, then
    $\prop \opat \view$
    denotes the assertion~$\prop$ where $\view$ has been substituted for the ambient view;
    as it does not depend on the ambient view anymore,
    $\prop \opat \view$ is objective.%
      \footnote{%
        An equivalent definition is the following:
        asserting $\prop \opat \view$ is asserting that, even without other knowledge, if we have the knowledge contained in~$\view$, then $\prop$ holds.
        In other words, $\prop \opat \view$ is objective and entails $\seen\view \wand \prop$.
        %The assertion~ $\prop$ requires no other knowledge than that of $\view$.
      }
\hlog{} provides reasoning rules about these assertions, shown in~\fref{fig:hlog:axioms}. We comment on rule~\splitso in the next paragraph.

In \hlog{}, the usual way of specifying a happens-before relationship between two program points is by giving the client the ability to transfer any assertion of the form $\seen\view$ between these two points: this corresponds to saying that the destination program point has all the knowledge the source program point had about the shared memory.
%
As seen later with the example of the pipeline~(\sref{sec:pipeline:proof}),
this is sufficient for transferring any subjective resource from a sender to a receiver.
Indeed, rule~\splitso says that we can split any subjective assertion~$\prop$ into two assertions $\seen \view$ and $\prop \opat \view$ for some view~$\view$
% (which happens to be the ambient view)
and, conversely, we can reconstruct $\prop$ from two such assertions.
The second part being objective, it can be shared in an invariant.
Hence, to transfer $\prop$, it is enough to transfer its subjective part~$\seen \view$.

In the specification of the queue,
to express the happens-before relationships mentioned earlier,
the representation predicate now takes more parameters:
%
\[
  \isQueue \tview \hview \elemViewList
\]

\begin{enumerate}%

  \item For each item~$\elem_\idx$ in the queue, we now have a corresponding
    view~$\eview_\idx$. This view materializes the flow of memory knowledge
    from the thread which enqueued the item, to the one which will dequeue it.

  \item The \textit{head} view~$\hview$ materializes memory knowledge
    accumulated by successive enqueuers.

  \item The \textit{tail} view~$\tview$ materializes memory knowledge
    accumulated by successive dequeuers.

\end{enumerate}%

The queue that we study, however, does not guarantee any happens-before relationship from
a dequeuer to an enqueuer.%
%
\footnote{%
  This is not entirely true: the implementation shown in \sref{sec:queue:impl} does create
  a happens-before relationship from the dequeuer of rank~$\idx$ to the enqueuer of
  rank~$\idx+\capacity$ (hence also to all enqueuers of subsequent ranks).
  We choose to not reveal this in the specification, since the constant~$\capacity$ is an implementation detail.
}
%
Hence, it provides fewer guarantees than a sequential queue guarded by a lock.

Interestingly, \hlog{} is able to express this subtle difference between the behavior of our library and that of a lock-based implementation:
the full specification under weak memory is shown in~\fref{fig:queue:spec:weak}.
%To simplify notation, we left the premise $\queueInv$ implicit for all logically atomic specifications.
This specification extends the previous one~(\fref{fig:queue:spec:sc}) with views.
The mentioned happens-before relationships are captured as follows.

\begin{enumerate}%

  \item When a thread with a local view~$\view$ (in other words, with
    $\seen\view$ as a precondition) $\enqueue$s an item~$\elem$, it pairs it
    with the view~$\view$.
    Afterwards, when another thread $\dequeue$s that same item~$\elem_0$, it
    merges the view~$\eview_0$ that was paired with it into its own local view
    (in other words, it obtains $\seen\eview_0$ as a postcondition).

  \item When a thread $\enqueue$s an item, it also obtains the head~view~$\hview$
    left by the previous enqueuer (in other words, it obtains $\seen\hview$ as
    a postcondition), and it adds its own view~$\view$ to the head~view (which
    becomes $\hview \viewjoin \view$).

  \item When a thread $\dequeue$s an item, it also obtains the tail~view~$\tview$
    left by the previous dequeuer (in other words, it obtains $\seen\tview$ as
    a postcondition), and it adds its own view~$\view$ to the tail~view (which
    becomes $\tview \viewjoin \view$).

\end{enumerate}%
