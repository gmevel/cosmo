\section{Related Work}
\label{sec:related}

The verification of fine-grained concurrent data structures is a well-studied problem with a particularly rich literature.
Several approaches were tried, targeting various verification frameworks, various data structures in different contexts.

The  notion of \emph{linearizability} is central for specifying such libraries.
\citet{dongol2015verifying} gave a survey of the different techniques used for linearizability of concurrent libraries at that time.
Of particular interest in the context of separation logic is the technique of \emph{logical atomicity}, which has been recently proved to be equivalent to linearizability~\cite{gueneau2021theorems} in the context of a sequentially consistent model.
This concept has been developed through several iterations over the last decade~\cite{da2014tada,iris-15,jacobs2011expressive,svendsen-birkedal-parkinson-hocap-13,jung-prophecies-20}.
In the present work, we adapt an unpublished, modern version of Iris's logically atomic triples~\cite{jung-slides-2019}, to the setting of \cosmo{}.
This is, to the best of our knowledge, the first use of logical atomicity in a weakly consistent setting.

% TODO : je ne sais pas quoi dire de ça. Dans la mesure où ce n'est qu'un preprint arxiv, je propose de ne pas en parler pour l'instant.
% Concurrent Data Structures Linked in Time
% Delbianco et al.
% https://germand.github.io/pubs/relink-ECOOP17.pdf
% c'est de la logique de séparation, avec preuve de linéarisabilité,
% et état fantôme pour l'historique

Another popular approach for proving the correctness of concurrent libraries is the use of refinement with respect to a simpler implementation.
This is the track chosen by ReLoC~\cite{frumin2018reloc}, which has recently been combined with logical atomicity~\cite{frumin2020reloc}.
Interestingly, ReloC has been recently used for proving the correctness of several concurrent queue implementations~\cite{vindum-birkedal-21,vindum-frumin-birkedal-21}, one of which is very close to ours.
However, these proofs do not handle relaxed memory behaviors, so that they do not provide a solution to the problem of specifying the lack of happens-before relationship between some data structure accesses, which we discussed in \sref{sec:queue:spec:weak}.
Because it lacks some happens-before relationships, our queue implementation \emph{is not} a refinement of a naive implementation which would use a lock to protect a sequential implementation, so a refinement-based approach would not be useful for proving our library correct.
The refinement approach has also been used to prove correct some data structures used in a concurrent garbage collector~\cite{zakowski2018verified}.


In a weakly consistent setting, new problems arise.
As discussed in \sref{sec:queue:spec:weak}, even the definition of linearizability needs special care.
\citet{smith2019linearizability} propose new definitions of linearizability for the case of weak memory models.
% Ce papier est le seul sur le sujet qui est vraiment publié ailleurs qu'arxiv. Vu que je n'ai pas lu les autres, je préfère ne pas les mentionner, de peur que ce soit n'importe quoi:
% On Library Correctness under Weak Memory Consistency
% https://www.soundandcomplete.org/papers/Libraries-POPL-2019.pdf
% Causal Linearizability, Doherty and Derrick 2016
% Des mêmes auteurs: https://arxiv.org/abs/1810.09612v1
In contrast, other authors have developed new kinds of specifications which allow for weak behaviors of the library itself~\cite{krishna-emmi-enea-jovanovic-20,emmi-enea-19,raad-19}.
We found that our method of combining logically atomic triples with views is expressive and allows for concise specifications at the same time.
%% % Constantin Enea, "Verifying Visibility-Based Weak Consistency".
%% % Technique de preuve à base de simulations (ce n'est pas de la
%% % logique de séparation), avec mise en oeuvre basé sur des prouveurs
%% % automatiques.
%% \nocite{krishna-emmi-enea-jovanovic-20}
%% \nocite{emmi-enea-19}
%% \begin{comment}
%% % Extrait de mon journal, POPL 2019:
%% Constantin Enea. "Weak-Consistency Specification via Visibility Relaxation".
%% Objets concurrents (dans le JDK par exemple) ont des specs faibles. Par
%% exemple, la méthode size() sur une collection renvoie un résultat plus ou
%% moins fiable s'il y a des updates en parallèle. Il cherche une façon simple et
%% générique de spécifier ces comportements relâchés. Approche axiomatique:
%% parler de linéarisation, mais parler aussi de visibilité. Pour chaque
%% linéarisation des opérations précédentes, on se demande lesquelles sont
%% visibles. Je n'y comprends pas grand-chose. Il y a des relations po, hb, etc.
%% En gros, c'est un modèle mémoire faible axiomatique, mais pas pour la mémoire
%% primitive; pour un objet concurrent avec des opérations add, remove, size,
%% etc. Tout ça donne des version relâchées de la notion de linéarisabilité, je
%% suppose. Ensuite, il y a du test; il veut annoter des méthodes du JDK avec des
%% specs dans son langage, et détecter (par le test) si ces specs sont bien
%% respectées. Je ne suis pas. Jens Palsberg demande comment on peut vérifier un
%% client qui utilise plusieurs objets différents avec des specs différentes.
%% Réponse pas très claire.
%% \end{comment}
Previous works include the generalization of various methods to weak consistency: \citet{le2013correctws,le2013correctfifo} used manual methods directly tied to the axiomatic memory model to prove the correctness of a queue and of a work-stealing algorithm, while \citet{lahav2015owicki} adapted the Owicki-Gries methodology to the release-acquire fragment of the C11 memory model and applied it to verify a read-copy-update library.
The idea of a separation logic for programs with a relaxed memory semantics has been first developed in the RSL logic~\cite{vafeiadis-narayan-13}, and further developed in subsequent work~\cite{turon-vafeiadis-dreyer-14,doko-vafeiadis-16,kaiser-17,dang-20,mevel-jourdan-pottier-20}.
None of these papers addressed the problem of the full functional correctness of a data structure.
In particular, the specification proposed for a circular buffer in GPS~\cite{turon-vafeiadis-dreyer-14} is a weak specification in the style of the persistent specification given at the end of \sref{sec:queue:spec:sc}: in contrast to ours, it does not specify in which order the elements leave the queue.


% TODO pour plus tard:



% Approche semi-automatisée sans mémoire faible:
% Automated Verification of CountDownLatch
% https://arxiv.org/abs/1908.09758
% Approche semi-automatisée avec mémoire faible (FSL automatisée dans Viper):
% https://www.cs.ubc.ca/~alexsumm/papers/SummersMueller18.pdf
%   => Dans les deux cas : pas pertinent pour ce papier. cela ne parle pas de véification de structure de données, ni d'atomicité logique


% model-checking
% https://www.cis.upenn.edu/~alur/Pldi07.pdf

% Checking Concurrent Data Structures Under the C/C++11 Memory Model
% https://dl.acm.org/doi/10.1145/3155284.3018749
% => model checking pour les structures de données en mémoire faible

% Verifying linearizability with hindsight
% O'Hearn et al.
% plus tard, cf. Guerraoui et al.
% et plus tard,
% Order out of Chaos: Proving Linearizability Using Local Views
% https://arxiv.org/pdf/1805.03992.pdf
% et encore plus tard,
% Proving Highly-Concurrent Traversals Correct
% https://arxiv.org/pdf/2010.00911.pdf


% Dodds et al. (2015)
% A Scalable, Correct Time-Stamped Stack
% pas de logique de séparation je crois, mais une preuve (manuelle?) de linéarisabilité
% pas de mémoire faible (utilisation d'atomiques SC de C11)
%   => Ça me semble pas intéressant pour nous. En plus, le focus est surtout sur l'algorithme lui-même plutôt que sur sa preuve.

% TODO : dans le style raffinement, il y a aussi les articles de Turon

% POPL 2007, Modular verification of a non-blocking stack. parkinson-bornat-ohearn-07
% Il semblerait que la spec est très faible : la stack n'est en fait, je crois, qu'un bag de pointeurs.
% quand on fait push, il faut donner l'ownership du pointer qu'on y met, quand on fait pop, on récupère un pointeur, mais on ne sait rien dessus (pas de spécification fonctionnelle)
% TODO : quoi dire là-dessus. Pour l'instant, j'en parle pas, ça a l'aire vraiment trop éloigné de ce qu'on fait.
