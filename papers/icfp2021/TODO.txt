# vim: set ft=text tw=80 et ts=2:

REM: nouvelle limite = 27 pages sans la biblio

À FAIRE SELON RÉPONSE AUX RAPPORTEURS:

- S 4.7: réécrire cette section sans écrire la preuve du début à la fin, mais
  un focus sur quelques instants importants de la preuve. « tel moment de la
  preuve est important parce que XXX. à ce moment, on dispose de YYY et il
  faut prouver ZZZ. ça marche parce que ΩΩΩ »


RELATED WORK:

- une demande sans réponse d’un rapporteur pour davantage de related work:
  > C "The general approach to specifying weak-memory data structures
  > probably needs further evaluation and comparison with other approaches"

  + reprendre et alléger le related work de Cosmo pour la partie mémoire faible / Cosmo
  + à approfondir: preuve de struct. de données concurrentes
    -> papier: Lars Birkedal et Simon Friis ont fait une preuve de queue (pas mémoire faible)
        Mechanized Verification of a Fine-Grained Concurrent Queue from Facebook’s Folly Library
        même structure que nous (algo légèrement différent) mais pas en mémoire faible
        (en attente de publication)
    -> papier: Lars Birkedal
        https://cs.au.dk/~birke/papers/2021-ms-queue-final.pdf
        queue de Michael-Scott
  + atomicité logique:
    -> papier: POPL 2011 Jacobs & Piessens
    -> papier: Philippa Gardner & Thomas Dinsdale-Young (TaDa)
    -> https://arxiv.org/abs/2006.13635
  + preuves en mémoire faible: pas grand-chose à priori
    -> papier: Nhat Minh Le
       Correct and Efficient Bounded FIFO Queues, 2013
       preuves directement dans le modèle axiomatique
       https://dblp.org/rec/conf/sbac-pad/LeGCP13.html?view=bibtex
       --> regarder les papiers qui le citent
    -> papier: POPL 2019 Azalea Raad
       --> récursif, regarder son related work
  + nous = 1er papier à présenter une struct de données concurrente en mémoire
    faible avec de la SL ??
