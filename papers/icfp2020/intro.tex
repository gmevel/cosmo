\begin{flushright}
  We are all in the gutter,
  but some of us are looking at the stars.
  --- \textit{Oscar Wilde}
\end{flushright}

\section{Introduction}
\label{sec:intro}

\mocaml~\cite{mocaml} extends the OCaml programming language~\cite{ocaml} with
support for shared-memory concurrency. It is an ongoing experimental project:
at the time of writing, although preparations are being made for its
integration into mainstream OCaml, this has not yet been done.
Nevertheless, \mocaml has a well-defined semantics: in particular, its
\emph{memory model}, which specifies how threads interact through shared
memory locations, has been published by \citet{dolan-18}.
Therefore, one may already ask: what reasoning rules can or should a \mocaml
programmer rely upon in order to verify their code?

\mocaml's memory model is \emph{weak}: it
does not enforce sequential consistency~\cite{lamport-79}. That is, the
execution of a program is not necessarily an interleaving of the actions
performed by its threads while interacting with a central shared memory. Many
mainstream programming languages, including lower-level languages such as
C11~\cite{c11mm,rc11} and higher-level languages such as
Java~\cite{jmm,lochbihler-jmm-12,bender-palsberg-19}, adopt weak memory
models, because this allows compilers
to perform more aggressive software optimizations and to better exploit
hardware optimizations.
% thereby achieving greater performance.
\mocaml follows
this trend. Although it is expected that most application programmers will
not need to worry about weak memory, because they will rely on a library of
concurrent data structures written by domain experts, adopting a weak memory
model allows said experts to write more efficient code---if they know what
they are doing, that is!

We believe that there is a need for a set of reasoning rules---in other words,
a program logic---that both populations of programmers can rely upon.
Concurrency experts, who wish to implement efficient concurrent data
structures, must be able to verify that their code is correct, even though
they have removed as many synchronization operations as they thought
possible. Furthermore, they must be able to verify that their code implements
a simple, high-level abstraction, thereby allowing application programmers, in
turn, to use it as a black box and reason about
application code in a simple manner. In short, the system must allow
compositional reasoning, and must allow both low-level reasoning, where the
details of the \mocaml memory model are apparent, and high-level reasoning,
which is independent of this memory model.

% Our terminology in this paper:
%   low-level = specific to the \mocaml memory model
%   high-level = independent of the memory model
% Our base logic \llog is definitely low-level.
% Our derived logic \hlog is also low-level
%   but includes a high-level fragment
%   where one can be unaware of the memory model
%   (e.g., by using only locks and nonatomic locations).

Others before us have made a similar argument. For instance,
\citet{sieczkowski-15} propose iCAP-TSO, a variant of \CSL that is sound with
respect to the TSO memory model. While iCAP-TSO allows explicit low-level
reasoning about weak memory, it also includes a high-level fragment, the ``SC
logic'', whose reasoning rules are the traditional rules of \CSL. These rules,
which are independent of the memory model, require all direct accesses to
memory to be data-race-free. Therefore, they require synchronization to be
performed by other means. A typical means of achieving synchronization is to
implement a lock, a concurrent data structure whose specification can be
expressed in the SC logic, but whose proof of correctness must be carried out
at the lower level of iCAP-TSO.
%
As another influential example, \citet{kaiser-17} follow a similar approach:
they instantiate Iris~\cite{iris}, a modern descendant of \CSL, for a fragment of
the C11 memory model. This yields a low-level ``base logic'', on top of which
Kaiser et al.\ proceed to define several higher-level logics, whose reasoning
rules are easier to use.
%
% I am not saying very clearly whether the higher-level logics
% abandon some expressive power compared to the base logic.
% I suppose they do, but I don't know exactly what is lost.
%
Our aim, in this paper, is analogous. We wish to allow the verification of a
low-level concurrent data structure implementation, such as the implementation
of a lock. Such a verification effort must take place in a program logic that
exposes the details of the \mocaml memory model. At the same time, we would
like the program logic to offer a high-level fragment that is independent of
the memory model and in which data-race-free accesses to memory, mediated by
locks or other concurrent data structures, are permitted.

Compared with other memory models in existence today, the \mocaml
memory model is relatively simple. Only two kinds of memory locations,
known as ``nonatomic'' and ``atomic'' memory locations, are distinguished.
%
Every program has a well-defined set of permitted behaviors. In particular,
data races on nonatomic memory locations are permitted, and have ``limited
effect in space and in time'' \cite{dolan-18}. This is in contrast with the
C11 memory model, where racy programs are deemed to have ``undefined
behavior'', therefore must be ruled out by a sound program logic.
%
\citet{dolan-18} propose two definitions of the \mocaml memory model,
and provide an informal proof that these definitions are equivalent. One
definition takes the form of an operational semantics, where the execution of
the program is in fact an interleaving of the executions of its threads.
These threads interact with a memory whose behavior is more complex than
usual, and whose description involves concepts such as \emph{time stamps},
\emph{histories}, \emph{views}, and so on (\sref{sec:lang}). The other
definition is an axiomatic semantics, where a ``candidate execution'' takes
the form of a set of memory events and relations between them, and an actual
execution is a candidate execution that respects certain conditions, such as
the acyclicity of certain relations.

In this paper, we take Dolan \etal.'s operational semantics, which we recall
in Section~\ref{sec:lang}, as a foundation. We instantiate Iris
for this operational semantics. This yields a
low-level logic, \llog (\sref{sec:logic:lo}), whose reasoning rules expose the
details of the \mocaml memory model.
% These rules are sound and possibly complete (a fact that we do not attempt
% to prove).
Because of this, these rules are not very pleasant to use. In particular, the
rules that govern access to nonatomic memory locations are rather unwieldy, as
they expose the fact that the store maps each nonatomic location to a history,
a set of write events.
%
In order to facilitate reasoning, on top of \llog, we build a higher-level
logic, \hlog (\sref{sec:logic:hi}), whose main features are as follows:
%
\begin{itemize}
\item
  % 1- On se restreint en expressivité afin de gagner en simplicité.
  \hlog forbids data races on nonatomic locations.
  Data races on atomic locations remain permitted:
  atomic locations are in fact
  the sole primitive means of synchronization.
  This design decision allows \hlog to offer
  a simplified set of reasoning rules,
  including:
  \begin{itemize}
  \item standard, straightforward rules for
    data-race-free access to nonatomic locations;
  \item standard, straightforward rules for possibly racy access to atomic
    locations, with the ability of exploiting the sequentially-consistent
    behavior of these locations; and
  \item nonstandard yet arguably simple rules for reasoning about
    the release/acquire behavior of atomic locations.
  \end{itemize}
  The last group of rules allow transferring a ``view'' of the nonatomic
  memory from one thread to another. By exploiting this
  mechanism, one can arrange to transfer an arbitrary assertion~$P$
  from one thread to another, even when the footprint of~$P$ involves nonatomic
  memory locations. This feature is used in all of our case studies
  (\sref{sec:examples}).

  Although views have appeared in several papers
  \cite{kaiser-17,dang-20}, letting the user reason about
  release/acquire behavior in terms of abstract views seems
  novel and simpler than previous approaches.  In particular, we
  claim that combining objective invariants and the axiom
  \splitso{} yields a simple reasoning scheme, which could be exploited in
  logics for other weak memory models.
\item
  % 2- (Optionnel) Si on se restreint à un fragment de la logique \hlog,
  %    alors on peut raisonner de façon indépendante du modèle mémoire.
  \hlog offers a high-level fragment
  where reasoning is independent of the memory model,
  that is,
  a~fragment whose reasoning rules are those of traditional \CSL.
  % Should we name this fragment?
  % Probably not, as we would need one more name,
  % and several fragments of Cosmo are actually memory-model-independent
  % e.g. with or without direct access to SC locations.
  In this fragment, there is no notion of view.
  This fragment consists at least of the rules that govern access to nonatomic locations
  % (that is, the rules of the first group above)
  and can be extended by allowing the use of concurrent data structures
  whose specification is independent of the memory model % i.e., no views are involved
  and whose correctness has been verified using full \hlog.
  The spin lock and ticket lock~(\sref{sec:examples})
  are examples of such data structures:
  their specification in \hlog
  is the traditional specification of a lock in \CSL
  \cite{gotsman-aplas-07,hobor-oracle-08}. % buisse-11,sieczkowski-15
  Thus, \hlog contains traditional \CSL,
  and allows reasoning about coarse-grained concurrent programs
  where all accesses to
  memory locations are mediated via locks.
  % (nonatomic memory locations only, in that case)
  % (one can also allow SC locations with races and invariants,
  %  but that's already a larger fragment)
\end{itemize}

% fp: déjà dit implicitement plus haut, on pourrait couper:
We illustrate the use of \hlog via several case studies (\sref{sec:examples}),
including a spin lock, a ticket lock, and a lock based on Peterson's algorithm.
% for implementing mutual exclusion between two threads
% by exploiting only read and
% write instructions on atomic memory locations.
%
% (no CAS)
%
All of our results, including the soundness of \hlog and the verification of our
case studies, are machine-checked in Coq.
%
Our proofs can be browsed in the accompanying artifact
and are also available from our repository~\cite{repo}.

%%  LocalWords:  OCaml dolan optimizations compositional sieczkowski iCAP TSO
%%  LocalWords:  et al logics nonatomic lang acyclicity Coq
