\section{A Low-Level Logic: \llog}
\label{sec:logic:lo}

% ------------------------------------------------------------------------------

% A general presentation of the Iris ``functor''.

In this section, we set up a program logic for \mocaml, based on the
operational semantics presented in the previous section. To do so, we rely on
Iris~\cite{iris}, a
generic framework for building program logics. Iris is not tied to a particular
programming language or calculus. Its lower layer, the Iris base
logic~\cite[\S3--5]{iris}, is a purely logical construction. Its upper layer,
the Iris program logic~\cite[\S6--7]{iris}, is parameterized with a
programming language. In order to instantiate it, a client must provide
the following information:
% \cite[\S7.3]{iris}
%
\begin{itemize}
\item A set of ``expressions''.
\item A subset of ``values''.
\item A set of machine ``states''.
      For instance, a state might be a store, that is,
      a map whose domain is the set of all currently allocated memory locations.
      % éviter de dire ``a map of memory locations to values'',
      % car ce ne seraient pas les mêmes valeurs que mentionné ci-dessus.
\item An operational semantics,
      in the form of a ``per-thread step relation''.
      This relation relates an expression and a state
      to an expression and a state
      and a list of expressions, which represent newly-spawned threads.
      % Rel (expr*state) (expr*state*list expr).
      % This relation must have the property that a value cannot make a step.
      %
      % The thread-pool reduction relation is given by Iris, I suppose.
\item A ``state interpretation'' predicate $\stateinterp : \typename{State}\ra\typeIProp$.%
      \footnote{$\typeIProp$ is the type of Iris assertions.}
      This predicate represents a global invariant about the machine state.
      % It appears in the definition of wp (not yet mentioned).
      It typically relates the state
      with a piece of ghost state
      whose monoid structure%
      \footnote{For simplicity, we use the word ``monoid'' everywhere,
        although ``partial commutative monoid'' would be more explicit,
        and an even more accurate term would be ``resource algebra''
        \cite[\S2.1]{iris} or ``camera''~\cite[\S4.4]{iris}.}
      is chosen by the client so
      as to justify splitting the ownership of the machine state
      in certain ways.
      % This ghost state typically appears
      % in the definition of client assertions
      % such as the ``points-to'' assertion,
      % which represents the ownership of a part of the state.
      % (not yet mentioned)
\end{itemize}
Once the client has provided this information, the
framework yields a program logic, that is,
%
\begin{itemize}
\item A weakest-precondition predicate $\WP\expr\pred$.
\item A Hoare triple $\hoare\prop\expr\pred$,
      which is just sugar for $\always{(\prop\wand\WP\expr\pred)}$.
      % \cite[\S6]{iris}.
\item An adequacy theorem,
      % \cite[\S6.4]{iris}
      which states % (roughly)
      that if a closed program~$\expr$ satisfies the triple
      $\hoare\TRUE\expr\pred$
      then it is safe to run, % in an empty initial store
      that is, its execution will not lead to a stuck configuration. % ``go wrong''.
\item A set of programming-language-independent deduction rules for triples.
      These include the consequence rule, the frame rule,
      rules for allocating and updating ghost state,
      rules for setting up and exploiting invariants,
      and so on.
\end{itemize}
%
It is then up to the client to perform
extra programming-language-specific work,
namely:
%
\begin{itemize}
\item Define programming-language-specific assertions,
      such as ``points-to'' assertions.
\item Prove
      % programming-language-specific
      entailment laws
      % that involve these assertions,
      describing, e.g., how points-to assertions
      can be split and combined.
\item Establish programming-language-specific deduction rules % (or axioms)
      for triples,
      e.g., axioms that give triples for reading and writing memory locations.
      % The preconditions and postconditions of these triples
      % typically involve points-to assertions.
\end{itemize}
%
We now apply this recipe to \mocaml. This yields \llog, a
logic for \mocaml.

% ------------------------------------------------------------------------------

% How we apply the Iris functor.

\subsection{Instantiating Iris for \mocaml}
\label{sec:logic:lo:instantiation}

% cf. theories/lang/lang.v
% The language is named view_lang.

We begin instantiating Iris as follows:
%
\begin{itemize}
\item An ``expression'' is a pair $\mkexpr\expr\view$
      of a \mocaml expression and a view.
\item Accordingly, a ``value'' is a pair $\mkval\val\view$
      of a \mocaml value and a view.
\item A ``state'' is a store $\store$,
      that is, a pair $\pair{\store\NA}{\store\AT}$
      of a nonatomic store $\store\NA$
      and an atomic store $\store\AT$.
\item The ``per-thread step relation''
      is as defined in \fref{fig:reduction}.
\end{itemize}

To complete this instantiation, there remains to define a suitable ``state
interpretation'' invariant~$\stateinterp$. In choosing this definition, we
have a great deal of freedom. Our choice is guided by several considerations,
including the manner in which we wish to allow splitting the ownership of the
state, and the invariants about the state that we wish to keep track of. In
the present case, we have the following two independent concerns in mind:
%
\begin{itemize}
\item[---] We wish to allow splitting the ownership of memory locations
  (including nonatomic and atomic memory locations) under a standard
  ``fractional permissions'' regime.
\item[---] We need to keep track of the global view invariant
  (\sref{sec:lang:semantics})
  enjoyed by the operational semantics of \mocaml,
  because this invariant is required to justify
  that a nonatomic read instruction cannot be stuck.
\end{itemize}
%
With these goals in mind, we define our state interpretation invariant as
follows. We give the definition first, then recall the Iris-specific notation
used in this definition. We delay an explanation of the definition to the next
subsection (\sref{sec:logic:lo:assertions}).
%
\begin{itemize}
\item Let $\gname\NA$ be a ghost location
      storing an element of
      \(
        \authm (\typeNALoc \maparrow \agm(\typeHist) \times \fracm).
      \)
\item Let $\gname\AT$ be a ghost location
      storing an element of
      \(
        \authm (\typeATLoc \maparrow
                \agm (\typeVal \times \typeView) \times \fracm).
      \)
\item Let $\gnameglobalview$ be a ghost location storing an element of
      $\authm(\typeView)$.%
      \footnote{
        $\typeView$ is the monoid of views equipped with the operation
        $\viewjoin$. It is an idempotent monoid.}
\item We define the state interpretation invariant as follows:
  \[
    \stateinterp\,\pair{\store\NA}{\store\AT}
    \;\;\eqdef\;\;
    \ownGhost{\gname\NA}{\authfull{\store\NA}}
    \isep
    \ownGhost{\gname\AT}{\authfull{\store\AT}}
    \isep
    \Exists \globalview.%
    \ISep{%
      \ownGhost{\gnameglobalview}{\authfull\globalview}
      \cr
      \liftobj{%
        \All (\atloc, \mkval{\val}{\viewatloc}) \in \store\AT.%
        \viewatloc \viewleq \globalview
      }
      \cr
      \liftobj{%
        \All (\naloc, \hist) \in \store\NA.%
        \viewapp\globalview\naloc \in \Dom\hist
      }
      \cr
      \liftobj{%
        \All \naloc \notin \Dom{\store\NA}.%
        \viewapp\globalview\naloc = \timeMin
      }
      % On conserve le plongement explicite de Prop dans iProp.
    }
  \]
% fp: il y a abus de langage car on convertit implicitement un store
%     en un élément du monoïde approprié, avec fraction 1 partout.
\end{itemize}

Let us briefly recall some Iris concepts and notation.
%
The monoids mentioned above are built out of the standard Iris toolkit.
%
An authoritative monoid $\authm(\cdot)$ \cite[\S6.3.3]{iris} has both
\emph{authoritative elements} of the form $\authfull{a}$ and
\emph{fragmentary elements} of the form~$\authfrag{b}$.
An authoritative element is intended to represent the full knowledge
of something, and cannot be split:
the composition $(\authfull{a_1}) \cdot (\authfull{a_2})$ is never valid.
A fragmentary element is intended to represent partial knowledge,
and can be split and joined:
the composition $(\authfrag{b_1}) \cdot (\authfrag{b_2})$ is
defined as $\authfrag{(b_1\cdot b_2)}$.
Because a fragment must be a part of the whole,
the composition $(\authfull{a}) \cdot (\authfrag{b})$
is valid only if $b \preccurlyeq a$ holds,
where $\preccurlyeq$ is the ordering induced by the monoid law.
%
The finite map monoid $\cdot\maparrow\cdot$
% arbitrary type on the left, monoid on the right
has finite maps as its elements;
composition is pointwise.
%
% The product monoid $\cdot\times\cdot$, ...
%
The agreement monoid \cite[\S3.1, \S4.3]{iris} on histories $\agm(\typeHist)$
has histories~$\hist$ as elements. Its composition law requires agreement:
that is, $\hist\cdot\hist$ is $\hist$, and $\hist_1\cdot\hist_2$ is invalid
if $\hist_1$ and $\hist_2$ differ.
%
The fractional permission monoid $\fracm$ has the fractions \(q \in \Q
\cap (0, 1]\) as its elements and addition as its composition law. Note that,
because the fraction~$0$ is excluded, the fraction~$1$ cannot be composed with
any fraction. This reflects the intuitive idea that the fraction~$1$ represents
exclusive ownership of something.

% ------------------------------------------------------------------------------

% Specific assertions for \mocaml.

\subsection{\mocaml-Specific Assertions}
\label{sec:logic:lo:assertions}

We have performed the first step described at the beginning of this
section~(\sref{sec:logic:lo}): we have instantiated Iris for the
operational semantics of \mocaml and for a state interpretation of our
choosing. This yields a weakest-precondition predicate
$\WP{\mkexpr\expr\view}\pred$; a~triple $\hoare\prop{\mkexpr\expr\view}\pred$;
an adequacy theorem, which guarantees that this triple is sound;
and a set of programming-language-independent deduction rules for triples.
There remains to perform programming-language-specific work.
%
We define three custom assertions, namely the nonatomic and atomic
``points-to'' assertions and a ``valid-view'' assertion.
%
Next (\sref{sec:logic:lo:axioms}), we give a set of
reasoning rules where these assertions appear.

% Note: \aginj is kept implicit for readability.

\subsubsection{Nonatomic Points-to}

We wish to be able to split up the ownership of the nonatomic store under a
fractional permission regime. As
usual~\cite{boyland-fractions-03,bornat-permission-accounting-05}, the
fraction~1 should represent exclusive read-write access, while a fraction
$q<1$ should represent shared read-only access. Furthermore, we wish to ensure
that whoever owns a share of a nonatomic location has exact knowledge of its
history.

For this purpose, we have placed the ghost-state-ownership assertion
$\ownGhost{\gname\NA}{\authfull{\store\NA}}$ in the state interpretation
invariant (\sref{sec:logic:lo:instantiation}), and we now define the nonatomic
points-to assertion as follows:
%
\[
  \histNA[q]\naloc\hist
  \;\eqdef\;
  \ownGhost{\gname\NA}
    {\authfrag
      {\mapliteral
        {\mapbinding\naloc{\pair\hist q}}
      }
    }
\]
%

\newcommand{\footnoteabuse}{\footnote{%
The notation $\authfull{\store\NA}$ involves an abuse of notation.
In it, $\store\NA$ must be understood as an element of the monoid
$\typeNALoc \maparrow \agm(\typeHist) \times \fracm$,
which maps every location~$\naloc$ in the domain of $\store\NA$
to a pair of the history $\storeapp{\store\NA}{\naloc}$
and the fraction 1.}}

What is going on here?
%
On the one hand,
the points-to assertion claims the ownership of a fragmentary element of the monoid
$\authm (\typeNALoc \maparrow \agm(\typeHist) \times \fracm)$.
The notation
$\mapliteral{\mapbinding\naloc{\pair\hist q}}$
describes an element of the monoid
$\typeNALoc \maparrow \agm(\typeHist) \times \fracm$:
it is a~singleton map,
which maps the location~$\naloc$ to the pair $\pair\hist q$.
%
On the other hand, the state interpretation invariant owns the authoritative
element $\authfull{\store\NA}$ of the monoid
$\authm(\ldots)$.\footnoteabuse{}
% $\authm(\typeNALoc \maparrow \agm(\typeHist) \times \fracm)$.
%
This ties the nonatomic store
$\store\NA$, which is part of the physical machine state, with the state of
the ghost location~$\gname\NA$.

Now, recall that, from the coexistence of the elements $\authfull{a}$ and
$\authfrag{b}$, one can deduce $b \preccurlyeq a$
(\sref{sec:logic:lo:instantiation}).
%
Thus, when a points-to assertion $\histNA[q]\naloc\hist$ is at hand,
one can unfold the definition of this assertion and get a fragmentary
element $\authfrag{\mapliteral{\mapbinding\naloc{\pair\hist q}}}$.
%
One can also briefly ``open'' the state interpretation invariant and see an
authoritative element $\authfull{\store\NA}$.
%
From their coexistence, one deduces
$\mapliteral{\mapbinding\naloc{\pair\hist q}} \preccurlyeq \store\NA$,
that is, the singleton map of~$\naloc$ to the pair $\pair\hist q$ is
indeed a fragment of the physical nonatomic store $\store\NA$.
% again viewed, by abuse of language, as a monoid element with fraction 1
% everywhere.
%

Here, $\preccurlyeq$ is the ordering induced by the composition law of the
monoid $\typeNALoc \maparrow \agm(\typeHist) \times \fracm$. Because this law
requires agreement of the history components and addition of the fraction
components, one can read
the assertion $\histNA[q]\naloc\hist$ as representing
the knowledge that the history of this location is~$\hist$
and
the ownership of a $q$-share of the nonatomic memory location~$\naloc$.

This technique is not original:
we follow the pattern presented by \citet[\S6.3.3, \S7.3]{iris}.

We omit the fraction~$q$ when it is $1$.
Thus, we write $\histNA\naloc\hist$ for $\histNA[1]\naloc\hist$.

\subsubsection{Atomic Points-to}

Regarding the atomic points-to assertion, we proceed essentially in the same
manner. Earlier (\sref{sec:logic:lo:instantiation}),
we have placed the assertion
$\ownGhost{\gname\AT}{\authfull{\store\AT}}$
in the state interpretation invariant.
%
We now define the atomic points-to assertion as follows:
%
\[
  \pointstoAT[q] \atloc {\mkval\val\view}
  \;\eqdef\;
  \Exists \altview.\;
  \liftobj{\view \viewleq \altview}
  \isep
  \ownGhost{\gname\AT}
    {\authfrag
      {\mapliteral
        {\mapbinding\atloc{\pair{\mkval\val\altview}q}}
      }
    }
\]
%
As a result of these definitions,
the assertion $\pointstoAT[q] \atloc {\mkval\val\view}$
claims the ownership of a $q$-share of the atomic memory location~$\atloc$,
guarantees that the value stored at this location is~$\val$, and
guarantees that the view  stored at this location is at least~$\view$.

By requiring the view $\altview$ stored at~$\atloc$ to
satisfy $\view\viewleq\altview$, as opposed to the equality $\view=\altview$,
we make the points-to assertion reverse-monotonic in its view
parameter: that is, if $\view_1\viewleq\view_2$ holds,
then $\pointstoAT[q] \atloc {\mkval\val{\view_2}}$
entails $\pointstoAT[q] \atloc {\mkval\val{\view_1}}$.
%
This seems convenient in practice, as it gives the user a concise way
of retaining partial knowledge of the view that is stored at location~$\atloc$.
%
% By building reverse monotonicity into the points-to assertion in this way,
% we do lose the ability of expressing negative information about the view,
% that is, an upper bound on the view. This should not be a problem in
% practice, as one always reasons about events that *must* be part of a view,
% and never about events that *must not* be part of a view. In fact, in the
% high-level logic, assertions are *monotonic* functions of a view, so this
% style of reasoning becomes built-in.
%

As with nonatomic points-to assertions, we omit the fraction~$q$ when it is $1$.

\subsubsection{Validity of a View}

The last part of the state interpretation invariant
(\sref{sec:logic:lo:instantiation})
asserts the existence of a view $\globalview$
such that items~(\ref{gvi2}), (\ref{gvi3}) and (\ref{gvi4})
of the global view invariant hold (\sref{sec:lang:semantics}).
It also includes the ghost-state-ownership assertion
$\ownGhost{\gnameglobalview}{\authfull\globalview}$.
%
We now define the ``valid-view'' assertion as follows:
%
\[
  \valid\view
  \;\eqdef\;
  \ownGhost\gnameglobalview
    {\authfrag\view}
\]

The assertion $\valid\view$ guarantees that $\view$ is a fragment of the
global view, that is, $\view\viewleq\globalview$ holds.
%
Because the $\typeView$ monoid is idempotent (that is,
$\view\viewjoin\view=\view$), this assertion is duplicable. Thus, it does not
represent the ownership of anything. It represents only the knowledge that
$\view$ is a fragment of the global view.
%

A validity assertion appears in almost all of the reasoning rules that we
present in the next subsection (\sref{sec:logic:lo:axioms}).
%
In other words, the rules effectively require (and allow) every thread to keep
track of the fact that its current view is valid. This encodes
item~(\ref{gvi1}) of the global view invariant.
%
\begin{comment}
The root cause of this phenomenon lies in the rule \basenaread, where the
global view invariant must be exploited in order to prove that a nonatomic
read instruction can make progress. As a secondary cause, in almost every
rule, a validity hypothesis is required in order to establish the preservation
of the state interpretation invariant.
\end{comment}

% ------------------------------------------------------------------------------

% Specific deduction rules (or axioms) for \mocaml.

\subsection{\mocaml-Specific Axioms}
\label{sec:logic:lo:axioms}

There remains to give a set of \mocaml-specific deduction rules that allow
establishing Hoare triples.
%
We present just the rules that govern the memory access operations;
the rest is standard~\cite[\S6.2]{iris}.
%
These rules appear in \fref{fig:llog:rules}.%
\footnote{%
  For the sake of readability, we hide the ``later'' modalities
  that appear in these rules. See \citet[\S6.2]{iris} for details.}
%
They are ``small axioms''~\cite{ohearn-19}, that is, triples that
describe the minimum resources required by each operation.
%
Each of them is just a single triple $\hoare\prop{\mkexpr\expr\thisview}\pred$,
which, for greater readability, we display vertically:
%
\begin{mathpar}
  \HoareRule{}
    \prop
    {\mkexpr\expr\thisview}
    \pred
\end{mathpar}
%

The precondition $\prop$ describes the resources required in order to safely
execute the expression $\expr$ on a thread whose view is $\thisview$. The
postcondition $\pred$, which takes a pair $\mkval{\val'}{\thisview'}$ of a
value and a new view as an argument, describes the updated resources that
exist at the end of this execution, if it terminates. This is a logic of
partial correctness.

The word ``axiom'' is used because each rule is just a fact of the form
``triple'', as opposed to an implication of the form ``triple implies
triple''. This does \emph{not} mean that we accept these rules without
justification. Each ``axiom'' in \fref{fig:llog:rules} is in reality
a~lemma that we prove.

\input{figure-baselogic-rules}

\begin{comment}
% fp: chapeau coupé pour économiser de la place.
There are eight axioms in \fref{fig:llog:rules}: three axioms for allocating,
reading, and writing nonatomic memory locations; three axioms for allocating,
reading, and writing atomic memory locations; and two axioms for successful and
unsuccessful CAS instructions on atomic memory locations.
\end{comment}

Allocating a nonatomic memory location (\basenaalloc)
returns a memory location~$\naloc$ and does not change the view of
the current thread. The points-to assertion
$\histNA\naloc{\mapliteral{\mapbinding 0\val}}$
in the postcondition
represents the full ownership of the newly-allocated memory location
and guarantees that its history contains a single write
of the value~$\val$ at time stamp~$0$.

Reading a nonatomic memory location (\basenaread) requires (possibly shared)
ownership of this memory location, which is why the points-to assertion
appears in the precondition $\histNA[q]\naloc\hist$.
%
What can be said of the value~$\val'$ produced by this instruction?
%
A nonatomic read can read from any write whose time stamp is high enough,
according to this thread's view of the location~$\naloc$. Thus, for some time
stamp~$\timest$ such that $\timest \in \Dom\hist$ holds (i.e., $\timest$ is a
valid time stamp) and $\viewapp\thisview\naloc \timeleq \timest$ holds (i.e., the
view $\thisview$ allows reading from this time stamp), the value~$\val'$ must
be the value that was written at time $\timest$, that is, $\histapp\hist\timest$.
%
The thread's view is unaffected,
and the points-to assertion is preserved.

In order to justify a nonatomic read, the validity of the current view is
required: the assertion $\valid\thisview$ appears in the precondition of
\basenaread. Without this requirement, we would not be able to establish this
triple. Indeed, we must prove that this read instruction can make progress,
that is, it cannot be stuck. In other words, we must prove that the
history~$\hist$ contains a write event whose time stamp is at least
$\viewapp\thisview\naloc$. Quite obviously, in the absence of any hypothesis about
the view~$\thisview$, it would be impossible to prove such a fact.
%
Thanks to the validity hypothesis $\valid\thisview$,
we find that that $\thisview$ must be a fragment of
the global view~$\globalview$. This implies
$\viewapp\thisview\naloc \timeleq \viewapp\globalview\naloc$.
Furthermore, the state interpretation invariant guarantees
$\viewapp\globalview\naloc \in \Dom\hist$.
%
Therefore, this read instruction can read (at least)
from the write event whose time stamp is $\viewapp\globalview\naloc$.
Therefore, it is not stuck.

Writing a nonatomic memory location (\basenawrite) requires exclusive
ownership of this memory location, which is expressed by the points-to
assertion $\histNA\naloc\hist$. It also requires the validity of the current
view~$\thisview$, as this information is needed to prove that
the updated view~$\thisview'$
is valid. In accordance with the operational semantics, the history~$\hist$ is
extended with a write event at some time stamp~$\timest$ that is both fresh
for the history~$\hist$ and permitted by the view~$\thisview$. The updated
points-to assertion $\histNA\naloc{\mapupdate\hist\timest\val}$ reflects this
updated history. The thread's new view~$\thisview'$ is obtained by
updating~$\thisview$ with a mapping of~$\naloc$ to the time stamp~$\timest$.

The axioms that govern atomic memory locations are also a little heavy, due to
the fact that atomic memory locations play two independent roles: they store a
value and a view. Regarding the ``value'' aspect, these axioms are identical
to the standard axioms of \CSL with fractional permissions. This reflects the
sequentially consistent behavior of atomic memory locations.
Regarding the ``view'' aspect, these axioms describe how views are written and
read by the atomic memory instructions. This reflects the
``release/acquire'' behavior of these instructions.

When an atomic memory location is allocated (\baseatalloc),
it is initialized with the view~$\thisview$ of the current thread.
This is expressed by the points-to assertion
$\pointstoAT \atloc {\mkval\val\thisview}$
in the postcondition.

When an atomic memory location is read (\baseatread),
the view stored at this location
is merged into the view of the current thread:
this is an ``acquire read''.
This is expressed by the inequality
$\viewatloc \viewjoin \thisview \viewleq \thisview'$.
We cannot expect to obtain an equality
$\viewatloc \viewjoin \thisview = \thisview'$
because, according to our definition of the atomic points-to assertion
(\sref{sec:logic:lo:assertions}),
$\view$ is only a fragment
of the view that is stored at location~$\atloc$.
Thus, we get only a lower bound on the
thread's new view~$\thisview'$.
% jh: peut-être faut-il préciser que cela n'a pas d'impact sur
% l'expressivité...
% fp: oui, mais le texte est déjà long.
Nevertheless, we can prove that $\thisview'$ is valid.

When performing an atomic write (\baseatwrite),
the same ``acquisition'' phenomenon occurs: we get
$\viewatloc \viewjoin \thisview \viewleq \thisview'$.
Furthermore, this thread's view is merged into the view
stored at this location: this is a ``release write''.
This is expressed by the updated points-to assertion
$\pointstoAT \atloc {\mkval\val{\viewatloc \viewjoin \thisview}}$.

The two axioms associated with the CAS instruction respectively describe the
case where this instruction fails (\basecasfailure) and the case where it
succeeds (\basecassuccess). These axioms express the idea that a~CAS behaves
as a read followed (in case of success) with a write.

% ------------------------------------------------------------------------------

% The soundness theorem for the base logic.

\subsection{Soundness of \llog}
\label{sec:logic:lo:soundness}

By relying on Iris's generic soundness theorem, we
establish the following result.
%
Recall that $\emptyview$ is the empty view and $\mapempty$ is the empty store.
The theorem states that, if the user can prove anything about $\expr$
using \llog, then $\expr$ is safe, that is, its execution cannot
lead to a stuck configuration.

\begin{theorem}[Soundness of \llog]
  % For any expression~$\expr \in \typeExpr$ and any
  % pure predicate~$\pred : \typeVal \times \typeView \rightarrow \typename{Prop}$,
  If the entailment
  ``$\valid\emptyview \vdash \WP {\mkexpr\expr\emptyview} {\TRUE}$'' holds,
  %
  % glen: abus de langage sur "holds" ici, on parle d’une assertion de \llog
  % fp: si on écrit \vdash et que l'on appelle cela un entailment,
  %     cela devient une affirmation de type Prop.
  %
  then the configuration $\mapempty; \mkexpr\expr\emptyview$
  is safe to execute.
  % is adequate with respect to~$\pred$.
\end{theorem}

\begin{comment}
\emph{Adequacy} is the conjunction of two properties:
%
\begin{itemize}
  \item no reduction sequence starting from this configuration gets stuck,
    that is, for any reduction sequence
    $\mapempty; \mkexpr\expr\emptyview \threadPoolReduces^*
    \store'; \mkexpr{\expr'}{\view'}, \pool'$,
    either $\expr'$ is a value or $\store'; \mkexpr{\expr'}{\view'}$ reduces;
  \item any value computed by a reduction sequence starting from this
    configuration satisfies $\pred$, that is, for any reduction sequence
    $\mapempty; \mkexpr\expr\emptyview \threadPoolReduces^*
    \store'; \mkexpr{\val'}{\view'}, \pool'$
    where $\val'$ is a value, $\pred \mkexpr{\val'}{\view'}$ holds.
\end{itemize}
\end{comment}

%%  LocalWords:  logics parameterized monoid Hoare invariants nonatomic CAS
%%  LocalWords:  instantiation monoids pointwise duplicable modalities
%%  LocalWords:  postcondition
