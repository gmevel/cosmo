% ------------------------------------------------------------------------------

% General presentation of locks; specification of locks in CSL.

\subsection{Specification of Locks}
\label{sec:lock:spec}

The lock is perhaps the most basic and best known concurrent data structure.
It supports three operations, namely \make, \acquire, and \release.
% \make allocates a fresh lock; \acquire locks a lock; \release unlocks it.
%
One way of describing its purpose is to say that it allows achieving
\emph{mutual exclusion} between several threads: that is, the \acquire and
\release operations delimit \emph{critical sections} in the code, and the
purpose of the lock is to guarantee that no two threads can be in a critical
section ``at the same time''. However, this is not a very good description,
especially in a weak memory setting, where the intuitive notions of ``time''
and ``simultaneity'' do not make much sense. What matters, really, is that a
lock mediates access to a shared resource, and does so in a correct manner. A
thread that successfully acquires the lock expects the resource to be in a
consistent state, and expects to be allowed to affect this state in arbitrary
ways, as long as it brings the resource back to a consistent state by the time
it releases the lock.

\input{figure-lock-spec}
\input{figure-spinlock}

This idea is expressed in \CSL in a simple and elegant
manner~\cite{ohearn-07}. The classic specification of dynamically-allocated
locks in \CSL
\cite{gotsman-aplas-07,hobor-oracle-08,buisse-11,sieczkowski-15,iris-lecture-notes},
%
a~version of which appears in \fref{fig:lock:spec},
%
allows the user to choose an \emph{invariant}~$\prop$ when a lock is
allocated. This invariant is a \SL assertion. It appears in the postcondition
of $\acquire$ and in the precondition of $\release$, which means that a thread
that acquires the lock gets access to the invariant, and can subsequently
break this invariant if desired, while a thread that releases the lock must
restore and relinquish the invariant.

In \fref{fig:lock:spec}, the entire specification is contained in a triple for
\make, whose precondition requires the invariant to hold initially, and whose
postcondition contains triples for \acquire and \release. Because a triple is
persistent \cite{iris}, therefore duplicable, several threads can share the
use of the newly-created lock.

In addition to the invariant~$\prop$, an abstract 0-ary predicate ``$\locked$''
appears in the postcondition of $\acquire$ and in the precondition of
$\release$. Because it is abstract, it must be regarded as nonduplicable by the user.
Therefore, it can be thought of as a
``token'', a witness that the lock is currently held. This witness is required
and consumed by \release.
%
There are two reasons why it is desirable to let such a token appear in the
specification. First, from a user's point of view, this is a useful feature,
as it forbids releasing a lock that one does not hold, an error that could
arise in the (somewhat unlikely) situation where several copies of the
invariant~$\prop$ can coexist. Second, from an implementor's point of view,
this makes the specification easier to satisfy. In a ticket lock
implementation (\sref{sec:examples:ticketlock}), for instance, only the thread
that holds the lock can safely release~it. Thus, a ticket lock does not
satisfy a stronger specification of locks where the ``\locked'' token is omitted.
%
% It is clear that the spec without \locked is stronger:
% indeed, if it is satisfied, then the spec in \fref{fig:lock:spec}
% is satisfied as well: just take \locked to be $\TRUE$.
%

If one can prove that an implementation of locks satisfies this specification,
then (because \SL is compositional) a user can safely rely on this
specification to \emph{reason} about her use of locks in an application
program. This is a necessary and sufficient condition for an implementation of
locks to be considered correct. Proving something along the lines of ``no two
threads can be in a critical section at the same time'' is not necessary, nor
would it be sufficient.
% It would not provide a means of reasoning about user programs.
% And, in a weak memory setting, a lock might actually guarantee
% mutual exclusion and nevertheless be broken.

We emphasize that, when this specification is understood in the setting of
\hlog, the user invariant~$\prop$ is an arbitrary \hlog assertion, thus
possibly a subjective assertion: no restriction bears on $\prop$. Indeed, the
synchronization performed by the lock at runtime ensures that every thread has
a consistent view of the shared resource. This is in contrast with Iris
invariants, which involve no runtime synchronization, and therefore must be
restricted to objective assertions (\sref{sec:logic:hi:assertions}).

We must also emphasize that, because we work in a logic of partial
correctness, this specification does not guarantee deadlock freedom, nor does
it guarantee any form of fairness. As an extreme example, an implementation
where \acquire diverges satisfies the specification in \fref{fig:lock:spec};
to see this, let ``$\locked$'' be $\FALSE$.
%
% Also, even the lock is perfectly correct, nothing prevents the user
% from attempting to acquire a lock twice.
%

% ------------------------------------------------------------------------------

% The spin lock.

\subsection{A Spin Lock}
\label{sec:examples:spinlock}

A spin lock is a simple implementation of a lock. It relies on a single atomic
memory location~$\lock$, which holds a Boolean value. Its implementation in
\mocaml appears in \fref{fig:spinlock}. The implementation of \acquire
involves busy-waiting in a loop, whence the name ``spin lock''.

An Iris proof of correctness of a spin lock \cite[Example
  7.36]{iris-lecture-notes}, in a traditional sequentially consistent setting,
would rely on the following Iris invariant, which states that either the lock
is currently available and the user assertion~$\prop$ holds, or the lock is
currently held:
%
\[
  \fname{isSpinLockSC}
  \eqdef
  \knowInv{}{
    (\pointsto\lock\False \isep \prop)
    \;\lor\;
    (\pointsto\lock\True)
  }
\]
%
In our setting, however, such an invariant does not make sense, and cannot
even be expressed, because the assertion~$\prop$ is an arbitrary (possibly
subjective) assertion of type $\typeVProp$, whereas \hlog requires every Iris
invariant $\knowInv{}{I}$ to be formed with an \emph{objective} assertion~$I$
of type $\typeIProp$ (\sref{sec:logic:hi:assertions}).

We work around this restriction by reformulating this invariant under an
objective form: ``either the lock is currently available and the user
assertion~$\prop$ holds \emph{in the eyes of the thread that last released the
  lock}, or the lock is currently held''. \hlog allows expressing this easily:
%
\[
  \isSpinLock
  \eqdef
  \knowInv{}{%
    (\Exists \view. \pointstoAT\lock{\mkval\False\view} \isep \prop\opat\view)
    \;\lor\;
    (\pointstoAT\lock\True)
  }
\]
%

The left-hand disjunct, which describes the situation where the lock is
available, now involves an existential quantification over a view~$\view$. The
atomic points-to assertion $\pointstoAT\lock{\mkval\False\view}$ indicates
that $\view$ is the view currently stored at location~$\lock$. The objective
assertion $\prop\opat\view$ indicates that $\prop$~holds at this view. That
is, the assertion~$\prop$ holds in the eyes of whomever last wrote the
location~$\lock$. (We use the present-tense ``holds'', as opposed to the
past-tense ``held'', because every \hlog assertion is monotonic in its
implicit view parameter.) In other words, $\prop$ holds in the eyes of the
thread that last released the lock.

The right-hand disjunct, which describes the case where the lock is held,
uses the simplified points-to assertion $\pointstoAT\lock\True$, which is
sugar for $\pointstoAT\lock{\mkval\True\emptyview}$
(\sref{sec:logic:hi:axioms}). In this case, the view stored at
location~$\lock$ is irrelevant.

In a spin lock, a ``\locked'' token is of no use, from the implementor's point of
view, so we let~``\locked'' be $\TRUE$. The proofs of the triples for \acquire
and \release are sketched in the following.

The proof of \acquire hinges mainly on establishing the following triple for
the CAS instruction:
% glen: Löb induction is then required to justify the loop in \acquire.
\[
  \hoare
    {\isSpinLock}
    {\CAS\lock\False\True}
    %{\Lam b. \text{if $b$ then $\prop$ else $\TRUE$}}
    {\Lam b. b = \True \implies \prop}
\]
This triple guarantees that
% the CAS instruction is safe, and
if CAS succeeds then one can extract the assertion~$\prop$.
%
To establish it, we must open the invariant $\isSpinLock$
for the duration of the CAS instruction \cite[\S2.2]{iris}
%
% (\hlog, like Iris, allows opening an invariant for the duration of an
%  atomic instruction.)
%
and reason separately about the case where the lock is available and the case
where it is held. In the latter case, we apply the reasoning rule \casfailure
(\fref{fig:hlog:rules}) and conclude easily. In the former case, we have
$\pointstoAT\lock{\mkval\False\view}$ and $\prop\opat\view$, for an unknown
view~$\view$. The rule \cassuccess (instantiated with the empty view
for $\view'$) shows that the outcome of the CAS instruction in this case is
$\pointstoAT\lock\True \isep \seen\view$.
%
In other words, the CAS instruction achieves the double effect of writing
$\True$ to the location~$\lock$ and acquiring the view~$\view$ that was stored
at this location by the last write.
%
This is exactly what we need.
%
Indeed, the axiom \splitso lets us combine $\prop\opat\view$ and $\seen\view$
to obtain $\prop$. By performing an ``acquire'' read, we have ensured that
$\prop$ holds in the eyes of the thread that has just acquired the lock.
%
Furthermore, the points-to assertion $\pointstoAT\lock\True$ allows us to
establish the right-hand disjunct of the invariant~$\isSpinLock$ and to
close it again.

The proof of \release exploits \splitso in the reverse direction. Per the
precondition of \release, we have~$\prop$. We split it into two assertions
$\prop\opat\view$ and $\seen \view$, where $\view$ is technically a fresh
unknown view, but can be thought of as this thread's view. Then, we open the
invariant $\isSpinLock$ for the duration of the write instruction. We do not
know which of the two disjuncts is currently satisfied (indeed, we have no
guarantee that the lock is currently held),
%
% glen: The value of \lock before releasing is unknown.
% In other words, the spin lock does not forbid spurious releases,
% as long as the thread that invokes release is able to provide P.
%
% If we wanted to have a guarantee that the lock is currently held,
% we could define \locked to be a ghost token.
%
but
% and by throwing away some information
we find that, in either case, we have $\pointstoAT\lock\_$. This allows us to
apply the reasoning rule \atwrite (\fref{fig:hlog:rules}), which guarantees
that, after the write instruction, we have
$\pointstoAT\lock{\mkval{\False}{\view}}$. In other words, because we have
performed a ``release'' write, we know that, after this write, our view of
memory is stored at location~$\lock$.
Because we have $\pointstoAT\lock{\mkval{\False}{\view}}$
and $\prop\opat\view$, we are able to prove that the
left-hand disjunct of~$\isSpinLock$ holds and to close the invariant.

%%  LocalWords:  postcondition duplicable ary affine nonduplicable runtime
%%  LocalWords:  implementor's compositional invariants spinlock isSpinLockSC
%%  LocalWords:  disjunct CAS disjuncts
