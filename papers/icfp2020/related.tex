\section{Related Work}

There is a wide variety of work on weak memory models and on
approaches to program verification with respect to weak memory models.
We restrict our attention to program verification based on extensions
of \CSL, because this is the most closely related work and because we
believe that the abstraction and compositionality of \SL are features
that will be absolutely essential in the long run.
%
% Indeed, the complexities of fine-grained concurrent data structures
% and weak memory must be hidden from the end user.
%

% All of the papers that we cite have been verified in Coq...

% ------------------------------------------------------------------------------

% RSL (OOPSLA 2013).

The first instance of \SL for weak memory appears to be RSL
\cite{vafeiadis-narayan-13}.
%
It is based on an axiomatic semantics of a fragment of the C11 memory model.
%
It supports nonatomic accesses, where it enforces the absence of data races;
release/acquire accesses, with reasoning rules that allow ownership transfers
from writer to reader; and relaxed accesses, without any ownership transfer.
%
The logic involves a permission $\mathit{Rel}(\ell, Q)$ to perform a release write
at location $\ell$ of a value $v$ while relinquishing the assertion $Q\;v$.
Symmetrically, there is a permission $\mathit{Acq}(\ell, Q)$ to perform an
acquire read at location $\ell$ of a value~$v$ and obtain the assertion~$Q\;v$.
The release and acquire permissions are created, and the predicate $Q$ is fixed,
when the location is allocated. % (page 7)
%
RSL~can verify simple concurrent data structures, such as a spin lock.
However, because it lacks invariants and ghost state, its expressive
power is limited.
% fp: Faut-il une comparaison spécifique avec \hlog?
% jh: on dit déjà quelque chose (pas d'invariants/ghost state), et en
% rajouter prendrait de la place et risquerait d'introduire des
% comparaisons discutables.

% The abstract says that the paper supports SC atomics with arbitrary
% ownership transfer, but this is actually incorrect; SC atomics are not
% supported at all. This is noted in the conclusion of the RustBelt Relaxed
% paper.

% Rel(l, Q) is a permission to do a release write at location l of
%   any value v that satisfies Q v.
% This permission seems duplicable. (Rel-Split.)

% Acq(l, Q) is a permission to do an acquire read at location l
%   and find a value v that satisfies Q v.
% Not duplicable, but splittable by splitting Q.
%
% Somewhat heavy and ad hoc rules for CAS.

% Based on an axiomatic semantics of C11. Formalized in Coq.

%  According to the GPS paper,
%  ``RSL supports simple, high-level reasoning about resource invariants
%    and ownership transfer [...] but provides no support for ghost state
%    or more complex forms of protocols and ownership transfer''.

% The spin lock seems to work only because it involves release/acquire
% writes at a single location, so one can express the invariant in the
% formula Q carried by the Rel and Acq permissions.

% ------------------------------------------------------------------------------

% FSL and FSL++. (Cited here because they descend from RSL.)

FSL \cite{doko-vafeiadis-16} extends RSL with support for release/acquire
fences. A release write can be replaced with a release fence followed with a
relaxed write; symmetrically, an acquire read can be replaced with a relaxed
read followed with an acquire fence. Two new assertions $\Delta P$ and $\nabla
P$ witness that $P$ has been released by the last release fence or will be
acquired by the next acquire fence, respectively.
%
% Comparaison spécifique avec nous:
In \hlog, both of these assertions would be replaced by $\prop\opat\view$, for
a well-chosen view~$\view$. \mocaml has no fences; instead, atomic read and
write instructions are used to transmit views.
%
FSL++ \cite{doko-vafeiadis-17} extends FSL with shared read permissions for
nonatomic accesses, support for CAS, and ghost state.

% FSL (VMCAI 2016).
%   Descends from RSL.
%   Nonatomics + Atomics (release/acquire, relaxed).
%   Rel(l, Q) is a permission to do a release write at location l of
%     any value v that satisfies Q v.
%   This permission also allows performing a relaxed write,
%   but in that case, it must be helped by a release fence.
%   The postcondition $\Delta P$ of a release fence
%   is a witness that $P$ has been made ready for transfer
%   (therefore cannot be used any more by this thread).
%   Symmetrically, Acq(l, Q) allows performing an acquire read.
%   A relaxed read can be used with the help of an acquire fence.
%   The assertion $\Nabla P$ means that after the next acquire fence
%   we will have $P$.

% FSL++
%   ``Even though FSL supports relaxed accesses and memory fences, it lacks
%     some key features which makes it inapplicable beyond simple toy examples.''
%   FSL++ extends FSL with fractional read permissions for nonatomic accesses,
%   support for CAS, and ghost state.
%   As an application, they prove the Rust library ARC (atomic reference counter).
%   FSL++ is about 22Kloc of Coq.

% ------------------------------------------------------------------------------

% GPS (2014).

GPS \cite{turon-vafeiadis-dreyer-14} supports a fragment of C11 that includes
nonatomic accesses and release/acquire accesses. Like the papers cited above,
it is based on an axiomatic presentation of the C11 memory model.
%
It introduces ghost state and a notion of per-location protocol that governs
a single atomic memory location.
%
At the cost of rather complex reasoning rules, it offers good expressive
power. The case studies described in the paper include a spin lock, a bounded
ticket lock, a Michael-Scott queue, and a circular FIFO queue.
%
In comparison, \hlog does not need per-location protocols.
% for atomic memory locations.
Because atomic memory locations in \mocaml have sequentially
consistent behavior, our ``atomic points-to'' predicate is objective.
Therefore, in \hlog, an invariant can refer to one or more atomic memory
locations if desired. This has been illustrated in \sref{sec:examples:ticketlock}
and \sref{sec:examples:peterson}.
%
% fp: Il serait intéressant de regarder comment le ticket lock est prouvé
% en GPS ou iGPS.
%
% jh: leur preuve est beaucoup plus complexe (800 LOC). On dit que
% notre approche est plus simple, est-ce que ça vaut le coup
% d'enfoncer le clou encore plus en donnant des chiffres?

% For C11. Supports nonatomics and release/acquire atomics. No relaxed accesses.
% Roughly analogous to our setting, insofar as races on nonatomics
% are forbidden and synchronization must go through atomic locations.
% However, their atomics are weaker than ours, I think.
% Apparently based on an axiomatic semantics of C11. Formalized in Coq.
% Ghost state.
% Per-location protocols (for atomic locations).
% The client can assert a lower bound on the protocol state
%   (e.g., ``this location has been observed in state s'').
% Complex rules.
% They prove the spin lock example using a single-location invariant,
% that is, a single-location protocol with only one state.
% Escrow: a duplicable permission to abandon P and get Q
%   (both of which are exclusive).
% Case studies: Michael-Scott queue, circular buffers, bounded ticket lock.

% \citet{he-18}.
% From the FSL++ paper:
% He et al. [14] have proposed an extension of GPS with FSL-style modalities,
% to give it support for relaxed accesses and memory fences. As the original
% FSL, this extension of GPS does not have support for atomic updates, which
% makes it inapplicable to programs like ARC. Additionally, unlike FSL, this
% extension of GPS lacks a soundness proof.

\citet{vafeiadis-cav-17} offers a good survey of the papers cited thus far.
% Invited talk at CAV.

% ------------------------------------------------------------------------------

\citet{sieczkowski-15}, already cited earlier (\sref{sec:intro}), present a
variant of \CSL that is sound with respect to the TSO memory model. The logic
includes a high-level fragment whose reasoning rules are those of \CSL. The
logic is proved sound with respect to an operational semantics where store
buffers are explicit. This work and ours seem quite close in spirit, but
differ due to the choice of a different memory model. In particular,
Sieczkowski et al.\ have no notion of view. In order to reason about the
behavior of store buffers, they propose a few ad hoc logical constructs, such
as an ``until'' modality $P \mathrel{\mathcal{U}_t} Q$, which means that $P$
holds until an update from thread $t$ is flushed to main memory, at which
point $Q$ holds.

% iCAP-TSO.
%
% In their ``SC'' logic, they use standard \SL triples, where the pre- and
% postcondition are interpreted relative to the current thread's
% point-of-view. Informally, an assertion holds from the point of view of a
% thread if it holds of the global state updated with the thread's store
% buffer and no other thread has pending writes that could affect this
% assertion. Thus, the subjective points-to assertion $x \mapsto v$ guarantees
% that a read instruction will return the value~$v$.
%
% The ``SC'' logic cannot reason about transfers of ownership. This kind of
% reasoning must be carried out in a lower-level ``TSO'' logic. It is possible
% to verify a data structure (e.g., a lock) in the logic ``TSO'' and to
% establish a spec expressed in ``SC'', which can be used by ``SC'' clients.
%
% The spec of locks is restricted to "stable" invariants.
%
% The verification of the spin lock (which uses a single release write to
% release the lock) relies on the idea that ``by the time the buffered release
% write makes it to main memory, [the resource invariant] holds objectively''.
% More precisely, in the unlocked state, the lock invariant states that
% either locked is false in memory and R holds
% or there is a release write on the way and by the time it reaches
%   main memory, R holds.
%
% Built on top of iCAP.
%
% (Section 4) An assertion P in the logic SC can be embedded as
% $\lceil r P \kw{in} t\rceil$ in the logic TSO; this means $P$ holds in the
% eyes of the thread $t$, as described above. There is another embedding
% $\lceil P\rceil$ which means that $P$ holds in the eyes of every thread.
%
% The subjective embedding $\lceil r P \kw{in} t\rceil$ is used to give
% meaning to SC triples on top of TSO triples.
%
% The logic TSO also has an "until" operator, $P \mathcal{U}_t Q$, which
% means $P$ holds until (at a certain time) an update from thread $t$ is
% flushed to main memory, at which point $Q$ holds. This assertion is
% *unstable*, as the write could be flushed at any time. There is an
% explicit stabilization operator (page 15) that turns any assertion P
% into its stabilized version.
%
% Assertions are relative to a thread ID.
% The global state of the TSO logic includes the pool of store buffers.

% ------------------------------------------------------------------------------

% iGPS and iRSL.

\citet{kaiser-17} propose the first instantiation of Iris in a weak-memory
setting. This involves abandoning the axiomatic memory models used by the
papers cited above and switching to an operational semantics. The
paper proposes such a semantics for a fragment of C11 that includes two kinds
of memory locations, namely nonatomic locations and release/acquire locations.
Like Dolan et al.'s semantics of \mocaml (\citeyear{dolan-18}), this semantics
involves time stamps and histories. It also includes a ``race detector'' which
ensures that data races on nonatomic memory locations lead to undefined
behavior. In comparison, Dolan et al.'s semantics does not need a race
detector: every \mocaml program has a well-defined set of permitted behaviors.
%
Several aspects of our work are modeled after Kaiser et al.'s paper. Indeed,
in a first step, they instantiate Iris, yielding a low-level ``base logic''.
Then, in a second step, they define a higher-level logic, whose
reasoning rules are easier to use, and whose assertions are implicitly
parameterized with the thread's view. We follow this approach.
%
Kaiser et~al.\ construct not one, but two high-level logics, iGPS and iRSL,
which are inspired by GPS and RSL, and benefit from the power of Iris.
%
iGPS introduces a new feature, namely single-writer protocols.
%
\hlog follows a different route: as explained above, it does not need
per-location protocols. Furthermore, it puts emphasis on explicit user-level
reasoning with abstract views, via assertions like $\prop\opat\view$ and
$\seen\view$.
%
% iRC11 also involves reasoning about views, but only in some of the
% intermediate-level layers.

% Reconstruct GPS and RSL on top of Iris.
%
% This time, based on an operational semantics of the nonatomic+RA
% fragment of C11. The semantics includes a ``race detector'' so
% that data races on nonatomics are considered unsafe.
% (In contrast, we do not need a race detector.)
% They also have a two-level construction: base logic, iGPS or iRSL.
% In the high-level logic, assertions are also parameterized with a view.
%
% At the low level, the predicate Hist(l, h) is very much like \llog's
% nonatomic points-to predicate.
%   But the history h records only the writes of the current era.
% Seen(pi, V) is different from what we have, as it mentions a thread id.
%
% iGPS has a new feature: single-writer protocols.
%
% New in \hlog, explicit reasoning about (abstract) views.

% ------------------------------------------------------------------------------

iRC11 \cite{dang-20} extends iGPS with additional features of the C11 memory
model, namely relaxed accesses and release/acquire fences. It is based on
ORC11, an operational presentation of the Repaired C11 memory model
\cite{rc11}. One of its key features is support for cancellable invariants, an
abstraction whose implementation in Iris in an SC setting has been
well-understood for some time~\footnote{%
For example, in RustBelt~\cite{rustbelt-18},
nonatomic persistent borrows are a form of cancellable invariant.},
but whose implementation in a weak-memory
setting is significantly more challenging. In particular, the tokens that
represent a fraction of the ownership of an invariant are not objective, and
therefore cannot appear in an invariant; they must be transmitted from one
thread to another via a synchronization operation. Dang et al.'s
implementation of cancellable invariants involves explicit reasoning about
views, yet this is not apparent in the cancellable invariant API,
a~remarkable achievement.
% A user of iRC11 need not know about views.

% The rules of ``raw cancellable invariants'' involve views
% and a ``view-join modality''.

% fp: Should we say that we do not anticipate a great need for
% cancellable invariants in our setting because \mocaml is equipped with a
% garbage collector?
% jh: ce n'est pas clair. Il y a des ressources qui ne sont pas gérées
% par le garbage collector. Et puis on peut avoir besoin de prêter des
% ressources en les réutilisant vraiment après (i.e., c'est un peu
% comme les ressources read-only de votre papier).

% fp: Should we say that (if desired) we can use standard Iris-SC
% cancellable invariants, with an objective assertion inside?
% jh: non : l'intérêt des invariants cancellables dont on parle ici,
% c'est de synchroniser au moment où on annule l'invariant.

On top of iRC11, Dang et al.\ reconstruct Lifetime Logic and the model of the
Rust type system previously built by Jung et al.\ in an SC setting
(\citeyear{rustbelt-18}). Furthermore, they prove the soundness of Rust's
atomic reference counter library (ARC).

% The rules for fences use \Delta and \nabla as in FSL.

% It has single-location invariants. (Single-location protocols are not
% described, for simplicity.)

% This requires adapting cancellable invariants to the weak-memory setting.
%   (No previous work did so.)
% One key difficulty is that the tokens that represent (fractional) ownership
% of a cancellable invariant cannot be ghost state, otherwise they would be
% objective, therefore freely transferrable via invariants, which would be
% unsound. Instead, the authors propose ``synchronized ghost state'', that
% is, ghost state tokens implicitly keep track of the view of the thread
% that owns the token. Runtime synchronization is required to transfer such
% a token from one thread to another.
%
% Dang et al. reconstruct Lifetime Logic, and the model of the Rust type
% system, on top of iRC11. They prove several Rust libraries correct.

% ------------------------------------------------------------------------------

% Not cited:

% https://ilyasergey.net/other/CSL-Family-Tree.pdf

% Tom Ridge. A rely-guarantee proof system for x86-TSO. VSTTE 2010.
% Not Separation Logic.

% Lahav & Vafeiadis. Owicki-Gries reasoning for weak memory models. ICALP 2015.
% OGRA
% Handles only the release-acquire fragment. Not Separation Logic.

% \citet{raad-19}. (Not Separation Logic.)

% Summers-Muller:
% http://pm.inf.ethz.ch/publications/getpdf.php?bibname=Own&id=SummersMueller18.pdf
% -- want to automate the use of several relaxed logics

% Verification under causally consistent shared memory
% https://www.cs.tau.ac.il/~orilahav/papers/siglog19.pdf
% (Short survey.)

%%  LocalWords:  compositionality RSL nonatomic Acq invariants FSL CAS GPS et
%%  LocalWords:  vafeiadis cav sieczkowski TSO al hoc modality instantiation
%%  LocalWords:  Dolan dolan parameterized logics iGPS iRSL iRC ORC rustbelt
%%  LocalWords:  cancellable API
