% References to sections, lemmas, theorems, etc.
\newcommand{\sref}[1]{\S\ref{#1}}
\newcommand{\fref}[1]{Figure~\ref{#1}}

% Abbreviations.
\def\etal.{\emph{et al.}}
\newcommand{\SL}{Separation Logic\xspace}
\newcommand{\CSL}{Concurrent \SL}
\newcommand{\mocaml}{Multicore OCaml\xspace}
% The base logic and the derived logic.
\newcommand{\llog}{BaseCosmo\xspace}
\newcommand{\hlog}{Cosmo\xspace}
% Lambda-calculus.
\newcommand{\lc}{$\lambda$-calculus\xspace}

% A macro for TEMPORARY remarks.
\colorlet{fp}{green!10!red!90!}
% \newcommand{\fp}[1]{\textcolor{fp}{---fp: #1}}
\colorlet{glen}{orange}
% \newcommand{\glen}[1]{\textcolor{glen}{---glen: #1}}

% ------------------------------------------------------------------------------
% Notations of the programming language.

% λ-calculus:
\newcommand{\Rec}[3]{\mu#1.\Lam#2.#3}
\newcommand{\NonRec}[2]{\Lam#1.#2}
\newcommand{\Let}[3]{\texttt{let}\;#1=#2\;\texttt{in}\;#3}
% Application will be simply denoted by a space (~), in curried style.

% unit value:
\newcommand{\Unit}{\texttt{()}}

% booleans:
\newcommand{\False}{\texttt{false}}
\newcommand{\True}{\texttt{true}}
\newcommand{\POr}{\;\mid\mid\;}
\newcommand{\PAnd}{\;\&\&\;}
\newcommand{\IfThenElse}[3]{\texttt{if}~#1~\texttt{then}~#2~\texttt{else}~#3}
\newcommand{\IfThen}[2]{\texttt{if}~#1~\texttt{then}~#2}

% pairs:
\newcommand{\Pair}[2]{(#1,#2)}
\newcommand{\Tuple}[1]{(#1)}

% concurrency:
\newcommand{\Fork}[1]{\texttt{fork}\;#1}

% memory:
\makeatletter
  \newcommand{\ReadNA@op}{\TextOrMath{\texttt{!\NA}}{\mathop{!\NA}}}
  \newcommand{\ReadAT@op}{\TextOrMath{\texttt{!\AT}}{\mathop{!\AT}}}
  \newcommand{\WriteNA@op}{\TextOrMath{\texttt{:=\NA}}{\mathrel{\coloneqq\NA}}}
  \newcommand{\WriteAT@op}{\TextOrMath{\texttt{:=\AT}}{\mathrel{\coloneqq\AT}}}
  \newcommand{\ReadNA}[1]{\ReadNA@op #1}
  \newcommand{\ReadAT}[1]{\ReadAT@op #1}
  \newcommand{\WriteNA}[2]{#1 \WriteNA@op #2}
  \newcommand{\WriteAT}[2]{#1 \WriteAT@op #2}
\makeatother
\newcommand{\CAS}[3]{\texttt{CAS}~#1~#2~#3}
\newcommand{\AllocNA}[1]{\texttt{new\NA}\;#1}
\newcommand{\AllocAT}[1]{\texttt{new\AT}\;#1}

% generic symbol for an operator:
\newcommand{\AnyOp}{\odot}

% Identifiers in the programming language:
\newcommand{\pname}[1]{\texttt{{#1}}\xspace}

% ------------------------------------------------------------------------------
% Notations of the logic.

% separating conjunction (not defined by iris.sty):
\newcommand{\isep}{\,\mathrel{*}\,}
% large separating conjunction:
\newcommand{\ISEP}{\scalebox{1.7}{\raisebox{-0.3ex}{$\ast$}}\!}
% large separating conjunction in an array:
%   (left brace on the left side)
%   (using \cr inside to separate lines)
\newcommand{\ISep}[1]{%
  \ISEP
  \left\{\begin{array}{@{\,}l}
  #1
  \end{array}\right.
}

% logical equivalence (as double implication):
%\newcommand{\iequiv}{\equiv}
\newcommand{\iequiv}{\provesIff}

% the “valid view” predicate:
\newcommand{\valid}[1]{\mathit{valid}\,#1}

% the “seen” predicate:
\newcommand{\seen}{\uparrow\!}
%\newcommand{\seen}{\mathop{\text{\eye}}}   % requires package <dingbat>
%  \let\checkmark\undefined
%  \usepackage{dingbat}
%\newcommand{\seen}{\mathop{\text{\faEye}}} % requires package <fontawesome>
%  \usepackage{fontawesome}

% the “P at V” operator:
%\newcommand{\opat}{\mathbin{\mathsf{at}}}
\newcommand{\opat}{@}

% lifting a formula from iProp to vProp:
\newcommand{\liftobj}[1]{\lceil#1\rceil}

% subscripts for access modes:
\newcommand{\sub}[1]{\TextOrMath{\textsubscript{#1}}{_\text{\normalfont {#1}}}}
\newcommand{\NA}{\sub{na}}
\newcommand{\AT}{\sub{at}}
\newcommand{\super}[1]{\TextOrMath{\textsuperscript{#1}}{^\text{\normalfont {#1}}}}
\newcommand{\HIST}{\super{hist}}

% Displaying ``fraction \cdot'' if fraction is not empty,
% displaying nothing otherwise.
\newcommand{\showfraction}[1]{%
  \ifthenelse{\equal{#1}{}}{}{#1\cdot}%
}

% Traditional \pointsto:
\NewDocumentCommand\pointsto{O{}mm}{%
  \showfraction{#1}{#2}\mathbin{\leadsto} {#3}%
}

% ownership of locations (with an optional fraction):
% nonatomic (base logic):
\NewDocumentCommand\histNA{O{}mm}{%
  \showfraction{#1}{#2} \mathbin{\leadsto\NA} {#3}%
}
% nonatomic (lifted logic):
\NewDocumentCommand\pointstoNA{O{}mm}{%
  \showfraction{#1}{#2} \mathbin{\leadsto\NA} {#3}%
}
% atomic:
\NewDocumentCommand\pointstoAT{O{}mm}{%
  \showfraction{#1}{#2}\mathbin{\leadsto\AT} {#3}%
}

% name of a function/predicate of the metalanguage:
\newcommand{\fname}[1]{\ensuremath{\mathsf{{#1}}}\xspace}

% canonical variable names:
%\newcommand{\expr}{\ensuremath{e}}  % already defined in iris.sty
%\newcommand{\val}{\ensuremath{v}}  % already defined in iris.sty
\newcommand{\naloc}{\ensuremath{a}}
\newcommand{\atloc}{\ensuremath{A}}
\newcommand{\view}{\ensuremath{\mathcal{V}}}
\newcommand{\altview}{\ensuremath{\mathcal{W}}} % an auxiliary view variable, used in some definitions
\newcommand{\viewatloc}{\view} % view stored in the atomic location \atloc
\newcommand{\thisview}{\ensuremath{\mathcal{W}}} % the ambient view in vProp
\newcommand{\globalview}{\ensuremath{\mathcal{G}}} % the reference view that appears in the state interpretation predicate
\newcommand{\hist}{\ensuremath{h}}
\newcommand{\store}{\ensuremath{\sigma}}
\newcommand{\timest}{\ensuremath{t}}
\newcommand{\gnameglobalview}{\ensuremath{\gname\sub{gv}}}
\newcommand{\pool}{\ensuremath{p}}

% make \pred (defined in iris.sty) use \Phi instead of \varPhi:
\renewcommand{\pred}{\Phi}

% types:
\newcommand{\typename}[1]{\text{\textsc{{#1}}}}
\newcommand{\typeNALoc}{\typename{Loc\NA}}
\newcommand{\typeATLoc}{\typename{Loc\AT}}
\newcommand{\typeTime}{\typename{Time}}
\newcommand{\typeExpr}{\typename{Expr}}
\newcommand{\typeVal}{\typename{Val}}
\newcommand{\typeView}{\typename{View}}
\newcommand{\typeHist}{\typename{Hist}}
\newcommand{\typeStore}{\typename{Store}}
\newcommand{\typeIProp}{\typename{iProp}}
\newcommand{\typeVProp}{\typename{vProp}}

% arrow for monotonic functions:
\newcommand{\monarrow}{%
  \stackrel
    {\smash{\raisebox{-0.5mm}{\textsf{\tiny mon}}}}
    \longrightarrow
}

% arrow for finite map types:
\newcommand{\maparrow}{%
  \stackrel
    {\smash{\raisebox{-0.5mm}{\textsf{\tiny fin}}}}
    \longrightarrow
}

% arrow for almost-everywhere-zero:
\newcommand{\aezarrow}{%
  \stackrel
    {\smash{\raisebox{-0.5mm}{\textsf{\tiny aez}}}}
    \longrightarrow
}

% map things:
\newcommand{\mapbinding}[2]{{#1} \mapsto {#2}}
\newcommand{\mapliteral}[1]{\left\{{#1}\right\}}
\newcommand{\mapempty}{\varnothing}
\newcommand{\maplookup}[2]{#1(#2)}
\newcommand{\mapupdate}[3]{{#1}[\mapbinding{#2}{#3}]}

% any mathematical pair:
\newcommand{\pair}[2]{\left(#1,#2\right)}

% expression-view pairs and value-view pairs:
\newcommand{\mkexpr}[2]{\left\langle #1,#2\right\rangle}
\newcommand{\mkval}[2]{\left\langle #1,#2\right\rangle}

% weakest precondition:
\newcommand{\WPname}{\mathsf{wp}}
\newcommand{\WP}[2]{\WPname~{#1}~\left\{{#2}\right\}}

% Hoare triples presented like inference rules,
% with two horizontal bars:
\newcommand{\HoareRule}[4]{%
  \tripleinfer[#1]{#2}{#3}{#4}%
}

% Store access (function application):
\newcommand{\storeapp}[2]{\maplookup{#1}{#2}}
% History access (function application):
\newcommand{\histapp}[2]{\maplookup{#1}{#2}}
% View access (function application):
\newcommand{\viewapp}[2]{\maplookup{#1}{#2}}
% domain of a map or function:
\newcommand{\Dom}[1]{\mathrm{dom}\;#1}

% initial time stamp:
\newcommand{\timeMin}{0}
% order between time stamps:
\newcommand{\timelt}{<}
\newcommand{\timeleq}{\leq}

% the structure of views:
\newcommand{\viewleq}{\sqsubseteq}
\newcommand{\viewgeq}{\sqsupseteq}
\newcommand{\emptyview}{\bot}
\newcommand{\viewjoin}{\sqcup}

% Memory events.
\newcommand{\noMemEvent}{\ensuremath{\varepsilon}}
\newcommand{\someMemEvent}{\ensuremath{m}}
\newcommand{\memEvent}[1]{\text{\textsf{#1}}}
\newcommand{\memEventReadNA}[2]{\memEvent{rd}(#1,#2)}
\newcommand{\memEventReadAT}[2]{\memEvent{rd}(#1,#2)}
\newcommand{\memEventWriteNA}[2]{\memEvent{wr}(#1,#2)}
\newcommand{\memEventWriteAT}[2]{\memEvent{wr}(#1,#2)}
\newcommand{\memEventUpdate}[3]{\memEvent{rdwr}(#1,#2,#3)}
\newcommand{\memEventAllocNA}[2]{\memEvent{alloc}(#1,#2)}
\newcommand{\memEventAllocAT}[2]{\memEvent{alloc}(#1,#2)}

% Reduction relations.
\newcommand{\memReduces}[1]{\xrightarrow{#1}}
\newcommand{\exprReduces}[1]{\xrightarrow{#1}}
\newcommand{\threadPoolReduces}{\mathrel{\Longrightarrow}}

% Hyperlinks.
\newcommand{\RULE}[1]{\hyperlink{#1}{\textsc{#1}}\xspace}

% Names of transition rules for the memory subsystem.
\newcommand{\namememnaalloc}{mem-na-Alloc}
\newcommand{\namememnaread}{mem-na-Read}
\newcommand{\namememnawrite}{mem-na-Write}
\newcommand{\namemematalloc}{mem-at-Alloc}
\newcommand{\namemematread}{mem-at-Read}
\newcommand{\namemematwrite}{mem-at-Write}
\newcommand{\namemematreadwrite}{mem-at-Read-Write}

\newcommand{\memnaalloc}{\RULE\namememnaalloc}
\newcommand{\memnaread}{\RULE\namememnaread}
\newcommand{\memnawrite}{\RULE\namememnawrite}
\newcommand{\mematalloc}{\RULE\namemematalloc}
\newcommand{\mematread}{\RULE\namemematread}
\newcommand{\mematwrite}{\RULE\namemematwrite}
\newcommand{\mematreadwrite}{\RULE\namemematreadwrite}

% Names of rules in the base logic.
\newcommand{\namebasenaalloc}{base-na-Alloc}
\newcommand{\namebasenaread}{base-na-Read}
\newcommand{\namebasenawrite}{base-na-Write}
\newcommand{\namebaseatalloc}{base-at-Alloc}
\newcommand{\namebaseatread}{base-at-Read}
\newcommand{\namebaseatwrite}{base-at-Write}
\newcommand{\namebasecassuccess}{base-cas-Success}
\newcommand{\namebasecasfailure}{base-cas-Failure}

\newcommand{\basenaalloc}{\RULE{\namebasenaalloc}}
\newcommand{\basenaread}{\RULE{\namebasenaread}}
\newcommand{\basenawrite}{\RULE{\namebasenawrite}}
\newcommand{\baseatalloc}{\RULE{\namebaseatalloc}}
\newcommand{\baseatread}{\RULE{\namebaseatread}}
\newcommand{\baseatwrite}{\RULE{\namebaseatwrite}}
\newcommand{\basecassuccess}{\RULE{\namebasecassuccess}}
\newcommand{\basecasfailure}{\RULE{\namebasecasfailure}}

% Names of rules in \hlog.

\newcommand{\nameseenzero}{Seen-Zero}
\newcommand{\nameseentwo}{Seen-Two}
\newcommand{\namesplitso}{Split-Subjective-Objective}

\newcommand{\seenzero}{\RULE{\nameseenzero}}
\newcommand{\seentwo}{\RULE{\nameseentwo}}
\newcommand{\splitso}{\RULE{\namesplitso}}

\newcommand{\namenaalloc}{na-Alloc}
\newcommand{\namenaread}{na-Read}
\newcommand{\namenawrite}{na-Write}
\newcommand{\nameatalloc}{at-Alloc}
\newcommand{\nameatread}{at-Read}
\newcommand{\nameatwrite}{at-Write}
\newcommand{\namecassuccess}{cas-Success}
\newcommand{\namecasfailure}{cas-Failure}
\newcommand{\nameatallocsc}{at-Alloc-\SC}
\newcommand{\nameatreadsc}{at-Read-\SC}
\newcommand{\nameatwritesc}{at-Write-\SC}
\newcommand{\namecassuccesssc}{cas-Success-\SC}
\newcommand{\namecasfailuresc}{cas-Failure-\SC}

\newcommand{\naalloc}{\RULE{\namenaalloc}}
\newcommand{\naread}{\RULE{\namenaread}}
\newcommand{\nawrite}{\RULE{\namenawrite}}
\newcommand{\atalloc}{\RULE{\nameatalloc}}
\newcommand{\atread}{\RULE{\nameatread}}
\newcommand{\atwrite}{\RULE{\nameatwrite}}
\newcommand{\cassuccess}{\RULE{\namecassuccess}}
\newcommand{\casfailure}{\RULE{\namecasfailure}}
\newcommand{\atallocsc}{\RULE{\nameatallocsc}}
\newcommand{\atreadsc}{\RULE{\nameatreadsc}}
\newcommand{\atwritesc}{\RULE{\nameatwritesc}}
\newcommand{\cassuccesssc}{\RULE{\namecassuccesssc}}
\newcommand{\casfailuresc}{\RULE{\namecasfailuresc}}

% Suffix for the names of the derived rules.
\newcommand{\SC}{SC}

% ------------------------------------------------------------------------------
% Notations for locks.

% general lock notations:
\newcommand{\lock}{\ensuremath{\ell}}
\newcommand{\make}{\pname{make}}
\newcommand{\acquire}{\pname{acquire}}
\newcommand{\release}{\pname{release}}
\newcommand{\wait}{\pname{wait}}
\newcommand{\locked}{\fname{locked}}

% notations for spin locks:
\newcommand{\isSpinLock}{\fname{isSpinLock}}

%% \newcommand{\namespinlockmake}{spinlock-Make}
%% \newcommand{\namespinlockacquire}{spinlock-Acquire}
%% \newcommand{\namespinlockrelease}{spinlock-Release}
%% \newcommand{\namespinlocktryacquire}{spinlock-Try-Acquire}

%% \newcommand{\spinlockmake}{\RULE{\namespinlockmake}}
%% \newcommand{\spinlockacquire}{\RULE{\namespinlockacquire}}
%% \newcommand{\spinlockrelease}{\RULE{\namespinlockrelease}}
%% \newcommand{\spinlocktryacquire}{\RULE{\namespinlocktryacquire}}

% notations for ticket locks:
\newcommand{\ticketserv}{\pname{served}} % possible names: current, served, ready
\newcommand{\ticketnext}{\pname{next}}
\newcommand{\vticketserv}{\ensuremath{s}}
\newcommand{\vticketnext}{\ensuremath{n}}
\newcommand{\gticketserv}{\mathit{served}} % was: {\gname\sub{served}}
\newcommand{\gticketissued}{\mathit{issued}} % was: {\gname\sub{issued}}
\newcommand{\issued}[1]{\fname{ticket}\;#1}
\newcommand{\isTicketLock}{\fname{isTicketLock}}

% the monoid of disjoint sets:
\newcommand{\setdisjm}{\textmon{DisjointSet}}

%% \newcommand{\nameticketlockmake}{ticketlock-Make}
%% \newcommand{\nameticketlockacquire}{ticketlock-Acquire}
%% \newcommand{\nameticketlockrelease}{ticketlock-Release}
%% \newcommand{\nameticketlockwait}{ticketlock-Wait}

%% \newcommand{\ticketlockmake}{\RULE{\nameticketlockmake}}
%% \newcommand{\ticketlockacquire}{\RULE{\nameticketlockacquire}}
%% \newcommand{\ticketlockrelease}{\RULE{\nameticketlockrelease}}
%% \newcommand{\ticketlockwait}{\RULE{\nameticketlockwait}}

% ------------------------------------------------------------------------------
% Notations for Peterson’s algorithm.

\newcommand{\petersonturn}{\pname{turn}}
\newcommand{\petersonflag}{\pname{flag}}
\newcommand{\petersonmyflag}{\pname{myFlag}}
\newcommand{\petersonotherflag}{\pname{otherFlag}}
\newcommand{\petersontid}{\pname{myTid}}

\newcommand{\typePetersonState}{\typename{PetersonState}}
\newcommand{\petersonoutside}{\ensuremath{\mathrm{O}}}
\newcommand{\petersonstartacq}{\ensuremath{\mathrm{A}}}
\newcommand{\petersonwaiting}{\ensuremath{\mathrm{W}}}
\newcommand{\petersoncritical}{\ensuremath{\mathrm{C}}}

\newcommand{\vpetersonturn}{\ensuremath{t}}
\newcommand{\vpetersonflag}{\ensuremath{f}}
\newcommand{\vpetersonstate}{\ensuremath{s}}
\newcommand{\gpetersonstate}[1]{\ensuremath{\gname_{#1}}}
\newcommand{\gpetersonview}[1]{\ensuremath{\omega_{#1}}}

\newcommand{\petersonInv}{\fname{petersonInv}}
\newcommand{\isPeterson}{\fname{canLock}}
\newcommand{\petersonLocked}{\fname{locked}}
