\subsection{A Ticket Lock}
\label{sec:examples:ticketlock}

% ------------------------------------------------------------------------------

% Explication du code.

The ticket lock is a variant of the spin lock where a ``ticket dispenser'' is
used to serve threads in the order that they arrive, thereby achieving a
certain level of fairness.
%
A simple implementation of the ticket lock appears in \fref{fig:ticketlock}.
%
A lock is a pair $\Pair\ticketserv\ticketnext$ of two atomic locations
$\ticketserv$ and $\ticketnext$, each of which stores an integer value.
%
The counter $\ticketserv$ displays the number of the ticket that is currently
being served, or ready to be served. The counter $\ticketnext$ displays the
number of the next available ticket.

A thread that wishes to acquire the lock first obtains a unique number~$n$
from the counter~$\ticketnext$, which it increments at the same time.
%
% The code of acquire contains an inlined implementation
% of fetch-and-add on top of CAS. It would be desirable to isolate it.
% However, that would require a logically-atomic spec.
%
This number is known as a \emph{ticket}. Then, the thread waits for its number
to be called: that is, it waits for the counter~$\ticketserv$ to contain the
value~$n$. When it observes that this is the case, it concludes that it has
acquired the lock.

A thread that wishes to release the lock simply increments the
counter~$\ticketserv$, so that the next thread in line is allowed to proceed
and take the lock. To do so, a CAS instruction is unnecessary: a sequence of a
read instruction, an addition, and a write instruction suffices. Indeed,
because the lock is held and can be released by only one thread, no
interference from other threads is possible.
%
% This argument must be explicitly made somewhere in the proof,
% so an exclusive \locked token is required.
%

% ------------------------------------------------------------------------------

% Explication de la preuve.

\input{figure-ticketlock}
\input{figure-ticketlock-invariant}

We now sketch a proof of this ticket lock implementation. Our proof requires a
straightforward modification of the proof carried out by Birkedal and Bizjak
in a sequentially consistent setting (\citeyear[\S9]{iris-lecture-notes}). That is
precisely our point: it is our belief and our hope that, in many cases, a
traditional Iris proof can be easily ported to \hlog. The process is mostly a
matter of identifying which atomic memory locations also serve as information
transmission channels (thanks to the ``release/acquire'' semantics of atomic
writes and reads) and of decorating every Iris invariant with explicit views,
where needed, so as to meet the requirement that every invariant must be
objective.
%
Here, a moment's thought reveals that only the view stored at the
location~$\ticketserv$ matters; the view stored at~$\ticketnext$ is
irrelevant.

The ghost state and the Iris invariant used in the verification of the
ticket lock appear in \fref{fig:ticketlock:invariant}.\footnote{%
%
This ghost state and this invariant are allocated in the body of \make, in the
scope of the local definitions of $\ticketserv$ and $\ticketnext$. The proofs
of the desired triples for \acquire, \wait, and \release also carried out in
this scope. This explains why we are able to get away with an unparameterized
definition of $\isTicketLock$: it is a local definition.
%
}
%
\begin{itemize}
\item
  The ghost location $\gticketserv$
  stores an element of the monoid $\authm(\exm(\nat))$.
  % \cite[\S9]{iris-lecture-notes}
  %
  The exclusive assertion
  $\smash{\ownGhost\gticketserv{\authfrag\vticketserv}}$
  represents both the knowledge that ticket~$\vticketserv$ is currently being served
  and a permission to change who is being served.
  %
  % fp: this unquantified assertion plays a role in justifying
  %     that the value of $\ticketserv$ cannot change between
  %     the read instruction and the write instruction in \release.
  %
  The exclusive assertion ``$\locked$'', defined as
  $\smash{\Exists\vticketserv.\ownGhost\gticketserv{\authfrag\vticketserv}}$,
  represents a permission to change who is being served,
  therefore a permission to release the lock.
\item
  The ghost location $\gticketissued$ keeps track of which tickets have been
  issued since the lock was created.
  It stores an element of $\authm(\setdisjm(\nat))$, where
  $\setdisjm~\nat$ is the monoid whose elements are finite sets of
  integers and whose partial composition law is disjoint set union.
  The exclusive assertion $\issued\vticketnext$ represents the ownership of
  the ticket numbered~$n$.
\item
  The invariant $\isTicketLock$ synchronizes the physical state and the
  ghost state by mentioning the auxiliary variables $\vticketserv$ and $\vticketnext$
  both in points-to assertions and in ghost state ownership assertions.
  %
  The same technique as in the previous subsection (\sref{sec:examples:spinlock})
  is used to make this invariant objective.
  %
  The last conjunct in its definition states that
  either no thread holds the lock
  and the user assertion~$\prop$ holds
  \emph{in the eyes of the thread that last released the lock},
  or
  the invariant owns the ticket numbered~$\vticketserv$.
  This implies that, in order to acquire the lock while maintaining the invariant,
  a thread must present and relinquish the ticket numbered~$\vticketserv$.
\end{itemize}
%
\begin{comment}
% fp: commented out, as Glen says these properties are not used in the proof.
Both $\locked$ and $\issued\_$ are exclusive, in the following sense:
\begin{align*}
  \locked \isep \locked &\wand \FALSE \\
  \issued{\vticketnext_1} \isep \issued{\vticketnext_2} &\wand \vticketnext_1 \not= \vticketnext_2
\end{align*}
\end{comment}
%
We do not sketch the proof further, as it is analogous to Birkedal and
Bizjak's proof (\citeyear[\S9]{iris-lecture-notes}), with the added detail
that the read of $\ticketserv$ in $\wait$ acquires a view and the write
of $\ticketserv$ in $\release$ releases a view.
%
% By the golden idol metaphor \cite{kaiser-17},
% if a thread can present the ticket numbered~$\ticketserv$
% then this proves that the left-hand disjunct of the invariant currently holds,
% allowing this thread to take ownership of the payload of the left-hand disjunct
% and transition to the right-hand disjunct of the invariant,
% giving up its ticket.
%
% Conversely, a thread that releases the lock
% does not (normally) have a ticket at hand,
% so must re-establish the invariant.
%
% Finally, a thread that wishes to enter the waiting line
% manufactures a fresh ticket
% by incrementing $\ticketnext$,
% then is allowed to wait,
% as the specification of the auxiliary function $\wait$
% is
% \(
%   \All\vticketnext.
%   \hoare
%     {\issued\vticketnext}
%     {\wait~\vticketnext~\lock}
%     {\Lam\Unit. \locked \isep \prop}
% \)


\endinput

% Old-style specification of the ticket lock:

\begin{mathpar}
  \HoareRule{\nameticketlockmake}
    {\prop}
    {\make~\Unit}
    {\Lam \lock. \Exists \gname. \isTicketLock~\gname~\lock~\prop}

  \HoareRule{\nameticketlockacquire}
    {\isTicketLock~\gname~\lock~\prop}
    {\acquire~\lock}
    {\Lam \Unit. \locked~\gname \isep \prop}

  \HoareRule{\nameticketlockrelease}
    {
      \isTicketLock~\gname~\lock~\prop
      \isep
      \locked~\gname \isep \prop
    }
    {\release~\lock}
    {\Lam \Unit. \TRUE}

  \HoareRule{\nameticketlockwait}
    {
      \isTicketLock~\gname~\lock~\prop
      \isep
      \issued~\gamma~n
    }
    {\wait~n~\lock}
    {\Lam \Unit. \locked~\gname \isep \prop}
\end{mathpar}

%%  LocalWords:  ticketlock CAS Birkedal Bizjak unparameterized monoid
%%  LocalWords:  Bizjak's
