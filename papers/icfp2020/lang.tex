\section{\mocaml}
\label{sec:lang}

In this section, we recall the \mocaml memory model and present the syntax and
operational semantics of a core calculus that is representative of the \mocaml
programming language.
We start by describing interactions with the memory by the memory subsystem
(\sref{sec:lang:model}), and then we present the reduction rules of the
programming language itself (\sref{sec:lang:proglang}).


% ------------------------------------------------------------------------------

\subsection{Memory Model}
\label{sec:lang:model}

\input{figure-objects}

In this section, we recall Dolan et al.'s definition of the \mocaml memory model
(\citeyear{dolan-18}) and extend it with memory allocation and CAS,
which do not appear in Dolan et al.'s paper.
% fp: this extension is straightforward.
% jh: pas tant que ça, notamment pour les allocations,
%     puisqu'il faut faire attention à ce qu'il y ait toujours au moins
%     une valeur à lire lors d'une lecture NA, même si on ne s'est pas
%     synchronisé avec l'allocation.
% fp: mais ça c'est une complexité pour l'implémenteur du compilo
%     plus qu'une complexité dans la sémantique, non?
%
We first define a number of semantic objects (\fref{fig:objects}), then
describe the behavior of the memory subsystem (\fref{fig:memory}).
%
At this point, we assume a set~$\typeVal$ of values, which is defined later on
(\sref{sec:lang:proglang}).

\subsubsection{Locations}

A memory location has one of two possible kinds, fixed throughout its lifetime:
it is either \emph{nonatomic} or \emph{atomic}.
%
We write $\naloc$ for a nonatomic location
and~$\atloc$ for an atomic one.

\subsubsection{Time Stamps}

% fp: les trois orthographes time stamp, time-stamp, timestamp existent.

We use \emph{time stamps}~$\timest$ to describe the behavior of reads and writes at nonatomic memory
locations. They belong to
$\Q \cap [0, \infty)$, the set of the nonnegative rational numbers. This set
is infinite, totally ordered,
% notation: \timelt, \timeleq
% fp: le fait que l'ensemble est ordonné joue run rôle pour lire la
% sémantique, et le fait qu'il est infini aide à comprendre la règle
% d'allocation (qui ne peut pas tomber à court de time stamps). Donc
% je laisse ces remarques.
and admits a minimum element $\timeMin$,
% Density (the fact that elements can be inserted in between any two
% elements) is not used in any of our proofs, I think.
which is used for initial writes,
and only for initial writes.
%
It is worth noting that the meaning of time stamps is purely ``per location'':
that is, the time stamps associated with two write events at two distinct
locations are never compared.

\subsubsection{Histories}

A \emph{history}~$\hist$ is a finite map of time stamps to values.
%
It represents the history of the write events at a single nonatomic memory
location.
%
If desired, a history can be also be viewed as a set of pairs of a time stamp
and a value, that is, a set of write events, under the constraint that no two
events have the same time stamp.
%
Naturally, a read instruction at a nonatomic memory location must read from
one of the write events in the history of this location, and a write
instruction must extend the history of this location with a write event at a
previously-unused time stamp. In either case, which time stamp may be chosen
depends on the ``view'' of the active thread, described next.

\subsubsection{Views}
\label{sec:lang:model:views}

A \emph{view}~$\view$, referred to as a ``frontier'' by \citet{dolan-18},
is a total mapping of nonatomic memory locations to time stamps
that is \emph{almost everywhere zero} (``aez'' for short),
that is, whose value is zero everywhere
except a finite set of locations.
%
% Another definition for views might then be:
% \[
%   \typeView  \eqdef  \typeNALoc \maparrow (\typeTime - \{\timeMin\}) \\
% \]
%
Each thread has a view, which evolves over time.
%
The view of a thread maps each nonatomic memory location to (the time stamp
of) the most recent write event at this location this thread is aware of.
%
% Note: a view *cannot* be seen as a set of write events.
%   Indeed, future write events can receive an old time stamp
%   and thus become part of an existing view.
%
The view of the active thread imposes a constraint on the behavior of reads and
writes at nonatomic memory locations (\sref{sec:lang:semantics}).

The order on time stamps gives rise to a partial order on views:
by definition,
the view inequality
$\view_1 \viewleq \view_2$
holds if and only if,
for every nonatomic memory location $\naloc$,
the time stamp inequality
$\viewapp{\view_1}\naloc \timeleq \viewapp{\view_2}\naloc$ holds.
%
This order can be thought of as an \emph{information} order:
if $\view_1 \viewleq \view_2$ holds,
then a thread with view $\view_2$ has more information
(is aware of more write events) than a thread with view~$\view_1$.
%
% This also implies that a nonatomic read or write instruction
% issued by a thread with view $\view_2$ has fewer permitted behaviors
% than the same instruction issued by a thread with view $\view_1$.
%
% Glen says:
%   Think of a view as a mask.
%   $\view_1 \viewleq \view_2$ means $\view_1$ allows more reads,
%   $\view_1$ is a weaker mask, $\view_1$ is less restrictive a mask.
%   The empty view $\emptyview$ allows all reads.
%
Equipped with this order, views form a bounded semilattice.
% $(\typeView, \viewleq, \viewjoin, \emptyview)$.
%
Its minimum element is the \emph{empty view}~$\emptyview$,
which maps every nonatomic memory location to the time stamp $\timeMin$.
%
Its join operation is pointwise maximum: that is,
$\view_1 \viewjoin \view_2$
is $\Lam \naloc. \max~(\viewapp{\view_1}\naloc, \viewapp{\view_2}\naloc)$.

\subsubsection{Stores}

% We take the set of nonatomic memory locations \typeNALoc{} and the set of
% atomic memory locations \typeATLoc{} to be countably infinite.
%
% fp: I don't think they need to be disjoint; they are separate name spaces anyway.
%
A \emph{store} $\store$ is a pair of a nonatomic store and an atomic store.
%
A \emph{nonatomic store}~$\store\NA$ is
a finite map of nonatomic memory locations to histories:
as suggested earlier, each nonatomic memory location
is mapped to a history of all write events at this location.
%
An \emph{atomic store}~$\store\AT$ is
a finite map of atomic memory locations to pairs of a value and a view.
% notation: $\mkval\val\view$
Indeed, atomic memory locations in \mocaml serve two purposes, which are
conceptually independent of one another.
%
On the one hand, each atomic memory location stores a value. A single value,
as opposed to a history, suffices, because atomic memory locations have
sequentially consistent semantics.
%
On the other hand, each atomic memory location stores a view,
that is, a certain amount of information about the nonatomic store.
%
This view is read and updated by atomic read and write instructions, thereby
giving atomic memory locations a ``release/acquire'' semantics.
%
% As explained by \citet{dolan-18}, ``nonatomic writes made by one thread can
% become known to another by communicating via an atomic location.''
%

% The fact that atomic locations contain both a value *and* a view is somewhat
% of an accident -- these two roles are entirely orthogonal. (That said, it is
% certainly convenient that a single atomic operation can read or write both a
% value and a view, so one would not advocate separating these roles.)

\subsubsection{The Memory Subsystem}

\input{figure-memory}

To complete the definition of the \mocaml memory model, there remains to give
an operational description of the behavior of the memory subsystem. This is
done via a labelled transition system, that is, a relation
$\store; \thisview \memReduces{\smash\someMemEvent} \store'; \thisview'$, which
describes how the shared global store~$\store$ and the view~$\thisview$ of the
active thread evolve through a memory event~$\someMemEvent$, yielding an
updated store~$\store'$ and an updated view~$\thisview'$.
%
The syntax of memory events is as follows:
%
\[
  \someMemEvent ::=
    \noMemEvent \mid
    \memEventAllocNA\naloc\val \mid
    \memEventReadNA\naloc\val \mid
    \memEventWriteNA\naloc\val \mid
    \memEventAllocAT\atloc\val \mid
    \memEventReadAT\atloc\val \mid
    \memEventWriteAT\atloc\val \mid
    \memEventUpdate\atloc\val{\val'}
\]
These memory events also appear in the definition of the semantics of \mocaml
expressions (\sref{sec:lang:semantics}). Thus, they form a language by which
the expression and memory subsystems communicate.

The rules that define the relation
$\store; \thisview \memReduces{\smash\someMemEvent} \store'; \thisview'$
(\fref{fig:memory})
can be briefly described as follows.

% These rules are taken from the PLDI paper, except that we added
% allocations and CAS.

When a fresh nonatomic memory location is allocated (\memnaalloc),
its history consists of a single write event at time stamp~$\timeMin$.
%
This guarantees that a read of this location will succeed, even if the reading thread
has not synchronized with the thread
where allocation was performed.
% fp: ce qui sous-entend alors que l'adresse de cette location a été
%     transmise via une lecture non-atomique?
%     Mai-z-alors, nous sommes en fait en train de discuter d'un programme
%     qui n'est pas vérifiable en Cosmo, car il exploite une data race
%     sur un nonatomique. C'est bien ça?

% jh: oui c'est exactement ça. Mais il faut que ce soit pris en compte
%     dans la sémantique (e.g., pour prouver la sûreté du typage). Et
%     puis, on pourrait imaginer que le mapsto fourni au moment de
%     l'allocation est objectif jusqu'à la première écriture.
As suggested earlier (\sref{sec:lang:model:views}),
the view of the active thread imposes a constraint on the behavior of read and
write instructions at nonatomic memory locations.
%
A read instruction at location~$\naloc$ (\memnaread) cannot read from an
outdated write event, therefore must read from an event whose time
stamp~$\timest$ is no less than $\viewapp\thisview\naloc$. Both
$\viewapp\thisview\naloc=\timest$ and $\viewapp\thisview\naloc\timelt\timest$ are
permitted: the latter case allows a thread to read from a write event of which
it is not yet aware. A nonatomic read instruction does \emph{not} update
the view of the active thread.
%
A~write instruction at location~$\naloc$ (\memnawrite) cannot produce an
outdated write event, therefore must use a time stamp~$\timest$ greater than
$\viewapp\thisview\naloc$. The new time stamp must be fresh (that is, not in
the domain of $\hist$), but there is no requirement for~$\timest$ to be greater
than every time stamp in the domain of $\hist$. The history of location~$\naloc$
and the view of the active thread are updated so as to include the new write
event.

Allocating a fresh atomic memory location (\mematalloc) causes it to be
initialized with the value~$\val$ provided by the allocation instruction and
with the view $\thisview$ of the active thread.
%
When reading an atomic memory location (\mematread) the view~$\view$ stored in
this location is merged into the
view of the active thread, reflecting the idea that the active thread acquires
information via this read operation.
%
Writing an atomic memory location (\mematwrite) overwrites the value~$\val$
stored at this location with the new value~$\val'$ and updates the view
$\view$ stored at this location by merging the view~$\thisview$ of the active
thread into~it, reflecting the idea that the active thread releases
information via this write operation. At the same time, the view of the active
thread is updated; indeed,
% in the \mocaml memory model,
an atomic write has both a ``release'' and an ``acquire'' effect.
%
The rule \mematreadwrite models a successful CAS instruction.
It which combines the effects of a read and a write in a single atomic
step.
%
An unsuccessful CAS instruction behaves like an atomic read.

As \mocaml is equipped with a garbage collector, there is no explicit
deallocation event.

% ------------------------------------------------------------------------------

\subsection{Programming Language}
\label{sec:lang:proglang}

We now present
% the syntax and operational semantics of
a core
calculus that is representative of the \mocaml programming language.
It is equipped with a dynamic thread creation operation
and with the two kinds of memory locations
% governed by the \mocaml memory model
presented earlier. % (\sref{sec:lang:model}).
% We omit the features that have no interaction with the memory model.
%
For simplicity, we refer to this calculus as \mocaml.

\subsubsection{Syntax}
\label{sec:lang:syntax}

\input{figure-lang-syntax}

% footnote removed to gain space
\newcommand{\footnoteaccessmodes}{\footnote{%
  This is in contrast with, say, C11~\cite{c11mm,rc11},
  where there is just one kind of memory location,
  but every memory access instruction is labeled with an
  access mode, also known as a ``memory order'',
  and there are quite a few different access modes.}}
% memory_order_relaxed,
% memory_order_consume,
% memory_order_acquire,
% memory_order_release,
% memory_order_acq_rel,
% memory_order_seq_cst

The syntax of the calculus appears in \fref{fig:syntax}.
%
It is a call-by-value \lc,
equipped with recursive functions,
primitive operations on Boolean and integer values,
tuples,
and standard control constructs, including conditionals and sequencing.
%
This calculus is extended with shared-memory concurrency, as follows.
%
% \begin{itemize}
% \item
First,
the construct $\Fork\expr$ spawns a new thread. In the parent thread, this
operation immediately returns the unit value~$\Unit$. In the new thread, the
expression~$\expr$ is executed, and its result is discarded.
% \item
Second, the language supports the standard operations of allocation, reading,
and writing, on both nonatomic and atomic memory locations.
%
\begin{comment}
For example, the expression $\AllocNA\val$ allocates a fresh
nonatomic memory location and initializes it with the value $\val$; the
expression $\ReadNA\naloc$ reads the nonatomic memory location $\naloc$; and
the expression $\WriteNA\naloc\val$ writes the value~$\val$ to the
location~$\naloc$.
\end{comment}
In addition, atomic memory locations
support a compare-and-set (CAS) operation.%
\footnote{%
  In the surface language, nonatomic locations have type
  \texttt{'a ref}. Their operations are
  \texttt{ref},~\texttt{!}, and~\texttt{:=}.
  % Mutable record fields are also nonatomic.
  % Mutable arrays are nonatomic.
  Atomic locations have type
  \href{https://github.com/ocaml-multicore/ocaml-multicore/blob/master/stdlib/atomic.mli}
    {\texttt{'a Atomic.t}}.
  Their operations are
  \texttt{Atomic.make}, \texttt{Atomic.get}, \texttt{Atomic.set},
  and \texttt{Atomic.compare\_and\_set}.
}
%
% There is no deallocation operation.
% \end{itemize}

\subsubsection{Operational Semantics}
\label{sec:lang:semantics}

\input{figure-reduction}

% The operational semantics of the \mocaml calculus is defined in two layers.

The ``per-thread'' reduction relation
$\expr \exprReduces{\smash\someMemEvent} \expr', \expr'_1, \ldots, \expr'_n$,
describes how an expression $\expr$ takes a step and reduces to a new
expression $\expr'$,
possibly interacting with the memory subsystem via an event $\someMemEvent$,
and possibly spawning a number of new threads $\expr'_1, \ldots, \expr'_n$.
%
% \footnote{In fact, a reduction step can either emit a memory event or spawn
%   new threads, but not both. Also, the number of newly spawned threads is
%   always zero or one.}
%
The bulk of the rules that define this reduction relation form a standard
small-step reduction semantics for call-by-value \lc. These rules do not
involve an interaction with the memory subsystem
% (they are annotated with the silent event $\noMemEvent$,
%  or, for the reduction rules under a context, they repeat the event
%  that appears in their premis)
and do not spawn new threads. We omit them. In \fref{fig:reduction}, we
present the reduction rules for the memory access operations, which interact
with the memory subsystem via a memory event, and the reduction rule for
``fork'', which spawns one new thread.

In \fref{fig:semantics}, we define the ``thread-pool'' reduction relation
$\store; \pool \threadPoolReduces \store'; \pool'$,
which describes how the machine steps
between two configurations $\store; \pool$ and $\store'; \pool'$.
In such a configuration, $\store$ is a store,
while $\pool$ is a thread pool, that is, a list % (or multiset)
of threads,
where each thread $\mkexpr\expr\thisview$ is a pair of
an expression~$\expr$
% (the code that this thread is running)
and a view~$\thisview$.
% (the view of this thread)
%
The left-hand rule allows the per-thread execution system and the memory
subsystem to synchronize on an event~$\someMemEvent$. The right-hand
rule shows how new threads are spawned; a newly-spawned thread inherits the
view of its parent thread. Because a per-thread reduction step cannot both
access memory and spawn new threads, these two rules suffice.

The operational semantics of \mocaml is the reflexive transitive
closure of the thread-pool reduction relation. It is therefore an interleaving
semantics, albeit not a sequentially consistent semantics, as it
involves a store whose behavior is nonstandard.

A machine configuration $\store; \pool$ is considered \emph{stuck} if the
thread pool~$\pool$ contains at least one thread that cannot take a step yet
has not reached a value.
%
A stuck configuration represents an undesirable event, a~runtime error. There
are many ways of constructing stuck configurations: examples include applying
a primitive operation to arguments of incorrect nature, attempting to call
a value other than a function, and so on. All such errors are ruled out by the
OCaml type system:\footnote{No changes to the type system are required
  by the move from OCaml to \mocaml. The type \texttt{'a Atomic.t} is a new
  (primitive) abstract type.} the execution of a well-typed program cannot
lead to a stuck configuration. Although this claim does not appear to have been
formally established, it seems clear that a syntactic proof of type soundness
for~ML with references~\cite{wright-felleisen-94} can be adapted to the
semantics of \mocaml.
%
% The key is to note that every value in a history must be well-typed
% according to the store typing.
%

A careful reader might note that a nonatomic read instruction (\memnaread)
\emph{could} potentially be stuck under certain circumstances. This could be
the case, for instance, if the history~$\hist$ is empty, or if the time
stamp~$\viewapp\thisview\naloc$ is too high, causing all write events in~$\hist$ to be
considered outdated. In reality, though, neither of these situations can
arise, because only a subset of \emph{well-formed} machine configurations can
be reached. In a well-formed configuration, every nonatomic location ever
allocated must have a nonempty history, and no thread can get hold of an
unallocated location, thereby removing the concern that $\hist$ might be empty.
% A similar invariant exists in ML with references.
% fp: Je crois que chez nous, on n'a pas besoin de cet invariant, car en
%     produisant une preuve de possession (fractionnelle) de la référence,
%     on prouve qu'elle est allouée. Ensuite le global view invariant
%     ci-dessous suffit à garantir que son historique est non vide.
Furthermore, a well-formed configuration must satisfy the following
\emph{global view invariant}: there exists a \emph{global view}~$\globalview$
such that:
\begin{enumerate}
\item
\label{gvi1}
  every thread's view~$\thisview$ is contained in $\globalview$,
  that is,
  $ \All\mkexpr\expr\thisview\in\pool.\;\thisview\viewleq\globalview $ holds;
\item
\label{gvi2}
  the view of an atomic location is contained in $\globalview$,
  that is,
  $
  \All (\atloc, \mkval{\val}{\viewatloc}) \in \store\AT.\;
  \viewatloc \viewleq \globalview
  $ holds;
\item
\label{gvi3}
  the global view maps every nonatomic location to a time stamp
  that exists in the history of this location, that is,
  $
  \All (\naloc, \hist) \in \store\NA.
  \viewapp\globalview\naloc \in \Dom\hist
  $ holds;
\item
\label{gvi4}
  the global view maps every currently unallocated location
  to the time stamp~$\timeMin$, that is,
  $
 \All \naloc \notin \Dom{\store\NA}.
 \viewapp\globalview\naloc = \timeMin
  $ holds.
\end{enumerate}
%
One can check that this is an indeed an invariant (i.e., it is preserved by
every reduction step) and that this invariant implies that a nonatomic read
instruction cannot be stuck.
%
% Items~(\ref{gvi1}) and (\ref{gvi3}) together guarantee that
% a nonatomic read instruction cannot be stuck.
% %
% Item~(\ref{gvi2}) ensures that an atomic read operation preserves
% item~(\ref{gvi1}).
% %
% Item~(\ref{gvi4}) ensures that the allocation of a nonatomic location
% preserves item~(\ref{gvi3}).
%
In fact, in the next section, we must prove this claim, and exploit this
invariant, otherwise we would be unable to prove that our logic is sound:
a~sound program logic must guarantee that a verified program cannot get stuck.
Thus, when we instantiate Iris for \mocaml, we build the global view invariant
into our ``state interpretation'' invariant
(\sref{sec:logic:lo:instantiation}), and when we establish the reasoning rule
for nonatomic read instructions (rule \basenaread in \fref{fig:llog:rules}),
we exploit it (\sref{sec:logic:lo:axioms}).

%%  LocalWords:  nonatomic CAS deallocation Dolan et al dolan nonnegative aez
%%  LocalWords:  semilattice pointwise labelled multiset runtime OCaml
%%  LocalWords:  unallocated
