\subsection{Peterson's Algorithm}
\label{sec:examples:peterson}

\input{figure-peterson-spec}
\input{figure-peterson}
\input{figure-peterson-invariant}

Peterson's algorithm~\cite[\S2.1.2]{raynal}
is a mutual exclusion algorithm for two threads,
% which ensures some form of fairness,
whose implementation requires only read and write operations
on sequentially consistent registers: no CAS is needed.
%
Peterson's algorithm remains valid in \mocaml, provided atomic locations are
used in its implementation. This is not an entirely trivial claim, as the user
invariant~$\prop$ may involve nonatomic locations.

Like an ordinary lock, this data structure offers three operations, namely
\make, \acquire and \release. This time, \make returns a pair of two handles:
each participant thread is expected to use one of these handles.
%
The~specification of this data structure (\fref{fig:peterson:spec})
%
is analogous to the specification of locks (\fref{fig:lock:spec}), but
introduces a new token, $\isPeterson~i$, so as to limit the number of
participant threads to two: \make produces only two tokens
$\isPeterson~0$ and $\isPeterson~1$.
%
The code, shown in \fref{fig:peterson}, uses three atomic locations:
\begin{itemize}%
  \item Each of the two threads $i \in \{0,1\}$ uses a Boolean register
    $\petersonflag_i$ to indicate it currently holds the lock or
    intends to acquire it.
    Both threads read $\petersonflag_i$ but only thread~$i$ writes it.
  \item An integer register \petersonturn indicates which thread has priority,
    should both threads simultaneously attempt to acquire the lock.
    % glen: again the meaning of "simultaneously" is unclear
    % fp: pas de problème, c'est informel
    It is read and written by both threads.
  \end{itemize}

We now outline our proof that the code in \fref{fig:peterson} satisfies the
specification in \fref{fig:peterson:spec}.
%
As we use a logic of partial correctness, we do not prove deadlock-freedom
or fairness. We only verify that this data structure behaves like a lock.
% and can safely be used as such.

The algorithm's possible states are summarized in~\fref{fig:peterson:states}.
%
At any time, each of the participant threads is in one of four possible
states: either it is outside of a critical section (\petersonoutside),
% delimited by \release and \acquire
or it has just entered \acquire and set its flag to $\True$ (\petersonstartacq),
or it has written \petersonturn and is now waiting (\petersonwaiting),
or it is inside a critical section (\petersoncritical).
%
\fref{fig:peterson:invariant} presents the algorithm's invariant.
%
We use two ghost variables $\gpetersonstate 0$ and $\gpetersonstate 1$ to keep
track of the logical state of each thread.

The key novelty, with respect to the proof that one could carry out in a
sequentially consistent setting, is that the invariant must record the
view~$\view_i$ that was the view of thread~$i$ when this thread last released
the lock. This view
% (or a better view)
is stored at the atomic location $\petersonflag_i$.
%
When the lock is available, because the lock could have been last released by
either thread, the assertion~$\prop$ holds either at $\view_0$ or at
$\view_1$. Because \hlog assertions are monotonic, this implies that
$\prop$~holds at the combined view $\view_0 \viewjoin \view_1$. The
invariant records this fact.

Thus, in the proof of \wait, when \wait terminates, the invariant can give us
$\prop\opat(\view_0 \viewjoin \view_1)$. A key step is to argue that we also
have $\seen{\view_1}$ and $\seen{\view_2}$, so as to then combine the pieces
via \splitso and obtain $\prop$. Let us briefly sketch why this is the case.

Establishing $\seen{\view_i}$ is easy enough, because $\view_i$ is a past view
of this very thread. One way of recording this information is to let the token
$\isPeterson~i$ carry it; indeed, the role of this token is precisely to carry
information from a \release to the next \acquire.
%
% fp: note that this token is an example of subjective ghost state,
%       following \citet{dang-20}
%
% Indeed, $\view_i$ is the view of this thread at the time the lock was
% released, and this is precisely when this token was created.
% Furthermore, the token is at hand in \acquire, since it appears in
% the precondition of
%
% jh: Ok, c'est vrai, mais on pense que ça nécessiterait une
% explication assez étoffée, et ce on n'a pas la place, et ce n'est pas le
% sujet ici.
%
We include in the definition of $\isPeterson~i$ an assertion $\seen\view$
and set up a ghost variable $\gpetersonview{i}$ whose purpose is to allow
us to prove that $\view$ is in fact~$\view_i$.
%
% fp: c'est un peu lourd visuellement, même si ça semble en fait
%           assez naturel. On peut se demander quel sucre de plus haut
%           niveau on pourrait introduire (en Coq et dans l'article)
%           pour rendre cela moins lourd. En gros l'invariant devait
%           être un record avec des champs fantômes appartenant à
%           des monoïdes Auth(Ex T), et on devrait pouvoir utiliser
%           le nom du champ directement pour faire référence à son contenu...
% jh, glen: ok, mais rajouter une nouvelle abstraction et l'expliquer
% prendrait plus de place que cela ne permettrait de gagner en clarté.
%
% fp: remarque de Glen cachée il faudrait plus de place:
% Instead of carrying $\view_i$ via a predicate $\seen \view$, we could as
% well recover it from the \mocaml-specific fact that the write to
% $\petersonturn_i$ also performs read-like synchronization; but we still need
% the ghost variable, to ensure that the view $\view_i$ of the invariant is
% not altered between the moment we write to $\petersonturn_i$ and the moment
% we finally claim $\prop$.
%
% jh: Oui, c'est intéressant, mais la preuve est déjà assez compliqué
% comme ça pour faire des apartés.

There remains to argue that we have $\view_{1-i}$
when \wait terminates.
Because of the conjunction in the conditional statement,
the loop in \wait has two exit points:
\begin{itemize}
\item
The loop can terminate immediately after reading $\petersonflag_{1-i}$
yields the value~$\False$. In this case, the thread acquires $\view_{1-i}$
via this read.
\item
Or, the loop terminates after reading \petersonturn yields the value~$i$. We
must prove that the thread acquires~$\view_{1-i}$ via this read. This is
nonobvious: it requires us to argue that the view~$\view$ stored at
location~\petersonturn must in fact contain $\view_{1-i}$.
%
The intuitive reason why this is true is that the two participants write
\petersonturn in a polite way, always giving priority to the other thread.
Thus, thread~$0$ writes only the value~$1$ to \petersonturn, and vice~versa.
Thus, when thread~$i$ reads~$i$ from \petersonturn, this value must have been
written by thread $1-i$, which implies that this read allows thread~$i$ to
acquire the view~$\view_{1-i}$.
%
Technically, this argument is reflected in the invariant by the proposition
%
$
  \left(\vpetersonturn = 0 \land \vpetersonstate_0 = \petersonwaiting\right)
  \implies
  \left(\vpetersonstate_1 = \petersonwaiting \land \view = \view_1\right)
$
and its symmetric counterpart.
\end{itemize}
% fp: joli!

%%  LocalWords:  CAS nonatomic peterson nonobvious versa

\endinput

% Note de Glen:

«Peterson separation logic» dans Google Scholar ne donne aucun résultat. avec
«mutual exclusion separation logic», j’ai trouvé la thèse de Vafeiadis:

	https://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-726.html

Apparemment il a une preuve de Peterson mécanisée dans son outil
SmallfootRG, qui implémente sa logique RGSep (Rely/Guarantee +
Separation logic) dans l’outil Smallfoot:

http://www0.cs.ucl.ac.uk/staff/p.ohearn/papers/smallfoot_v0.1.pdf

(outil de vérification automatique de certaines specs «légères» en logique de
séparation); mais aucun détail n’est donné.
