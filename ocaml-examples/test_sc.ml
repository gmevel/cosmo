(* The purpose of sleeping at the start of each thread is to make them yield and
 * compensate for the bias towards the first thread, so that they get more
 * chance of being interleaved. *)

let run2 (type a b) (f1 : unit -> a) (f2 : unit -> b) : a * b =
  let t1 = Domain.spawn (fun () -> Unix.sleepf 0.0 ; f1 ()) in
  let y2 = Unix.sleepf 0.0 ; f2 () in
  let y1 = Domain.join t1 in
  (y1, y2)

(*
let () =
  let count = [| ref 0 ; ref 0 ; ref 0 ; ref 0 |] in
  for i = 1 to 100_000 do
    (* if i mod 100 = 0 then Gc.compact () ; *)
    Gc.compact () ;
    if i mod 1_000 = 0 then begin
      Gc.print_stat stderr ;
      flush stderr ;
    end ;
    let x = ref 0 and y = ref 0 in
    let (a, b) = run2
        (fun () -> x := 1 ; !y)
        (fun () -> y := 1 ; !x)
    in
    incr count.(2*a + b)
  done ;
  Printf.printf "count 0 0 = %6u\n" !(count.(0)) ;
  Printf.printf "count 0 1 = %6u\n" !(count.(1)) ;
  Printf.printf "count 1 0 = %6u\n" !(count.(2)) ;
  Printf.printf "count 1 1 = %6u\n" !(count.(3)) ;
*)

let x = ref 0
let y = ref 0

let f1 =        (fun () -> x := 1 ; !y)
let f2 =        (fun () -> y := 1 ; !x)

let () =
  let count = [| ref 0 ; ref 0 ; ref 0 ; ref 0 |] in
  for i = 1 to 100_000 do
    (* if i mod 100 = 0 then Gc.compact () ; *)
    if i mod 1_000 = 0 then begin
      Gc.print_stat stderr ;
      flush stderr ;
    end ;
    x := 0 ; y := 0 ;
    let (a, b) = run2 f1 f2 in
    incr count.(2*a + b)
  done ;
  Printf.printf "total     = %#9u\n" (!(count.(0)) + !(count.(1)) + !(count.(2)) + !(count.(3))) ;
  Printf.printf "count 0 0 = %#9u\n" !(count.(0)) ;
  Printf.printf "count 0 1 = %#9u\n" !(count.(1)) ;
  Printf.printf "count 1 0 = %#9u\n" !(count.(2)) ;
  Printf.printf "count 1 1 = %#9u\n" !(count.(3)) ;
