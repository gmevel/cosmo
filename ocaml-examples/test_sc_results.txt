#### some results obtained, demonstrating violation of sequential consistency
#### CPU: Intel Core i7-6500U @ 2.5 GHz

### without sleeping:


total   = 1_000_000
count00 =        63
count01 =   967_524
count10 =    32_410
count11 =         3


### without sleeping, threads are inlined:


total   =   100_000
count00 =         3
count01 =    99_123
count10 =       873
count11 =         1

total   =   100_000
count00 =         2
count01 =    99_198
count10 =       800
count11 =         0

total   =   100_000
count00 =         5
count01 =    99_110
count10 =       885
count11 =         0

total   =   100_000
count00 =         1
count01 =    98_885
count10 =     1_114
count11 =         0


### with `sleep 0` at the start of f1 and f2:


total   =   100_000
count00 =         8
count01 =     4_774
count10 =    95_206
count11 =        12

total   =   100_000
count00 =        14
count01 =     6_781
count10 =    93_190
count11 =        15

total   =   100_000
count00 =        13
count01 =     5_827
count10 =    94_158
count11 =         2


### with `sleepf 0.0001` at the start of f1 and f2:


total   =   100_000
count00 =         4
count01 =    64_271
count10 =    35_722
count11 =         3

total   =   100_000
count00 =         1
count01 =    66_653
count10 =    33_344
count11 =         2

total   =   100_000
count00 =         3
count01 =    65_808
count10 =    34_179
count11 =        10

total   =   100_000
count00 =         4
count01 =    43_035
count10 =    56_954
count11 =         7

total   =   100_000
count00 =         0
count01 =    59_198
count10 =    40_799
count11 =         3


### with `sleepf 0.00001` at the start of f1 and f2:


total   =   100_000
count00 =        53
count01 =    90_971
count10 =     8_971
count11 =         5

total   =   100_000
count00 =        18
count01 =    83_398
count10 =    16_577
count11 =         7

total   =   100_000
count00 =        66
count01 =    93_913
count10 =     6_021
count11 =         0

total   =   100_000
count00 =        18
count01 =    45_930
count10 =    54_048
count11 =         4

total   =   100_000
count00 =        31
count01 =    94_699
count10 =     5_266
count11 =         4

total   =   100_000
count00 =        26
count01 =    94_491
count10 =     5_481
count11 =         2


### with `sleepf 0.000001` at the start of f1 and f2:


total   =   100_000
count00 =        29
count01 =    84_828
count10 =    15_142
count11 =         1

total   =   100_000
count00 =        19
count01 =    94_509
count10 =     5_471
count11 =         1


### with `sleepf 0.` at the start of f1 and f2:


total   =   100_000
count00 =        56
count01 =    94_598
count10 =     5_346
count11 =         0

total   =   100_000
count00 =        48
count01 =    83_350
count10 =    16_601
count11 =         1

total   =   100_000
count00 =         8
count01 =    94_183
count10 =     5_806
count11 =         3

total   =   100_000
count00 =        15
count01 =    91_236
count10 =     8_748
count11 =         1

total   =   100_000
count00 =        74
count01 =    95_485
count10 =     4_438
count11 =         3

total   =   100_000
count00 =        17
count01 =    84_222
count10 =    15_760
count11 =         1

total   =   100_000
count00 =        28
count01 =    93_597
count10 =     6_371
count11 =         4


### with `sleepf 0.` in run2 before f1 and f2:


total   =   100_000
count00 =        16
count01 =    94_621
count10 =     5_361
count11 =         2

total   =   100_000
count00 =        40
count01 =    95_494
count10 =     4_463
count11 =         3

total   =   100_000
count00 =        25
count01 =    91_519
count10 =     8_456
count11 =         0


### with `sleepf 0.` in run2 before f1 and f2, threads are inlined:


total   =   100_000
count00 =        58
count01 =    93_903
count10 =     6_039
count11 =         0

total   =   100_000
count00 =        53
count01 =    88_029
count10 =    11_918
count11 =         0

total   =   100_000
count00 =       186
count01 =    96_234
count10 =     3_579
count11 =         1

total   =   100_000
count00 =       195
count01 =    96_201
count10 =     3_603
count11 =         1

total   =   100_000
count00 =       215
count01 =    96_156
count10 =     3_629
count11 =         0

total   =   100_000
count00 =        82
count01 =    88_784
count10 =    11_132
count11 =         2

total   =   100_000
count00 =        16
count01 =    73_300
count10 =    26_682
count11 =         2

total   =   100_000
count00 =         7
count01 =    94_924
count10 =     5_069
count11 =         0

total   =   100_000
count00 =       136
count01 =    90_152
count10 =     9_711
count11 =         1

total   =   100_000
count00 =       163
count01 =    95_327
count10 =     4_509
count11 =         1

# sum of the preceding 10 sequences:
total   = 1_000_000
count00 =     1_111
count01 =   913_010
count10 =    85_871
count11 =         8

total   =   100_000
count00 =       100
count01 =    93_316
count10 =     6_581
count11 =         3

total   =   100_000
count00 =        99
count01 =    95_009
count10 =     4_891
count11 =         1

total   =   100_000
count00 =       106
count01 =    96_999
count10 =     2_895
count11 =         0

total   =   100_000
count00 =       143
count01 =    96_021
count10 =     3_834
count11 =         2

total   =   100_000
count00 =       170
count01 =    96_254
count10 =     3_573
count11 =         3

total   =   400_000
count00 =       548
count01 =   382_024
count10 =    17_418
count11 =        10

total   =   500_000
count00 =       293
count01 =   478_954
count10 =    20_741
count11 =        12
