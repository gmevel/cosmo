module D = Domain
module A = Atomic

let flag : bool A.t array = Array.init 2 (fun tid -> A.make false)
let turn : int A.t = A.make ~-1
let lock : int option A.t = A.make None

let critical ~(my_tid : int) (type a) (f : unit -> a) : a =
  assert (0 <= my_tid && my_tid < 2) ;
  let other_tid = 1 - my_tid in
  A.set flag.(my_tid) true ;
  A.set turn other_tid ;
  while A.get flag.(other_tid) && A.get turn = other_tid do
    D.Sync.cpu_relax ()
  done ;
  let lock_value = Some my_tid in
  assert (A.compare_and_set lock None lock_value) ;
  let result = f () in
  assert (A.compare_and_set lock lock_value None) ;
  A.set flag.(my_tid) false ;
  result

let run2 (type a b) (f1 : int -> a) (f2 : int -> b) : a * b =
  let num_threads = A.make 0 in
  let t1 = D.spawn (fun () -> f1 (A.fetch_and_add num_threads 1))
  and t2 = D.spawn (fun () -> f2 (A.fetch_and_add num_threads 1)) in
  let y1 = D.join t1
  and y2 = D.join t2 in
  (y1, y2)

let pp_buffered ~(my_tid : int) (file : out_channel) =
  Printf.fprintf file

let pp_unbuffered ~(my_tid : int) (file : out_channel) =
  Printf.ksprintf begin fun (s : string) ->
    for i = 0 to String.length s - 1 do
      output_char file s.[i] ;
      flush file
    done
  end

let pp_unbuffered_atomic ~(my_tid : int) (file : out_channel) =
  Printf.ksprintf begin fun (s : string) ->
    critical ~my_tid begin fun () ->
      output_string file s ;
      flush file
    end ;
    Unix.sleepf 0.00000000001
  end

let pp = pp_unbuffered_atomic
let ppc = pp_unbuffered

let thread1 (file : out_channel) (my_tid : int) =
  for i = 1 to 100 do
    pp ~my_tid file "[%i] pre section %i\n" my_tid i
  done ;
  (*
  critical ~my_tid begin fun () ->
    for i = 1 to 1_000_000 do
      ppc ~my_tid file "[%i] critical section %i\n" my_tid i
    done
  end ;
  *)
  for i = 1 to 100 do
    pp ~my_tid file "[%i] post section %i\n" my_tid i
  done

let thread2 (file : out_channel) (my_tid : int) =
  for i = 1 to 100 do
    pp ~my_tid file "[%i] PRE SECTION %i\n" my_tid i
  done ;
  (*
  critical ~my_tid begin fun () ->
    for i = 1 to 1_000_000 do
      ppc ~my_tid file "[%i] CRITICAL SECTION %i\n" my_tid i
    done
  end ;
  *)
  for i = 1 to 100 do
    pp ~my_tid file "[%i] POST SECTION %i\n" my_tid i
  done

let () =
  let file = open_out "peterson.out" in
  run2 (thread1 file) (thread2 file) |> ignore ;
  close_out file
