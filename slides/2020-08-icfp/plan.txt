2020-06-08

plan exposé ICFP
----------------



INTRO:

ne pas présenter le modèle mémoire dans un 1er temps

traditionnellement en CSL à grain fin pour SC (p.ex. Iris sur lequel on se base):
  - assertions pointsto objectif
  - invariants qui permettent aux threads d’agir sur une ressource partagée

«chaque thread voit la mémoire différement»
-> assertions subjectives (↦, s’il existe, est forcément subjectif)

[uniquement si on parle de BaseCosmo dans l’exposé:]
(
on peut imaginer un pointsto plus compliqué avec un historique (objet compliqué)
on veut retrouver l’illusion de la simplicité
)

problématique: comment retrouver une SL simple & correcte ?
  problématique générale, solution particulière pour M-OCaml

[à déplacer au bon endroit:]
remarque: on ne traite que les programmes data-race free

-> notion de vue (non explicitée pour l’instant)
-> règle SPLIT-SUBJ-OBJ:  P ⊣⊢ P @ V ∗ ↑ V
    + on utilisera 2 mécanismes distincts pour transférer ces 2 types d’assertions
    -> invariants objectifs, accès atomiques

contrib: une logique pour M-Ocaml, basée sur ces idées

présentation rapide Multicore OCaml
  - langage en dvlpt
  - a un modèle mémoire clairement défini: citation



PARTIE 1b: MODÈLE MÉMOIRE / COSMO:

location atomique: porte une valeur SC
-> ↦at simplifié

location non-atomique: comportement relâché (exemple: datarace = aucune garantie)
-> ↦na subjectif, ne peut pas apparaitre dans un invariant
-> aucune datarace autorisée sur les NA

pb: on ne peut pas transférer la possession de NA (point de vue logique)
/ accéder à un loc NA depuis 2 threads (point de vue modèle mémoire) (message passing impossible)

solution: les accès atomiques font de la synchronisation rel-acq
à présenter de façon axiomatique sans parler de vue tout de suite

montrer sur un exemple: exécution du spin-lock
  { P }         ||
  //release lk: ||
  lk :=at false ||
                || //acquire lk:
                || CAS lk false true //success
                || { P }

pour modéliser ça dans la logique: notion de vue = information subjective de chaque thread
sémantique M-OCaml: on peut stocker cette info dans une loc AT

modélisation dans la logique:
  + les vues ont une opération d’union, sont ordonnées par inclusion (treillis)
  + on ajoute une assertion ↑ V pour parler de la vue du thread courant
  + le ↦at contient une vue pour parler de la vue stockée dans la loc AT
      * règles d’accès étendues

comment on fait en pratique? on utilise SPLIT-OBJ-SUBJ (rappel)



PARTIE 2b: UN EXEMPLE: LE SPIN-LOCK:

exemple: spin-lock en SC, qui ne transfère que des ressources obj.
  + code
  + spec
  + invariant
  + preuve SC

la preuve SC ne marche pas en Cosmo si on autorise P à être subj.
on patche l’invariant
on utilise SPLIT-OBJ-SUBJ



CONCLU:

tout est prouvé en Coq / Iris

dans l’article:
  + modèle de la logique: vProp := view -> iProp
  + construction de la logique en 2 étages
  + plus de structures de données

idées-clé à retenir

travaux reliés:
  - comparaison avec ORC11

future work:
  - more case studies
  - data races



--------------------------------------------------------------------------------

ancien plan exposé ICFP
-----------------------



PARTIE 1: LE MODÈLE MÉMOIRE:

rendre ça plus court

2 sortes de locations
  + non-atomic
  + atomic (= SC behavior + RelAcq sync)

pour rendre ça plus court: seulement dire:
  - que les NA ont un comportement complètement relâché (ne pas parler d’hist)
  - que les atomiques contiennent une valeur ET une vue
      + sémantique SC pour la valeur
      + sémantique rel-acq pour la vue
    sur l’exemple du message-passing?

dans tous les cas, il faut que quelque part dans l’exposé on puisse voir le 
message passing

rem: ne pas parler de timestamps

rem: regarder exposé PLDI pour voir comment eux ont présenté leur modèle



PARTIE 2: COSMO:

point non trivial: ↦na mentionne seulement une valeur (ce qu’on veut obtenir)

distinction subj / obj
  - ↦at est objectif
  - ↦na est subjectif (conséquence du point précédent)
outils de raisonnement sur les vues: ↑ V, P @ V, règle SPLIT-SUBJ-OBJ
monotonie
les vues forment un treillis, vue = ensemble d’informations, plus grand = + d’infos, union = union des infos

triplets des opérations non-atomiques:
  - règles usuelles !
  - souligner la lecture non-standard du ↦na

triplets des opérations atomiques:
  - point spécifique à M-OCaml: ↦at contient une vue
  - règle générale
      + lecture = acquire
      + écriture = release
  - cas particulier: règles simplifiées = usuelles

exemple du spin lock: montrer la spec pour insister sur le fait qu’elle ne 
change pas et que P devient subjectif:
  { isLock lk P } acquire lk { isLock lk P ∗ P }
  { isLock lk P ∗ P } release lk { isLock lk P }
détailler les preuves (si on supprime la preuve du message-passing)



CONCLUSION / BACKUP:

plus dans l’article:
  - plus de structures de données

travaux reliés:
  - comparaison avec ORC11

future work:
  - more case studies
  - data races
