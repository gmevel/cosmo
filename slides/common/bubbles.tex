%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%             Macros for drawing bubbles (taken from a Mezzo talk)             %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This counter auto-numbers the nodes that stand for program points starting
% from 1.
\newcounter{MzNode}

% This is used to generate automatic slides, with \only<...>.
\newcounter{MzSlideId}

\newcommand{\MzBang}[1]{%
  \begin{tikzpicture}[remember picture, overlay]
    \only<\theMzSlideId>{%
    \node [starburst, fill=red1, draw=red3, line width=2pt, rotate=-30]
      at ($(current page.north east)+(-2cm,-1.5cm)$)
      {\bf \color{black} #1};
    }
  \end{tikzpicture}
}

% First phase: placing program points
\newcommand{\MzStartProgramPoints}{%
  \setcounter{MzNode}{0}%
}

\newcommand{\MzRawProgramPoint}[1]{%
  % Some extra space is added (need to figure out why!).
  \hspace{-.4ex}%
  \tikz[remember picture]\node(mzprogrampoint-#1){};%
  \hspace{-1ex}%
}

\newcommand{\MzProgramPoint}{%
  \addtocounter{MzNode}{1}%
  \MzRawProgramPoint{\theMzNode}%
}

% Second phase: filling in the speech bubbles
\newcommand{\MzStartBubbles}{%
  \setcounter{MzNode}{0}%
  % Leave the first slide without any bubble.
  \setcounter{MzSlideId}{1}%
}

% A redefinable command for the content of a bubble. It takes:
% - the bubble title
% - the bubble content
\newcommand{\MzBubbleContent}[2]{%
      \small
      \hfill\underline{\textbf{\textsf{#1}}}\vspace{.2em}\\
      {#2}
}

% This is the most basic version of the bubble command. It takes:
% - the absolute x-y position of the bubble
% - the width of the bubble
% - the title of the bubble (top right corner)
% - the contents of the bubble
% - a Beamer slide range (e.g. "2", or "3-") on which this bubble should appear.
% Where the bubble will be anchored depends on the current value of \theMzNode.

% This command does NOT automatically create the tikzpicture environment.
% Indeed, one might wish to put multiple bubbles inside a single tikzpicture.
% Creating multiple tikzpictures leads to different systems of absolute
% coordinates (because the pictures have nonzero width!) so does not seem
% to be a good idea.
\newcommand{\MzAbsoluteRawBubble}[5]{%
    \only<#5>{\node [
      draw,
      ultra thick,
      teal,
      fill=teal!10,
      opacity=.9,
      shape=rectangle callout,
      callout absolute pointer={(mzprogrampoint-\theMzNode)},
      overlay,
      text width=#2,
      text=black,
    ]
    at (#1) {%
      \MzBubbleContent{#3}{#4}
    };}
}

% This is a raw version of the bubble command. It takes:
% - the x offset of the bubble, relative to the program point
% - the y offset of the bubble, relative to the program point
% - the other four parameters as above.
\newcommand{\MzRawBubble}[6]{%
  \begin{tikzpicture}[remember picture, overlay]%
    \MzAbsoluteRawBubble
      {$(mzprogrampoint-\theMzNode.center)+(#1,#2)$}
      {#3}{#4}{#5}{#6}
  \end{tikzpicture}%
}

% This takes the current program point (you MUST call \MzAutoBubble at least
% once), and generates a fresh slide with a bubble for the same program point.
\newcommand{\MzAutoBubbleSamePoint}[5]{%
  \addtocounter{MzSlideId}{1}%
  \MzRawBubble{#1}{#2}{#3}{#4}{#5}{\theMzSlideId}%
}

% This takes the next program point, and generates a fresh slide, with a bubble
% on the corresponding program point
\newcommand{\MzAutoBubble}[5]{%
  \addtocounter{MzNode}{1}%
  \MzAutoBubbleSamePoint{#1}{#2}{#3}{#4}{#5}%
}

% This generates a bubble for the next program point, with visibility determined
% by the 6th argument.
\newcommand{\MzBubble}[6]{%
  \addtocounter{MzNode}{1}%
  \MzRawBubble{#1}{#2}{#3}{#4}{#5}{#6}%
}

% This is like \MzAutoBubbleSamePoint, except the x and y coordinates are absolute,
% as opposed to relative to the program point.
\newcommand{\MzAbsoluteAutoBubbleSamePoint}[5]{%
  \addtocounter{MzSlideId}{1}%
  \MzAbsoluteRawBubble{$(#1,#2)$}{#3}{#4}{#5}{\theMzSlideId}%
}

% This is like \MzAutoBubble, except the x and y coordinates are absolute,
% as opposed to relative to the program point.
\newcommand{\MzAbsoluteAutoBubble}[5]{%
  \addtocounter{MzNode}{1}%
  \MzAbsoluteAutoBubbleSamePoint{#1}{#2}{#3}{#4}{#5}%
}

% ------------------------------------------------------------------------------

% A somewhat ad hoc command that helps place a series of bubbles at the same
% absolute position.
% The parameters are:
% - x coordinate of bubbles
% - y coordinate of bubbles
% - width of bubbles
% - a series of bubbles, defined using the commands \bubble and \samebubble;
%   each of these commands takes just one argument, namely the bubble content.

\newcommand{\MzPlaceAbsoluteBubbles}[4]{%
  \MzStartBubbles
  \MzRePlaceAbsoluteBubbles{#1}{#2}{#3}{#4}%
}

\newcommand{\MzRePlaceAbsoluteBubbles}[4]{%
  \gdef    \bubble##1{\MzAbsoluteAutoBubble         {#1}{#2}{#3}{}{##1}}
  \gdef\samebubble##1{\MzAbsoluteAutoBubbleSamePoint{#1}{#2}{#3}{}{##1}}
  \begin{tikzpicture}[remember picture, overlay]
    #4
  \end{tikzpicture}
}

% ------------------------------------------------------------------------------

% A command for drawing arrow-less bubbles.
% Useful to put several bubbles with the same coordinate system.
% - the absolute x coordinate of the bubble
% - the absolute y coordinate of the bubble
% - the other four parameters as above.
\newcommand{\MzAbsoluteBubbles}[1]{%
  \begin{tikzpicture}[remember picture, overlay]%
    #1
  \end{tikzpicture}%
}

% A command for drawing arrow-less bubbles.
% Must be put inside \MzAbsoluteBubbles.
% - the absolute x coordinate of the bubble
% - the absolute y coordinate of the bubble
% - the other four parameters as above.
\newcommand{\MzAbsoluteRawArrowLessBubble}[6]{%
  \only<#6>{\node [
    draw,
    ultra thick,
    teal,
    fill=teal!10,
    opacity=.9,
    shape=rectangle,
    overlay,
    text width=#3,
    text=black,
  ]
  at ($(#1,#2)$) {%
    \MzBubbleContent{#4}{#5}
  };}
}


% A command for drawing a single arrow-less.
% Same parameters as above
\newcommand{\MzSingleAbsoluteRawArrowLessBubble}[6]{%
  \begin{tikzpicture}[remember picture, overlay]%
    \MzAbsoluteRawArrowLessBubble{#1}{#2}{#3}{#4}{#5}{#6}
  \end{tikzpicture}%
}
