From cosmo.base Require Export base.
From Coq Require Import QArith.Qcanon.

(** Lattice for Qp *)
Program Canonical Structure Qp_Lat :=
  Make_Lat (Qp) (=) (≤)%Qp
           (λ (p q : Qp), if (decide (p ≤ q)%Qp) then q else p)
           (λ (p q : Qp), if (decide (p ≤ q)%Qp) then p else q)
           _ _ _ _ _ _ _ _ _ _ _ _ _.
Next Obligation. move=> x y. unfold sqsubseteq, join. destruct decide=>//. Qed.
Next Obligation.
  move=> x y. unfold sqsubseteq, join. destruct decide as [|?%Qp_lt_nge]=>//.
  by apply Qp_lt_le_incl.
Qed.
Next Obligation. move=>x y z. unfold join; destruct decide=>?? //. Qed.
Next Obligation.
  move=> x y. unfold sqsubseteq, meet. destruct decide as [|?%Qp_lt_nge]=>//.
  by apply Qp_lt_le_incl.
Qed.
Next Obligation. move=>x y. unfold sqsubseteq, meet. destruct decide=>//. Qed.
Next Obligation. move=>x y z. unfold meet; destruct decide=>?? //. Qed.

Instance Qp_leibnizequiv : LeibnizEquiv Qp := λ _ _ H, H.
