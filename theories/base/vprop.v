From iris.base_logic.lib Require Export own.
From iris.base_logic.lib Require Import fancy_updates.
From iris.proofmode Require Import tactics monpred.
From iris.bi Require Export monpred.
From iris.bi Require Import big_op.
From cosmo.lang Require Export lang.

(* TODO : move this to Iris. *)
Lemma monPred_in_bot {I : biIndex} {PROP : bi} `{!BiIndexBottom bot} :
  bi_entails (PROP := monPredI I PROP) True (monPred_in bot).
Proof. iStartProof PROP. iIntros (i) "_". by iPureIntro. Qed.

Canonical Structure lat_bi_index (Lat : latticeT) :=
  {| bi_index_type := Lat |}.
Instance lat_bi_index_bot (Lat : latticeT) (bot : Lat) :
  LatBottom bot → BiIndexBottom bot.
Proof. done. Qed.

Canonical Structure view_bi_index :=
  {| bi_index_type := view |}.

(*
   ICFP20: Here we build the type vProp of Cosmo assertions. These are defined
   as the monotonic predicates from views to BaseCosmo assertions (whose type
   is iProp).
   
   It is parameterized by Σ, the global resource algebra.
   
   In Iris jargon, both iProp and vProp are “BIs”, bunched implications, meaning
   that they are equipped with the usual separation logic connectives.
   
   “monPred” is a general mechanism provided by Iris to build a BI (here, vProp)
   whose elements are monotonic families from a “biIndex” type (here, the type
   of views ordered by inclusion) to an existing BI (here, iProp).
   
   (Technically, such a mononotic predicate is a dependent pair of a function
   view → iProp and a proof that this function is monotonic.)
   
   This construction yields logical connectives whose semantics is as shown in
   Figure 7 “Cosmo assertions and their meaning” in the paper; for example:
       (P ∗ Q)(W)   =  P(W) ∗ Q(W)            where P, Q : vProp
       (P -∗ Q)(W)  =  ∀ V ⊒ W, P(V) -∗ Q(V)
       (↑ V)(W)     =  V ⊑ W
       P @ V        =  P(V)
       ⎡ P ⎤(W)     =  P                      where P : iProp
   
   In this Coq development, unlike in the paper, we do not dedicate notations to
   ↑ V and P @ V; the former is referred to as “monPred_in V”, while the latter
   is simply function application.
   
   The following two theorems corresponds to the two directions of the rule
   SPLIT-SUBJECTIVE-OBJECTIVE from Figure 8 “Cosmo axioms”:
       monPred_in_elim:   ∀ (P : vProp) (V : view),  monPred_in i -∗ ⎡ P i ⎤ → P
       monPred_in_intro:  ∀ (P : vProp),  P -∗ ∃ (V : view), monPred_in i ∧ ⎡ P i ⎤
   They are also provided by Iris.
   
   Other rules from the same figure (SEEN-ZERO and SEEN-TWO) are trivial.
 *)

(* Types of view predicates. *)
Definition vProp Σ := monPred view_bi_index (uPredI (iResUR Σ)).
Definition vPropO Σ := monPredO view_bi_index (uPredI (iResUR Σ)).
Definition vPropI Σ := monPredI view_bi_index (uPredI (iResUR Σ)).

Hint Extern 10 (IsBiIndexRel _ _) => unfold IsBiIndexRel; solve_lat
            : typeclass_instances.

(* ICFP20: These instances correspond to the rule SEEN-TWO. *)
Global Instance into_sep_monPred_in V1 V2 :
  IntoSep (PROP := vPropI Σ) (monPred_in (V1 ⊔ V2)) (monPred_in V1) (monPred_in V2).
Proof. split => ?. rewrite monPred_at_sep. iPureIntro. split; solve_lat. Qed.

Global Instance from_sep_monPred_in V1 V2 :
  FromSep (PROP := vPropI Σ) (monPred_in (V1 ⊔ V2)) (monPred_in V1) (monPred_in V2).
Proof. split => ?. rewrite monPred_at_sep. iPureIntro. intros [??] ; solve_lat. Qed.

Global Instance into_and_monPred_in V1 V2 p :
  IntoAnd (PROP := vPropI Σ) p (monPred_in (V1 ⊔ V2)) (monPred_in V1) (monPred_in V2).
Proof.
  split => ?. destruct p ; rewrite /= 2?monPred_at_intuitionistically monPred_at_and ;
  iPureIntro ; split ; solve_lat.
Qed.

Global Instance from_and_monPred_in V1 V2 :
  FromAnd (PROP := vPropI Σ) (monPred_in (V1 ⊔ V2)) (monPred_in V1) (monPred_in V2).
Proof. split => ?. rewrite monPred_at_and. iPureIntro. intros [??] ; solve_lat. Qed.
