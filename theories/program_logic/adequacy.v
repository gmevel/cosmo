From iris.program_logic Require Import adequacy.
From iris.algebra Require Import gmap auth frac agree csum.
From cosmo.lang Require Import notation.
From cosmo.base Require Import lattice_cmra.
From cosmo.program_logic Require Import lifting store weakestpre.

Class cosmoPreG Σ := CosmoPreG {
  cosmoPreG_invPreG :> invPreG Σ;
  cosmoPreG_store_inG :> inG Σ (authR storeUR);
  cosmoPreG_lenstore_inG :> inG Σ (authR lenstoreUR);
  cosmoPreG_seen_inG :> inG Σ (authR seenUR)
}.

Definition cosmoΣ : gFunctors :=
  #[invΣ; GFunctor (authR storeUR); GFunctor (authR lenstoreUR); GFunctor (authR seenUR)].
Instance subG_noprolPreG {Σ} : subG cosmoΣ Σ → cosmoPreG Σ.
Proof. solve_inG. Qed.

(*
   ICFP20: This is the proof of adequacy (ie. soundness) of our program logics.
   Roughly speaking, these theorems express the fact that if WP e {φ} holds,
   then:
     - e is safe (there is no execution starting from e which gets stuck),
     - when an execution starting from e reaches a value, then that value
       satisfies the postcondition φ (ie φ(v) holds).
   
   There is a theorem for the base logic (BaseCosmo) and another one for the
   lifted logic (Cosmo). The latter derives from the former, which reuses the
   generic proof of adequacy of Iris.
   
   The hypothesis of the theorem for BaseCosmo is that seen ∅ -∗ WP (e at ∅) {φ}
   holds (or, with the notations of the article: valid ⊥ -∗ WP <e, ⊥> {φ} ),
   where ∅ is the empty view. This means that the user only needs to establish
   the WP assertion on the proviso that the configuration (the configuration
   with the empty view ∅ and the empty store ∅) is reachable, ie. satisfies the
   “Global View Invariant” of the paper.
   
   In the theorem for Cosmo, the fact that configurations satisfy the Global
   View Invariant is hidden in the definition of WP.
 *)

(* These adequacy theorems only apply when the initial state is empty.
   If we want a theorem which also applies when the initial state is
   not empty, we need to make sure the initial state is well-formed,
   i.e. we need to check that the initial view and the views stored
   at atomic locations are all consistent with the contents of the
   store. *)

(* ICFP20: This is Theorem 3.1 “Soundness of BaseCosmo” from the paper. *)
Theorem basecosmo_adequacy Σ `{cosmoPreG Σ} (e : expr) (φ : view_lang.val → Prop):
  (∀ `{cosmoG Σ} , ⊢ WP (e at ∅) {{ vV, ⌜φ vV⌝ }}) →
  adequate NotStuck (e at ∅) ∅ (λ vV _, φ vV).
Proof.
  intros Hwp. apply (wp_adequacy _ _).
  iIntros (Hinv ?).
  iMod (own_alloc (● (∅ : storeUR))) as (store_name) "store".
  { by apply auth_auth_valid. }
  iMod (own_alloc (● (∅ : lenstoreUR))) as (lenstore_name) "lenstore".
  { by apply auth_auth_valid. }
  iMod (own_alloc (● (to_latT ∅ : seenUR) ⋅ ◯ to_latT ∅)) as (seen_name) "[seenA #seen]".
  { by apply auth_both_valid_discrete. }
  set (STOREG := StoreG Σ _ store_name _ lenstore_name).
  set (SEENG := SeenG Σ _ seen_name).
  set (COSMOG := CosmoG Σ _ _ _).
  specialize (Hwp COSMOG).
  iExists _, _. iSplitL; last by iApply Hwp. simpl. iSplitL "store lenstore".
  - rewrite /store_interp /store_to_cmra /store_to_lenstore_cmra 2!fmap_empty.
    by iFrame.
  - rewrite /seen_interp. iExists ∅. iFrame. iPureIntro.
    split ; last split ; intros [ℓ i].
    + intros ??. rewrite store_lookup_eq. by case_bool_decide.
    + intros ?. rewrite store_lookup_eq. by case_bool_decide.
    + done.
Qed.

(* ICFP20: This is Theorem 4.1 “Soundness of Cosmo” from the paper. *)
Theorem cosmo_adequacy Σ `{cosmoPreG Σ} (e : expr) φ:
  (∀ `{cosmoG Σ} , ⊢ WP e {{ v, ⌜φ v⌝ }}) →
  adequate NotStuck (e at ∅) ∅ (λ v _, φ v).
Proof.
  intros Hwp. apply (basecosmo_adequacy _).
  iIntros (COSMOG).
  iAssert (|==> seen ∅)%I as ">Hseen". { rewrite seen_eq. by iApply own_unit. }
  specialize (Hwp COSMOG). destruct Hwp as [Hwp]. rewrite wp_eq in Hwp.
  iApply (iris.program_logic.weakestpre.wp_wand with "[Hseen]").
  iApply (Hwp ∅ with "[//] [//] Hseen").
  iIntros ([v V]) "[_ ?] //".
Qed.
